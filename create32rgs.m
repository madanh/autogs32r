function [ vol,OPR,WPR,errors ] = create32rgs( volfolder, pr1file, pr2file,outResultFolder)
%% read the config
configFileName = 'config.ini';
geomFileName = 'geometry.ini';
config = readConfig(configFileName);
geom =  readConfig(geomFileName);

%% load 3D volume
if ischar(volfolder)
    vol = readCTFolder(volfolder,config.permute_xy_in_ct_to_match_matlab_indexing);
elseif isstruct(volfolder)
    vol = volfolder;
else
    eggog = MException('create32rgs:input',...
        'create32rgs:volfolde should be either a folder name string ar a volume structure');
    throw(eggog);
end


%% load 2D projections
if ischar(pr1file) 
    OPR = loadOneProjection(pr1file);
elseif isstruct(pr1file)
    OPR = pr1file;
else
     eggog = MException('create32rgs:input',...
        'create32rgs:pr1file should be either a folder name string ar a volume structure');
    throw(eggog);
end

if ischar(pr2file) 
    WPR = loadOneProjection(pr2file);
elseif isstruct(pr2file)
    WPR = pr2file;
else
     eggog = MException('create32rgs:input',...
        'create32rgs:pr2file should be either a folder name string ar a volume structure');
    throw(eggog);
end

%% 3D PROCESSING
if ~isfield(vol,'markerIntristicCoords3D')
% detect 3d markers

    opt.ct_threshold = config.ct_threshold;
    opt.maximum_number_of_3d_markers = config.maximum_number_of_3d_markers;
    opt.diagnostic = config.detect_3d_diag;
    opt.ball_radius_in_mm = geom.ball_radius_in_mm;
    opt.max_paxes_ratio = config.max_paxes_ratio;

    [vol]= feval(config.detect_3d_function_name,vol,opt);


%     fprintf('we have %1.f marker candidates ',size(vol.markerIntristicCoords3D,1));


    % identify the 3D marker groups
    opt.single_link_cutoff_threshold = 1.1*(geom.interball_distance_in_mm+4*geom.ball_radius_in_mm);
    opt.diagnostic = config.linkage_diag;
    opt.group_size = geom.n_markers_in_groups; % 4 markers in one group
    opt.reverse_order = config.reverse_order;
    opt.sortdim = config.sortdim; %sort markers in groups along z
    opt.test = 'test';
    %<========== Place for the breakpoint: call the 3D identifying function
    vol = feval(config.identify_3d_function_name,vol,opt);
    fprintf('and %1.f markers after clustering\n',size(vol.markerIntristicCoords3D,1));
end

%% 2D PROCESSING

    %identify groups of markers in 2D images

    %Form options structure for identify_groups.m
    clear opt;
    opt.reuse = config.id_groups_reuse;
    opt.marker_projection_jitter_threshold = config.marker_projection_jitter_threshold;
    opt.ball_radius_in_mm = geom.ball_radius_in_mm;
    opt.diagnostic = config.id_groups_diag;
    opt.detect_2d_function_name = config.detect_2d_function_name;
    opt.interball_distance_in_mm = geom.interball_distance_in_mm;
    opt.max_arc_angle = config.max_arc_angle;
if ~isfield(OPR,'To3DMarkerCorrespondence')
    OPR = feval(config.identify_2d_function_name,OPR,vol,opt);
end
if ~isfield(WPR,'To3DMarkerCorrespondence')
    WPR = feval(config.identify_2d_function_name,WPR,vol,opt);

end

%% Run Uros's stereo_Carm_calibration_Shecter.m (or a derivative )with the identified markers
% In order to get GS registrations
opt.diagnostic = config.calibr_diag;

[WPR.header,OPR.header,errors,WPR_r,OPR_r] = feval(config.calibrate_function_name,vol,WPR,OPR,opt);
if opt.diagnostic; drawnow; end
%% Save into the outResultFolder
if exist('outResultFolder','var') && ~config.disable_output
    
    [st,mess,~]=mkdir(outResultFolder);
    
    %mkdir will not raise exception if the folder exists
    if ~st %if anything went wrong
        fprintf(2,'unable to create output folder %s : %s \n',outResultFolder,mess);
        return
    end
    outPatientFolderName = outResultFolder;
    
    % try to write headers
    %Writing WPR header ,the file name is hardcoded :(
    try
        writeRadHeaderUrosStyle(fullfile(outPatientFolderName,config.wpr_rad_suffix),WPR.header);
    catch
        fprintf(2,'unable to write file %s, continuing\n',fullfile(outPatientFolderName,'WPR.rad'));
        return;
    end
    
    %Writing OPR header, the file name is hardcoded :(
    try
        writeRadHeaderUrosStyle(fullfile(outPatientFolderName,config.opr_rad_suffix),OPR.header);
    catch
        fprintf(2,'unable to write file %s, continuing\n',fullfile(outPatientFolderName,'OPR.rad'));
        return;
    end
end

end




