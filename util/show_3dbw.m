function show_3dbw( im,varargin )
%SHOW_3DBW Summary of this function goes here
%   Detailed explanation goes here

ind = find(im);
[x,y,z] = ind2sub(size(im),ind);
scatter3(x,y,z,varargin{:});
drawnow

end

