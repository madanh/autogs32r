function [ output_args ] = harness( inDbFolder, outResultFolder)

%% "imports"
addpath(genpath('./lib'));
addpath(genpath('./lib/mitrovic'));
addpath(genpath('./lib/mitrovic/nrc-powell'));
addpath(genpath('./lib/io'));
addpath(genpath('./lib/matchgroups'));

%% read the config
configFileName = 'config.ini';
geomFileName = 'geometry.ini';
config = readConfig(configFileName);
geom =  readConfig(geomFileName);

%% Get the patient folders
try
    listing = dir(inDbFolder);
catch
    fprintf(2,'Cannot get the input database directory contents, dropping to keyboard \n');
    keyboard;
end
counter=0;
for i = 1:length(listing)
    %get proper subdirectories vvv
    if listing(i).isdir && ~strcmp(listing(i).name,'.') && ~strcmp(listing(i).name,'..');
        counter = counter+1;
        CTFolderNames{counter} = fullfile(inDbFolder,listing(i).name);
    end
end

% This is a quick dirty code involved in quick dirty statistics collection
if config.collect_stats
    if exist('./out.csv','file')
        movefile('./out.csv',strcat('./out',num2str(randi(1000)),'.csv'),'f');
    end
end
%% MAIN LOOP
for i = 1:counter
    %      hgload(fullfile(CTFolderNames{i},'selected_markers.fig'))
    %       set(gcf,'visible','on')
    %      continue
    fprintf(2,'i=%.0d\n',i);
    %% prepare input and call gold standard creation function
    volfolder = dir([fullfile(CTFolderNames{i},config.vol_dir_suffix),'*']);
    volfolder = fullfile(CTFolderNames{i},volfolder.name);
    
    opt.verbose = 1;
    [pr1fname,pr2fname] = getConventionalProjectionNames(CTFolderNames{i},opt);
    if isfield(config,'disable_output') && ~config.disable_output
        [ vol,OPR,WPR,errors ] = create32rgs( volfolder, pr1fname, pr2fname,outResultFolder);
    else
        [ vol,OPR,WPR,errors ] = create32rgs( volfolder, pr1fname, pr2fname);
    end
    
    %% collect stats
    if isfield(config,'collect_stats') && config.collect_stats
        try
            patnumber = regexp(volfolder,'Patient (\d+)','tokens');
            patnumber = str2double(patnumber{1}{1});
            dlmwrite('./out.csv',[patnumber,errors.mTRE,errors.FLE,errors.FRE,size(vol.markerIntristicCoords3D,1),...
                size(OPR.markerIntristicCoords2D,1),size(WPR.markerIntristicCoords2D,1)],'delimiter',',','-append');
        catch
            warning('out.csv was not written');
        end
    end
    
end
end
