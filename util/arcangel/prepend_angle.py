#!/usr/bin/python 

__author__ = 'madanh'

import os
import re

def dprint(inp):
    '''
    debugging print
    :return: nothing
    '''

    print inp

def main(in_dir = 'outcsv',outfile = 'outprepended.csv'):
    try:
        os.remove(outfile)
    except:
        pass
    with open(outfile,'a') as a:
        #get dir
        dirlist = os.listdir(in_dir)
        dprint(dirlist)
        #for each file in dir
        for fname in dirlist:
            # get current angle
            p = re.compile('out([0-9.]+).csv')
            m = p.match(fname)
            angle = m.group(1);
            # dprint(m.group(1))
            with open(os.path.join(in_dir,fname)) as f:
                content = f.readlines()
                # dprint(content)
                #for each line
                for line in content:
                    #prepend angle
                    l = "".join([angle,',',line])
                    l =l.replace(',',' ')
                    #dump into an outfile
                    dprint(l)
                    a.write(l)


    return 0

if __name__ == '__main__':
    main()
