fname = 'outprepended.csv';
A = dlmread(fname,' ');

%% N vs ANGLE
figure;
scatter(A(:,1),A(:,9));
angles = unique(A(:,1));
mean_n = [];
for angle = angles.'
    mean_n = [mean_n mean(A(A(:,1) == angle,9))];
end

hold on
plot(angles,mean_n,'r')

xlabel('Minimum arc angle parameter of circle detector, degrees')
ylabel('Number of markers that were detected in both projections')

grid on

legend({'N','mean'},'Location','Best')

%% ERROR ESTIMATES vs ANGLE
figure;
scatter(A(:,1),A(:,3));
mean_mTRE = [];
for angle = angles.'
    mean_mTRE = [mean_mTRE mean(A(A(:,1) == angle,3))];
end

hold on
plot(angles,mean_mTRE,'r')
ylabel('mTRE new')
grid on


figure;
scatter(A(:,1),A(:,4));
mean_mTRE = [];
for angle = angles.'
    mean_mTRE = [mean_mTRE mean(A(A(:,1) == angle,4))];
end

hold on
plot(angles,mean_mTRE,'r')
ylabel('FRE')
grid on

%% FRE vs N
figure;
scatter(A(:,9),A(:,4),50,A(:,1),'.');
ylabel('FRE');
xlabel('N markers used in calibration');



%% mTRE vs N
figure;
scatter(A(:,9),A(:,3),50,A(:,1),'s');
ylabel('mTRE new');
xlabel('N markers used in calibration');


%% just a sample of chi-squared "over" k  pdfs
% figure
% x = 0:0.01:20;
% for k = 2:2
%     hold on
%     plot(x,chi2pdf(x,k))
% end