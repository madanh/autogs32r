function [ hash ] = get_hash( file_path )
%GET_HASH Summary of this function goes here
%   Detailed explanation goes here
    [~,hash] = system(['sha1sum' ' ' file_path]);
    hash = strsplit(hash);
    hash = hash(1:2:end-1);
    
    for i = 1:numel(hash)
        hash{i} = hash{i}(1:36);
        hash{i}([9,14,19,24]) = '-';
    end
end