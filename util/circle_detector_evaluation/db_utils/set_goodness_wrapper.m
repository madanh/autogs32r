function goodnes_value = set_goodness_wrapper(set_goodnes_function_string,detector,...
    tr_set,gs_set,det_opt_names,varargin )
%SET_GOODNESS_WRAPPER wraps set_goodness_function to enable database caching
%   tr - training set
%   gs - gold standard set


%% MLOCK
mlock;

persistent det_id;
persistent im_id;
persistent gs_id;
persistent set_g_fun_id;
%% READ DBCONFIG
dbconfig = loadjson(fullfile(fileparts(which('det_wrapper')),'dbconfig.json'));
%% SET UP PERSISTENT CONNECTION
persistent conn;

if isempty(conn)%if calling the first time;
    javaaddpath(dbconfig.db_driver_path);
    conn = database(dbconfig.db_name,dbconfig.db_username,dbconfig.db_password,'Vendor',dbconfig.db_vendor,'Server',dbconfig.db_host,'PortNumber',dbconfig.db_port);
end
%% GET HASHES
if isempty(det_id)
    det_id = get_hash(which(detector));
    im_id = get_hash(strjoin(tr_set(:).'));
    gs_id = get_hash(strjoin(gs_set(:).'));
    set_g_fun_id = get_hash(which(set_goodnes_function_string));
end
%% create detector options structure
    
    def_opt = feval(detector,'get default options');
    detector_opt = cell2struct(num2cell(varargin{1}),det_opt_names,1);
    detector_opt.diagnostic = false;
    detector_opt = unite_options(def_opt,detector_opt);
%% CHECK IF SET_GOODNESS TABLE HAS CORRESP VALUES
% prepare input //select * from t1 where a<@b and a@>b
im_id_string = ['{ ',strjoin(im_id,','),' }'];
gs_id_string = ['{ ',strjoin(gs_id,','),' }'];
opt_json = savejson('options',detector_opt);
query = ['select value from set_goodness where',...
    ' det_id =''',det_id{1},''' and ',... 
    ' im_id <@ ''',im_id_string,''' and ', ' im_id @> ''',im_id_string,''' and ',...
    ' gs_id <@ ''',gs_id_string,''' and ', ' gs_id @> ''',gs_id_string,''' and ',...
    ' opt = ''', opt_json, ''' and ',...
    ' set_goodness_fun_id = ''', set_g_fun_id{1} ,''';'];

res = fetch(conn,query);

%% ON HIT
if ~isempty(res)
 goodnes_value = res{1};
%% ON MISS
else    
% run the goodness function 
    goodnes_value = feval(set_goodnes_function_string,detector,tr_set,gs_set,det_opt_names,varargin{1});
% save to db
    query = ['insert into set_goodness (det_id,im_id,gs_id,opt,set_goodness_fun_id,value) VALUES (''',...
        strjoin({det_id{1},im_id_string,gs_id_string,opt_json,set_g_fun_id{1}},''','''),''',',num2str(goodnes_value),...
        ');'];
    suc = exec(conn,query);
    if ~isempty(suc.Message)
        fprintf(2,[mfilename, ': could not insert into db\n a KEYBOARD IS YOU!!']);
        keyboard;
        
    end
    close(suc);
    
% save  set_goodness_function to db if not there
query = ['select exists(select 1 from set_goodness_functions where id=''',set_g_fun_id{1},''');'];
    res = fetch(conn,query);
    if ~res{1}
        % Identify the detector and add ot the detectors table
        cd_string = ['cd',fileparts(which(mfilename))]; % have to cd in order for git to work
        [~,current_git_commit]  = system([cd_string,' && ','git --no-pager log -1 --format="%H"']);
        datainsert(conn,'set_goodness_functions',{'id','name','commit','path'},{set_g_fun_id{1},set_goodnes_function_string,current_git_commit,which(detector)});
    end
end
end

