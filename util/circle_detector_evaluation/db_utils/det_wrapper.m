function [c_i,r_i,diag] = det_wrapper(detector,im_file_string,opt,dbconfig)
% fprintf('%s: function entered\n',mfilename());
mlock;
%% SET UP PERSISTENT DB CONNECTION
if nargin<4
    dbconfig = loadjson(fullfile(fileparts(which('det_wrapper')),'dbconfig.json'));
end

persistent conn;
persistent call_count;




if isempty(call_count) 
    call_count = 0;
else
    if call_count > 1000 % CLEAN THE old stuff
        call_count = 0;
        close(conn);
        clear('conn');
        conn = [];
    end
    call_count = call_count+1;
end


if isempty(conn)%if calling the first time;
    javaaddpath(dbconfig.db_driver_path);
    conn = database(dbconfig.db_name,dbconfig.db_username,dbconfig.db_password,'Vendor',dbconfig.db_vendor,'Server',dbconfig.db_host,'PortNumber',dbconfig.db_port);
end



%% CHECK THE DB IF WE NEED TO RUN THIS
% get detector id
det_id = get_hash(which(detector));
det_id = det_id{1};
im_id =get_hash(im_file_string);
im_id = im_id{1};
def_opt = feval(detector,'get default options');
opt = unite_options(def_opt,opt);
opt_json = savejson('options',opt);

query = ['select exists(select 1 from runs where det=''',det_id,''' and im=''',im_id,''' and opt=''',opt_json,''');'];
res = fetch(conn,query);


if ~res{1} %on database miss
   fprintf('M');
    query = ['select exists(select 1 from detectors where id=''',det_id,''');'];
    res = fetch(conn,query);
    if ~res{1}
        % Identify the detector and add ot the detectors table
        [~,current_git_commit]  = system('git --no-pager log -1 --format="%H"');
        datainsert(conn,'detectors',{'id','name','commit','path'},{det_id,detector,current_git_commit,which(detector)});
    end
    % Identify the im and add to the images table
    query = ['select exists(select 1 from images where id=''',im_id,''');'];
    res = fetch(conn,query);
    if ~res{1}
        tmp1 = dir(im_file_string);
        tmp1 =tmp1.date;
        datainsert(conn,'images',{'id','name','modified'},{im_id,im_file_string,datestr(tmp1,'yyyy-mm-dd hh:MM:ss')});
    end
   % RUN THE DET
    load(im_file_string,'im');
    [c_i,r_i] = feval(detector,im,opt);

    % SAVE TO DB
    query = ['insert into runs(det,im,opt,c,r) values(''',strjoin({det_id,im_id,opt_json,savejson('centers',c_i),savejson('radii',r_i)},''','''),''');'];
    curs = exec(conn,query);
    close(curs);
    


else % on database hit
    fprintf('H');
    query=['select c,r from runs where det=''',det_id,''' and im=''',im_id,''' and opt=''',opt_json,''';'];
    res = fetch(conn,query);
    c_i = loadjson(char(res{1,1}.getValue));
    c_i = c_i.centers;
    r_i = loadjson(char(res{1,2}.getValue));
%     if isfield(r_i,'radii')
    r_i = r_i.radii;
    if size(res,1)>1
        fprintf(2,['Warning:Database error: duplicate entry for query:\n',query]);
        
    end
%     else
%         figure; text(0.1,0.5,'a keyboard is you','FontSize',32)
%         warning('A KEYBOARD IS YOU!!\n');
%         keyboard;
%         
%     end
    diag = []; % no diagnostic output on database hit
end



end
