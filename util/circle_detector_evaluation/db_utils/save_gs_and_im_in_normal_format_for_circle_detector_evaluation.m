function [ status ] = save_gs_and_im_in_normal_format_for_circle_detector_evaluation( PR, outFolder )
%SAVE_GS_AND_IM_IN_NORMAL_FORMAT_FOR_CIRCLE_DETECTOR_EVALUATION Summary of this function goes here
%   Detailed explanation goes here


    [~, name, ~]=fileparts(PR.fname);
   
    where_to_save = fullfile(outFolder,[name,'.mat']);
    
    what_to_save.im = PR.im;
    what_to_save.gs = PR.gs.centers;
    
    save(where_to_save,'-struct','what_to_save');

end

