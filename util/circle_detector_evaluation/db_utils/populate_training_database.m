outFolder = '/mnt/stonfs/FRANJO/Me/CircleGSDatabase';


inDbFolder ='/mnt/stonfs/FRANJO/Me/HybridDatabase';

%% "imports"
addpath('./lib');
addpath('./lib/mitrovic');
addpath('./lib/mitrovic/nrc-powell');
addpath('./lib/io');
addpath('./lib/matchgroups');

%% read the config
configFileName = 'config.ini';
geomFileName = 'geometry.ini';
config = readConfig(configFileName);
geom =  readConfig(geomFileName);

%% Get the patient folders
try
    listing = dir(inDbFolder);
catch
    fprintf(2,'Cannot get the input database directory contents, dropping to keyboard \n');
    keyboard;
end
counter=0;
for i = 1:length(listing)
    %get proper subdirectories vvv
    if listing(i).isdir && ~strcmp(listing(i).name,'.') && ~strcmp(listing(i).name,'..');
        counter = counter+1;
        CTFolderNames{counter} = fullfile(inDbFolder,listing(i).name);
    end
end

% This is a quick dirty code involved in quick dirty statistics collection
if config.collect_stats
    if exist('./out.csv','file')
        movefile('./out.csv',strcat('./out',num2str(randi(1000)),'.csv'),'f');
    end
end
%% MAIN LOOP
for i = 1:counter
    
    
    %% load 2D projections
    try
        switch config.input_style
            case 'Gena'
                OPR=load(fullfile(CTFolderNames{i}, config.opr_fname));
                WPR=load(fullfile(CTFolderNames{i}, config.wpr_fname));
            case 'Hybrid'
                opt = [];
                opt.verbose = config.load_verbose;
                [OPR,WPR] = loadUrosStyleProjections(CTFolderNames{i},opt);
                %             WPR=load(fullfile(CTFolderNames{i}, config.wpr_fname));
                
        end
    catch
        warning('Unable to load projection images from %s',CTFolderNames{i})
        warning('Skipping to the next patient')
        continue
    end
    
    
    %% LOAD GOLD STANDARDS
    try
        %OPR
        the_file = dir([CTFolderNames{i},'/Pat*OPR_Markers.mat']);
        if isempty(the_file)
            the_file = dir([CTFolderNames{i},'/Pat*LAT_Markers.mat']);
        end

        [OPR.gs_filename] = fullfile(CTFolderNames{i},the_file.name);
        gs = load(OPR.gs_filename);
        OPR.gs = gs;
        
        % WPR
        the_file = dir([CTFolderNames{i},'/Pat*WPR_Markers.mat']);
        if isempty(the_file)
            the_file = dir([CTFolderNames{i},'/Pat*AP_Markers.mat']);
        end
        [WPR.gs_filename] = fullfile(CTFolderNames{i},the_file.name);
        gs = load(WPR.gs_filename);
        WPR.gs = gs;
    catch
        warning('Unable to load gold standard marker centers from %s',CTFolderNames{i})
        warning('Skipping to the next patient')
        continue
    end
    
  
  
% %% SHOW GS MARKERS
%     for PR = [OPR,WPR]
%         figure;imagesc(PR.im);axis image;colormap gray; hold on;
%         scatter(PR.gs.XMc./PR.dx+0.5,PR.gs.YMc./PR.dy+0.5,'r+');
%         title(PR.gs_filename)
%     end

%% CONVERT GS FROM MM TO PX AND BETTER FORMAT
    command_string = '*PR.gs.centers = [*PR.gs.XMc./*PR.dx+0.5,*PR.gs.YMc./*PR.dy+0.5];';
    for PR = ['O','W']
        eval(strrep(command_string,'*',PR));
    end

    

%% SAVE TO NORMAL GS FORMAT
save_gs_and_im_in_normal_format_for_circle_detector_evaluation(OPR,outFolder);
save_gs_and_im_in_normal_format_for_circle_detector_evaluation(WPR,outFolder);

    
    
%% CHECK IF WE'VE RUN THIS TEST BEFORE
%     tic;
%     exp_db    
%     toc

%     
% %BLACK MAGIC FOLLOWS:
%         to_evaluate = verbatim;
%         %{
%         figure; imagesc(OPR.im); colormap gray; axis image; hold on;
%             title(OPR.gs_filename)
%             scatter(OPR.gs.XMc./OPR.dx+0.5,OPR.gs.YMc./OPR.dy+0.5,'y+');
%         OPR.marker_coords =spread(OPR.im); %call circle detector
%         showMarkers2D(OPR.marker_coords);
%         tolerance = 4;
%         [TP,FP,P] = circle_detector_error_rates(OPR.marker_coords,OPR.gs.centers, tolerance);
%         %}
%         eval(to_evaluate); %OPR
%         eval(strrep(to_evaluate,'O','W')); %replace O with W to get WPR


%% PROCESS
%     process_one_projection(OPR, 'spread');
%     process_one_projection(WPR, 'spread');

    
    
    

end














