function [ optimal_options_values ] = train_circle_detector( detector,...
    training_set, training_gs,opt_names, opt_bounds,varargin )
%TRAIN_CIRCLE_DETECTOR Find options of circle detector that maximise
%Fmeasure
% Inputs:
% --------
%
% training_set: a cell of strings with paths to image files
% training_gs: a cell of strings with paths to correponding gold standard
%     files
% varargin{1}: additional local optimizer options constucted by
% optimooptions, only fmincon is currently supported (you can also specify
% a struct with only the fields that are to be changed
% varargin{2}: additional global optimizer options
%
% Created by Hennadii Madan madanh2014@gmail.com

%% TODOS
% * Pull the disk reading part out of 
% evaluate_circle_detector_fmeasure_on_a_set to here, optionially provide a
% way to switch behaviours

% * We will need to propagate the database ID all the way to detector
%call, this calls for config file reading!


%% INPUT PARSING

if strcmpi(detector ,'test')
    
    if nargin>1
        m = containers.Map();
        m('1')  = @internal_test_1;
        m('syntax') = @internal_test_1;
        m('2')  = @internal_test_2;
        m('one image') = @internal_test_2;
        m('3')  = @internal_test_3;
        m('one image two options') = @internal_test_3;
        m('4')  = @internal_test_4;
        m('only pairs_per_circle_threshold') = @internal_test_4;
        m('5')  = @internal_test_5; %two images two options
        
        
        feval(m(num2str(training_set)));
    else
        internal_test_1();
    end
    return
end

extra_local_optimizer_options = struct();
if nargin>5
    if ~isempty(varargin{1})
        extra_local_optimizer_options = varargin{1};
    end
end

extra_global_optimizer_options = struct();
if nargin>6
    if ~isempty(varargin{2})
        extra_global_optimizer_options = varargin{2};
    end
end

%% STARTING POINTS (AD HOC GLOBAL FOR PATTERN SEARCH)
% defaults
search_space_size = prod(cellfun(@(x) abs(x(2)-x(1)),opt_bounds));
global_optimizer_options.NPoints = round(0.1*search_space_size);
global_optimizer_options = unite_options(global_optimizer_options,...
    extra_global_optimizer_options);
rng('shuffle'); 
starting_points = ...
    cell2mat(cellfun(@(x) randi([x(1),x(2)],1,global_optimizer_options.NPoints),opt_bounds,'UniformOutput',false));

%% SETUP FOR PATTERN SEARCH
    cost_fun = @(varargin) -set_goodness_wrapper('evaluate_circle_detector_fmeasure_on_a_set',detector,...
        training_set,training_gs,opt_names,varargin{:});
    
    lb = cellfun(@min,opt_bounds);
    ub = cellfun(@max,opt_bounds);
    ps_options = psoptimset(@patternsearch);
    ps_options.PollingOrder = 'Random';
    ps_options.InitialMeshSize = ceil(0.5*min(cellfun(@(x) abs(x(2)-x(1)),opt_bounds)));
    ps_options = psoptimset(ps_options,extra_local_optimizer_options);
    try
        for i = 1:size(starting_points,2)
            %% PATTERN SEARCH LOCAL (SINGLE RUN)
            x0 = starting_points(:,i);
            
            % SIGNATURE FOR THE REFERENCE:
            % [x,fval,exitflag,output] = patternsearch(fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options)
            
            [optimal_options_values{i}, f_measure_value{i}] = ...
                patternsearch(cost_fun,x0,[],[],[],[],lb,ub,[],ps_options);
            fprintf(2,'| %10.10s ','fmeas',opt_names{:})
            fprintf(2,'|\n');
            fprintf(2,'%c------------','+'*ones(size([optimal_options_values{i};f_measure_value{i}])));
            fprintf(2,'+\n');
            fprintf(2,'| %10d ',f_measure_value{i},optimal_options_values{i});
            fprintf(2,'|\n');
            fprintf(2,'%c------------','+'*ones(size([optimal_options_values{i};f_measure_value{i}])));
	    fprintf(2,'+\n');
% SAVE SAME STUFF IN THE LOG


	fid = fopen([mfilename(),'_',detector,'.log'],'a+');
        if fid > 0 % fopen gives negative, not zero on errors
            fprintf(fid,'| %10.10s ','fmeas',opt_names{:});
            fprintf(fid,'|\n');
            fprintf(fid,'%c------------','+'*ones(size([optimal_options_values{i};f_measure_value{i}])));
            fprintf(fid,'+\n');
            fprintf(fid,'| %10d ',f_measure_value{i},optimal_options_values{i});
            fprintf(fid,'|\n');
            fprintf(fid,'%c------------','+'*ones(size([optimal_options_values{i};f_measure_value{i}])));
            fprintf(fid,'+\n');
            fclose(fid);
        else
            fprint(2,[mfilename(),':could not write log file\n']);
        end
% SAVE SAME STUFF IN DB

        end
        munlock det_wrapper % must do this
        munlock('set_goodness_wrapper'); % and this
    catch ME
        munlock det_wrapper % must do this
        munlock('set_goodness_wrapper'); % and this
        % FIND THE BEST OF THE BESTS - Sorry :(
        if exist('f_measure_value','var')
            f_measure_value = cell2mat(f_measure_value);
            best_fmeas_value = min(f_measure_value);
            ind = find(f_measure_value == best_fmeas_value);
            optimal_options_values = [optimal_options_values{ind}];
        end
        ME.rethrow()
    end

%% FIND THE BEST OF THE BESTS
    f_measure_value = cell2mat(f_measure_value);
    best_fmeas_value = min(f_measure_value);
    ind = find(f_measure_value == best_fmeas_value);
    optimal_options_values = [optimal_options_values{ind}];
end





%% ########################################################################
%%                                !!!BELOW TESTS!!!
%% ########################################################################

%% ========================================================================
%%                                TEST 1
%% ========================================================================
function internal_test_1()
fprintf(2,'train_circle_detector : running internal_test_1 - syntax test\n')
training_set = {'/mnt/stonfs/FRANJO/Me/CircleGSDatabase/Pat25_2D-DSA_AP.mat';'/mnt/stonfs/FRANJO/Me/CircleGSDatabase/Pat25_2D-DSA_LAT.mat'};
training_gs = training_set;
opt_names = {'radius';'max_arc_angle'};
opt_bounds = {[5 15];[30 60]};
detector = 'spread';

%local opt options
local_opt.TolX = 0.1;
local_opt.TolFun = 0.05;
local_opt.Display = 'iter';

% global opt optoins
global_opt.Display = 'iter';
global_opt.MaxTime = 30;
global_opt.TolFun = 0.05;
global_opt.TolX = 0.1;

try
    train_circle_detector(detector,...
    training_set,training_gs,...
    opt_names,opt_bounds,....
    local_opt,global_opt)
    fprintf(2,'train_circle_detector : internal_test_1 : [ PASS ]\n')
catch
    fprintf(2,'train_circle_detector : internal_test_1 : [ FAIL ]\n')
end
end


%% ========================================================================
%%                                TEST 2
%% ========================================================================
function internal_test_2()
fprintf(2,'train_circle_detector: running internal_test_2 : single image \n')
training_set = {'/mnt/stonfs/FRANJO/Me/CircleGSDatabase/Pat25_2D-DSA_AP.mat'};
training_gs = training_set;
opt_names = {'max_arc_angle'};
opt_bounds = {[30 50]};
detector = 'spread';

%local opt options
local_opt.ScaleMesh = 'off'; % this MUST be off because of MATLAB bug
% local_opt.CompletePoll = 'on';
% local_opt.CompleteSearch = 'on';
local_opt.TolMesh = 1;


% local_opt.TolX = 0.5;

local_opt.TolFun = 0.01;
local_opt.Display = 'iter';
% local_opt.DiffMinChange = 1;
% local_opt.Diagnostics = 'on';

% global opt optoins
global_opt.NumTrialPoints = 1;
global_opt.NumStageOnePoints =1;
global_opt.Display = 'iter';
global_opt.MaxTime = 90;
global_opt.TolFun = 0.05;
global_opt.TolX = 0.1;

try
    train_circle_detector(detector,...
    training_set,training_gs,...
    opt_names,opt_bounds,....
    local_opt,global_opt)
    fprintf(2,'train_circle_detector : internal_test_2 : [ PASS ]\n')
catch
    fprintf(2,'train_circle_detector : internal_test_2 : [ FAIL ]\n')
end

end


%% ========================================================================
%%                                TEST 3
%% ========================================================================
function internal_test_3()
fprintf(2,'train_circle_detector: running internal_test_3 : single image two options \n')
training_set = {'/mnt/stonfs/FRANJO/Me/CircleGSDatabase/Pat25_2D-DSA_AP.mat'};
training_gs = training_set;
opt_names = {'radius';'max_arc_angle'};
opt_bounds = {[5 15];[30 60]};
detector = 'spread';

%local opt options
local_opt.ScaleMesh = 'off'; % this MUST be off because of MATLAB bug
% local_opt.CompletePoll = 'on';
% local_opt.CompleteSearch = 'on';
local_opt.TolMesh = 1;


% local_opt.TolX = 0.5;

local_opt.TolFun = 0.01;
local_opt.Display = 'iter';
% local_opt.DiffMinChange = 1;
% local_opt.Diagnostics = 'on';

% global opt optoins
global_opt.NumTrialPoints = 1;
global_opt.NumStageOnePoints =1;
global_opt.Display = 'iter';
global_opt.MaxTime = 90;
global_opt.TolFun = 0.05;
global_opt.TolX = 0.1;

try
    train_circle_detector(detector,...
    training_set,training_gs,...
    opt_names,opt_bounds,....
    local_opt,global_opt)
    fprintf(2,'train_circle_detector : internal_test_2 : [ PASS ]\n')
catch
    fprintf(2,'train_circle_detector : internal_test_2 : [ FAIL ]\n')
end

end


%% ========================================================================
%%                                TEST 4
%% ======================================================================
function internal_test_4()
fprintf(2,'train_circle_detector: running internal_test_4 : only pairs_per_circle_threshold \n')
training_set = {'/mnt/stonfs/FRANJO/Me/CircleGSDatabase/Pat25_2D-DSA_AP.mat'};
training_gs = training_set;
opt_names = {'pairs_per_circle_threshold'};
opt_bounds = {[1 16]};
detector = 'spread';

%local opt options
local_opt.ScaleMesh = 'off'; % this MUST be off because of MATLAB bug
% local_opt.CompletePoll = 'on';
% local_opt.CompleteSearch = 'on';
local_opt.TolMesh = 1;


% local_opt.TolX = 0.5;

local_opt.TolFun = 0.01;
local_opt.Display = 'iter';
% local_opt.DiffMinChange = 1;
% local_opt.Diagnostics = 'on';

% global opt optoins
global_opt.NumTrialPoints = 1;


try
    train_circle_detector(detector,...
    training_set,training_gs,...
    opt_names,opt_bounds,....
    local_opt,global_opt)
    fprintf(2,'train_circle_detector : internal_test_2 : [ PASS ]\n')
catch the_exception_that_cause_failure
    fprintf(2,'train_circle_detector : internal_test_2 : [ FAIL ]\n')
    the_exception_that_cause_failure.rethrow();
end

end

%% ========================================================================
%%              TEST 5: TWO IMAGES TWO OPTIONS PATTERN SEARCH
%% ========================================================================


function internal_test_5()
fprintf(2,'train_circle_detector: running internal_test_5 : TWO IMAGES TWO OPTIONS PATTERN SEARCH \n')
training_set = {'/mnt/stonfs/FRANJO/Me/CircleGSDatabase/Pat25_2D-DSA_AP.mat',...
    '/mnt/stonfs/FRANJO/Me/CircleGSDatabase/Pat25_2D-DSA_LAT.mat'};
training_gs = training_set;
opt_names = {'sobel_threshold';'pairs_per_circle_threshold'};
opt_bounds = {[50 150];[1 16]};
detector = 'spread';

%local opt options
local_opt.ScaleMesh = 'off'; % this MUST be off because of MATLAB bug
% local_opt.CompletePoll = 'on';
% local_opt.CompleteSearch = 'on';
local_opt.TolMesh = 1;


% local_opt.TolX = 0.5;

local_opt.TolFun = 0.01;
local_opt.Display = 'iter';
% local_opt.DiffMinChange = 1;
% local_opt.Diagnostics = 'on';

% global opt optoins
global_opt.NumTrialPoints = 1;


try
    train_circle_detector(detector,...
    training_set,training_gs,...
    opt_names,opt_bounds,....
    local_opt,global_opt)
    fprintf(2,'train_circle_detector : internal_test_2 : [ PASS ]\n')
catch the_exception_that_cause_failure
    fprintf(2,'train_circle_detector : internal_test_2 : [ FAIL ]\n')
    the_exception_that_cause_failure.rethrow();
end

end


