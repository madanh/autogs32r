function [varargout] = process_one_projection( PR , detector, varargin )
%PROCESS_ONE_PROJECTION Summary of this function goes here
%   varargin{1} is for database
%   varargin{2} is for optional detector options structure


%% OPTIONS (FOR THE DETECTOR)
%get options
det_opt = feval(detector,'get default options'); %this facility has to be built into detectors

if nargin>3
    % override default options if external options are supplied
    det_opt = unite_options(det_opt,varargin{1});
end

%% DISABLE DIAGNOSTIC VIS
    det_opt = unite_options(det_opt,struct('diagnostic',false));

%% CHECK IF WE DID IT BEFORE
if nargin>2
    results_database = varargin{1};
    
    %get image id
    im_id = [PR.fname,': ',PR.timestamp];
    
    %get detector source ide
    cur_path = fileparts(which('process_one_projection'))
    command = ['cd ',cur_path,';',...
        'cd ../..',';',...
        'export TERM=dumb',';',...
        'git --no-pager log -1 --format="%H" ' ];
    [status,commit_id ]= system(command );
    src_id = [detector,': ',commit_id];
    
    
    
    % combine image detector and options identifiers into a run id
    % string
    run_id = [src_id,im_id,char(13),evalc('disp(det_opt)')];
    if results_database.runs.isKey(run_id);
        marker_coords = results_database.runs(run_id);
    end
end
%% RUN 2D CIRCLE DETECTION WITH DEFAULT PARAMS


if ~exist('marker_coords','var')
    tic;
    marker_coords =feval(detector,PR.im,det_opt); %call circle detector
    fprintf('detector %s took %d to run\n',detector,toc);
    %write to database
    if exist('results_database','var')
        results_database.runs(run_id) = marker_coords;
    end
end

tolerance = 4;
[TP,FP,P] = circle_detector_error_rates(marker_coords,PR.gs.centers, tolerance);


figure; imagesc(PR.im); colormap gray; axis image; hold on;
title(PR.gs_filename)
scatter(PR.gs.XMc./PR.dx+0.5,PR.gs.YMc./PR.dy+0.5,'y+');
showMarkers2D(marker_coords);




end

