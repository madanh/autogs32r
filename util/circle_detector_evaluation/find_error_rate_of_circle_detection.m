function [ precision,recall ] = find_error_rate_of_circle_detection( centers, gs, number_of_true_positives )
%FIND_ERROR_RATE_OF_CIRCLE_DETECTION Summary of this function goes here
%   centers - x,y of circle centers
%   gs - binary segmentation of circles (human)

if ~exist('number_of_true_positives','var')
    number_of_true_positives = 12;
end

tp_plus_fp= size(centers,1);

tp = 0;


for xy = round(centers)' %kinda python style iteration here
    if gs(xy(2),xy(1))
        tp=tp+1;
    end
    
end

precision = tp/tp_plus_fp;
recall = tp/number_of_true_positives;

end

