%% define common part of the query
detector = 'spread';
set_number = 1;

db_folder = '/mnt/stonfs/FRANJO/Me/CircleGSDatabase/';


complete_image_set = dir(db_folder);
complete_image_set(logical(cellfun(@sum,(cellfun(@(x) strcmp(x,{'.','..'}),{complete_image_set.name},'UniformOutput',false)))))=[]; %r
complete_image_set = {complete_image_set.name}.';
complete_image_set = fullfile(db_folder,complete_image_set);
 % pick 15% for crossvalid
start_index = ceil((set_number-1)*0.15*numel(complete_image_set)+1);
stop_index = ceil((set_number)*0.15*numel(complete_image_set));
training_set = complete_image_set(start_index:stop_index); % pick 15% for crossvalid


training_gs = training_set;

%% GET HASHES
tr_set = training_set.';
gs_set = training_gs.';
set_goodnes_function_string = 'evaluate_circle_detector_fmeasure_on_a_set';

det_id = get_hash(which(detector));
im_id = get_hash(strjoin(tr_set));
gs_id = get_hash(strjoin(gs_set));
set_g_fun_id = get_hash(which(set_goodnes_function_string));

% prepare input 
im_id_string = ['{ ',strjoin(im_id,','),' }'];
gs_id_string = ['{ ',strjoin(gs_id,','),' }'];

%% open connection
    dbconfig = loadjson(fullfile(fileparts(which('det_wrapper')),'dbconfig.json'));
    javaaddpath(dbconfig.db_driver_path);
    conn = database(dbconfig.db_name,dbconfig.db_username,dbconfig.db_password,'Vendor',dbconfig.db_vendor,'Server',dbconfig.db_host,'PortNumber',dbconfig.db_port);

%% form the query
% this one gets the maximum goodnes value and corresp options for given
% det,im,gs and goodnes functions
query = [
'select opt,value from set_goodness where (value) in',...
'(select max(value) from set_goodness  where',...
    ' det_id =''',det_id{1},''' and ',... 
    ' im_id <@ ''',im_id_string,''' and ', ' im_id @> ''',im_id_string,''' and ',...
    ' gs_id <@ ''',gs_id_string,''' and ', ' gs_id @> ''',gs_id_string,''' and ',...
    ' set_goodness_fun_id = ''', set_g_fun_id{1} ,...
    ''');'];



%% run the query
res = exec(conn,query);
res = fetch(res);

if ~isempty(res.Message)
   fprintf(2,[mfilename,': cound not fetch the query\n A KEYBOARD IS YOU!!\n']);
   keyboard; 
end
%% populate s from the query results

s = cellfun(@(x) loadjson(char(x)),res.Data(:,1));
s = [s.options];


%% OLD PROCESSING
ideal = s;


ideal_cell = struct2cell(ideal);
ideal_cell = squeeze(ideal_cell);

ideal_mat = cell2mat(ideal_cell);


points = ideal_mat(2:end,:).';
n_points = size(points,1);
d = squareform(pdist(points));

number_of_dimensions =  size(points,2);
neighborhood_threshold = sqrt(number_of_dimensions)+eps;

neighbors = sparse(d<=neighborhood_threshold);

[S,C] = graphconncomp(neighbors);

disp('BEHOLD, the connected components!')
disp(fieldnames(s).')
for i = 1:S
    disp(points(C==i,:));
end

%% TODO: EXPAND THE IDEAL REGION!

%% GET AN AD_HOC VALIDATAION SET

val_set = setdiff(complete_image_set,training_set);



%% GET FMEAS ON THE AD_HOC VAL SET
opt_names = fieldnames(s);
opt_names(1) = [];
val_fmeas = -ones(1,n_points);
for i = 1:n_points
    val_fmeas(i) = set_goodness_wrapper('evaluate_circle_detector_fmeasure_on_a_set',detector,val_set.',val_set.',opt_names,points(i,:).')
end
fprintf('training goodness is %0.4f\n',res.Data{1,2})
fprintf('validation goodness is %0.4f\n',mean(val_fmeas(val_fmeas~=-1)))
fprintf('validation goodness is %0.4f\n',max(val_fmeas(val_fmeas~=-1)))
munlock('set_goodness_wrapper');
munlock('det_wrapper');


