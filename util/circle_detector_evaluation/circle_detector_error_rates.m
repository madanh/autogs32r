function [varargout] =  circle_detector_error_rates(centers,gs, tolerance)
%calculate what's necessary for precision and recall of circle detector 
%based on gold standard circle center coordinates and detector output

if numel(centers)==0
    varargout = {0,0,size(gs,1)};
    return
end
d = pdist2(centers,gs);
tolerant = d<=tolerance;
TP  = sum(any(tolerant));
FP = sum(~any(tolerant,2));
% duplicate_TP =sum(sum(tolerant,2)>1);
P = size(gs,1);

varargout = {TP,FP,P};
end