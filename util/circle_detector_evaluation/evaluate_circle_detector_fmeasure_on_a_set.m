function [ out_f_measure ] = evaluate_circle_detector_fmeasure_on_a_set( detector_string,...
tr_set,tr_gs,opt_names,varargin)
% fprintf('%s: function entered\n',mfilename());
%EVALUATE_CIRCLE_DETECTOR_FMEASURE_ON_A_SET Find average F1 measure of a circle
%detector over a set of images and associated gold standard marker center
%positions
%
% INPUTS:
%   detector_string - name circle detector function with signature centers = detector(im, opt)
%   tr_set - cell of paths to image files
%   tr_gs - cell of paths to gold standrad center files
%   opt_names - option names of the detector opt input, values of which
%   are (to be) supplied in varargin. This consturction is necessary to
%   enable usage of standard optimization functions
%
% OUTPUTS: F1 meausure
%
% IMPLICITS: tolerance (in px)
%
% Created by Madan Hennadii madanh2014@gmail.com October 2015


%% DEV INFO
% TEST WITH :
% evaluate_circle_detector_fmeasure_on_a_set('spread',...
% {'/mnt/stonfs/FRANJO/Me/CircleGSDatabase/Pat25_2D-DSA_AP.mat','/mnt/stonfs/FRANJO/Me/CircleGSDatabase/Pat25_2D-DSA_LAT.mat'},...
% {'/mnt/stonfs/FRANJO/Me/CircleGSDatabase/Pat25_2D-DSA_AP.mat','/mnt/stonfs/FRANJO/Me/CircleGSDatabase/Pat25_2D-DSA_LAT.mat'},...
% 'radius',{9})

% TODO:
% * - when reporting is implemented - save all diagnostic info! Esp. if
% HDF5 or mat is used
%% INPUT CHECKS
    assert(ischar(tr_set{1}),['training set should be cell of strings,'...
    'the in_memory variables varian implementation pending']);
    assert(ischar(tr_gs{1}),['training gold standard should be cell of strings,'...
    'the in_memory variables varian implementation pending']);
%% DEFAULTS


    opt.tolerance = 3; %px

%% reformat input into more palatable stuff
    tr_set=tr_set(:);
    tr_gs=tr_gs(:);

%% create detector options structure
    
    
    detector_opt = cell2struct(num2cell(varargin{1}),opt_names,1);
    detector_opt.diagnostic = false;
%% iterate over the traning set
    TP = -ones(size(tr_set));
    FP = TP;
    P = TP;

    for i = 1:numel(tr_set)

        
      
        

        % TODO: check DB for existence or record with current params

        
        centers = feval('det_wrapper',detector_string,tr_set{i},detector_opt);
        load(tr_gs{i},'gs');
        [TP(i),FP(i),P(i)] = circle_detector_error_rates(centers,gs, opt.tolerance);
        
        % TODO: report (push to db)
    end
%% return average Fmeasure
        precision = TP./(TP+FP);
        precision(isnan(precision))=0;
        recall = TP./P;
  
        fmeas = harmmean([precision,recall],2); %fmeasures of individual runs
        
        out_f_measure = mean(fmeas,1); % average of f1-measures of individual runs
        if isnan(out_f_measure)
            fprintf(2,'%s : nans in output, please investigate\n',mfilename());
            fprintf(2,'%s : A KEYBOARD IS YOU!\n',mfilename());
            keyboard;
        end
end

