%% PARAM-PARAM-PAM
volfolder = '/home/madanh/HybridDatabase/Patient 28/SE000008';
angulationfile = '/home/madanh/HybridDatabase/Patient 28/XA000000';
outfile = ['res',sprintf('%04d_',round(clock)),'.mat'];

delta_theta_values = 170:-20:10; % [degrees]
delta_theta_tolerance = 1; % [degrees]

max_sample_size_per_bin = 50; %maximum number of times to run GS creation for given angulation
sampling = 'sequential'; % 'sequential'|'random'

%% read the config
configFileName = fullfile(fileparts(mfilename),'config.ini');
% geomFileName = 'geometry.ini';
config = readConfig(configFileName);
% geom =  readConfig(geomFileName);




%% Get PA angles for all files
info = dicominfo(angulationfile);
thetas = info.PositionerPrimaryAngleIncrement;
nangles = numel(thetas);

pd =squareform(pdist(thetas));

%% Peruse the create32r "overloads" and store stuff in memory
% projections (all of them)
allproj = arrayfun(@(x) loadOneProjection(angulationfile,x),1:nangles,'UniformOutput',false);

% volume
vol = readCTFolder(volfolder,config.permute_xy_in_ct_to_match_matlab_indexing);


%% put a newline in the output file 
% in order to differentiate between runs
dlmwrite('errors.csv','\n','-append');

%% cicle through delta_theta
for delta_theta = delta_theta_values
    
    % pick projections approximately delta_theta apart
    good_pairs = abs(pd-delta_theta)<=delta_theta_tolerance;
    
    
    [row,col] = find(good_pairs);
    %dedupe
    dupes = row<col;
    row = row(dupes);
    col = col(dupes);
    rc = [row,col];
    
    %sample
    sample_size_per_bin = min(max_sample_size_per_bin,size(rc,1));
    
    if strcmpi(sampling,'random')
        the_sample = datasample(rc,sample_size_per_bin,1,'Replace',false);
    else
        the_sample  = rc(1:sample_size_per_bin,:);
    end
    
    % call create32rgs for each pair of images
    if exist('errors','var')
        clear errors
    end
    
    for the_pair = the_sample.'
        [vol, pr1, pr2,error]= create32rgs( vol, allproj{the_pair(1)}, allproj{the_pair(2)});
        allproj{the_pair(1)} = pr1;
        allproj{the_pair(2)} = pr2;
        if exist('errors','var')
            errors = vertcat(errors,error);
        else
            errors = error;
        end
    end
    
    % save the stats (just dump all in a dict, will process later with a separate script)
    try
        load(outfile,'res');
        res(delta_theta) = errors;
    catch
        res = containers.Map(delta_theta,errors);
    end
    save(outfile,'res');
end



