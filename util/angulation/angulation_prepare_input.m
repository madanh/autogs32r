dbfolder = '/mnt/stonfs/FRANJO/Me/HybridDatabase/';
the_expr = [dbfolder,'Pati*/XA*'];
[~,names] =system(['ls -1 ',the_expr]);
names = strsplit(names,'\n');
names = names(cellfun(@(a) strncmp(a,dbfolder,numel(dbfolder)),names));

for name = names
    disp(name)
    info = dicominfo(name{1});
    n_frames = numel(info.PositionerPrimaryAngleIncrement);
    t_info = info;
    t_info = rmfield(t_info,'PositionerPrimaryAngleIncrement');
    t_info = rmfield(t_info,'PositionerSecondaryAngleIncrement');
    for j = 1:n_frames
        im = dicomread(name{1},'frames',j);
        t_info.PositionerPrimaryAngle = info.PositionerPrimaryAngle+...
            info.PositionerPrimaryAngleIncrement(j);
        t_info.PositionerSecondaryAngle = info.PositionerSecondaryAngle+...
            info.PositionerSecondaryAngleIncrement(j);

        
        patient_folder = fileparts(name{1});
        out_dir_name = fullfile(patient_folder,'angulation');
        if ~exist(out_dir_name,'dir')
            mkdir(out_dir_name);
        end
        
        out_fname = fullfile(out_dir_name,['angulation',num2str(j,'%04d')]);
        dicomwrite(im,out_fname,t_info,'CreateMode','Copy');
        
    end
    
end
