function [closestOrder ] = identify( in2d, vol )
%IDENTIFY Summary of this function goes here
%   Detailed explanation goes here

diagnostic = 1;
%%      [ ] Get projection matrices
    SOD = in2d.DistanceSourceToPatient;
    SID = in2d.DistanceSourceToDetector;
    PA = in2d.PrimaryAngle;
    SA = in2d.SecondaryAngle;
    
    u0 = in2d.dx*(size(in2d.im,2))/2;
    v0 = in2d.dy*(size(in2d.im,1))/2;
    % overwrite u0 v0 if in2d is an already registered projection
    if isfield(in2d,'PrincipalPointUX')
        u0 = in2d.PrincipalPointUX;
        v0 = in2d.PrincipalPointVY;
    end
    
    %% Get from CT intristic to Carm (Shechter) coordinates
    [nx,ny,nz] = size(vol.data);
    centerCoordsWorld= ((size(vol.data)-1).*[vol.dx vol.dy vol.dz])/2;
    tx = centerCoordsWorld(1);
    ty = centerCoordsWorld(2);
    tz = centerCoordsWorld(3);
    ctToWorld = [vol.dx 0 0 -vol.dx;
                    0 vol.dy 0 -vol.dy;
                    0 0 vol.dz -vol.dz;
                    0 0 0 1];

%     worldToCarm =   [1 0 0 tx;
%                         0 0 1 tz;
%                         0 1 0 ty;
%                         0 0 0 1]  %change the translations to real ones if needed

                
                
                
                
    worldToCarm =   [ 1  0 0 0;
                        0 0 -1 0;
                        0 1 0 0;
                        0 0 0 1] ; %change the translations to real ones if needed
    homogeneousMarkerCoordsCt = [vol.markerIntristicCoords3D';ones(1,size(vol.markerIntristicCoords3D,1))];
%     homogeneousMarkerCoordsCarm = worldToCarm*ctToWorld*homogeneousMarkerCoordsCt ;


    
    homogeneousMarkerCoordsCarm = vol.Tct_reg*ctToWorld*homogeneousMarkerCoordsCt;
    [P,R,t] = Carm_Shecter(SOD,SID,PA,SA,-u0,-v0);
    
    u = P*homogeneousMarkerCoordsCarm;
    u1=u(1,:)./u(3,:);
    u2=u(2,:)./u(3,:);
    
    im1 = u1./in2d.dx;
    im2 = u2./in2d.dy;
    projectedMarkerCoords=[im1',im2'];
%% TODO:ICP
    
    
%% MATCHING
    
    [closestOrder,closestDistance]= closest(in2d.markerIntristicCoords2D,projectedMarkerCoords);
    threshold = 15*2;% remove points that are further than 2 radii %TODO:MOVE TO CONFIG
    validMarkers = closestDistance<threshold;
    closestOrder(closestDistance>threshold) = -1; %unnmatched are marked with -1 values
    
    %Check if any 2d markers match multiple projected markers
   for i = 1:size(in2d.markerIntristicCoords2D,1) %projectedMarkerCoords
        if validMarkers(i)
            value = closestOrder(i);
            ind = find(closestOrder(validMarkers) ==value);
            if numel(ind)>1
                [~,minInd] = min(closestDistance(ind));
                notClosest = closestOrder(ind~=ind(minInd));
                closestOrder(notClosest) = -1;
                validMarkers(notClosest) = 0;
            end
        end
    end
    
%% vis

if diagnostic
    figure;
    imagesc(in2d.im);
    hold on ;
    scatter(im1,im2);
    for i = 1:size(in2d.markerIntristicCoords2D,1) %projectedMarkerCoords
        if validMarkers(i)
        line([projectedMarkerCoords(closestOrder(i),1),in2d.markerIntristicCoords2D(i,1)],...
            [projectedMarkerCoords(closestOrder(i),2),in2d.markerIntristicCoords2D(i,2)],'LineWidth',2);
        end
    end
end
  x=im1;
  y=im2;


end

function [closestOrder,varargout ]= closest(ref,mov)
    %assuming ref and mov have 2 columns
    closestOrder=zeros(size(ref,1),1);
    varargout{1}=closestOrder;
     for i=1:size(ref,1)
         %distances from all points in mov to the ith point in ref
         d = mov-repmat(ref(i,:),size(mov,1),1); 
         distance = sqrt(dot(d',d'))';
         [varargout{1}(i) ,closestOrder(i)]= min(distance);
     end
         
    
end
