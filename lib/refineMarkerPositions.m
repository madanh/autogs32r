function [outOPR,outWPR]=refineMarkerPositions(OPR,WPR,vol)
diagnostic = true;
% loop through marker indeces of 3d markers to get only markers relevant to
% calibration

outOPR = OPR;
outWPR = WPR;
for i = 1:size(vol.markerIntristicCoords3D,1)
    inOPR = find(OPR.To3DMarkerCorrespondence==i);
    inWPR = find(WPR.To3DMarkerCorrespondence==i);
    
    if ~isempty(inOPR) && ~isempty(inWPR) % only these markers will be used in calibration
        outOPR.markerIntristicCoords2D(inOPR,:)=getSubpixelCenter(OPR,inOPR);
        if diagnostic
        saveas(gca,strcat('./diag/OPR',num2str(inOPR),'.png'));
        end
        outWPR.markerIntristicCoords2D(inWPR,:)=getSubpixelCenter(WPR,inWPR);
        if diagnostic
        saveas(gca,strcat('./diag/WPR',num2str(inOPR),'.png'));
        end
        
    end
end
end

