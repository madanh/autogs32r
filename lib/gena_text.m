function h = gena_text( a,varargin )
%GENA_SHOW Summary of this function goes here
%   Detailed explanation goes here
spec = num2str([1:size(a,1)]');
if nargin >1 && ~isempty(varargin{1})
    spec = varargin{1};
end

if size(a,2)<3
  h = text(a(:,1),a(:,2),spec,varargin{2:end});%'BackgroundColor',[0.6  0.6 0]);  
else
    h = text(a(:,1),a(:,2),a(:,3),spec,varargin{2:end});
end


end

