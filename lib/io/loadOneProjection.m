function PR = loadOneProjection(fname,n)
    %Reads the n-th frame of a given DICOM sequence
    if nargin<2
        n=1;
    end
    % read the first frame
    PR.im = dicomread(fname,'frames',n);
    
    % read and pack the used metadata
    info = dicominfo(fname);
    PR.dx=info.ImagerPixelSpacing(1);
    PR.dy=info.ImagerPixelSpacing(2);
    PR.PositionerPrimaryAngle = info.PositionerPrimaryAngle;%+info.PositionerPrimaryAngleIncrement(ind);
    PR.PositionerSecondaryAngle = info.PositionerSecondaryAngle;%+info.PositionerSecondaryAngleIncrement(ind);
    PR.DistanceSourceToDetector = info.DistanceSourceToDetector;
    PR.DistanceSourceToPatient = info.DistanceSourceToPatient;
    PR.PatientOrientation = info.PatientOrientation;
    if isfield(info,'PositionerPrimaryAngleIncrement')
        PR.PrimaryAngle = info.PositionerPrimaryAngle +info.PositionerPrimaryAngleIncrement(n);
    else
        PR.PrimaryAngle = info.PositionerPrimaryAngle;
    end
    if isfield(info,'PositionerSecondaryAngleIncrement')
        PR.SecondaryAngle = info.PositionerSecondaryAngle + ...
            info.PositionerSecondaryAngleIncrement(n);
    else
        PR.SecondaryAngle = info.PositionerSecondaryAngle;
    end
    
    % read and pack filesystem info
    PR.fname = fname;
    l = dir(fname);
    PR.timestamp = l.date;
    PR = unflip_projection(PR);
    
    
end

function PR = unflip_projection(PR)
%see issue33 in repo https://bitbucket.org/madanh/harness
tmp = strsplit(PR.PatientOrientation,'\\');
[row_direction,col_direction] = tmp{:};

switch row_direction
    case {'R','L'}
        if abs(PR.PositionerPrimaryAngle)<90
            RLflipcrit = 1;
        else
            RLflipcrit = -1;
        end
        
        if strcmpi(row_direction,'L')
            RL = 1;
        else
            RL = -1;
        end
        to_flip_or_not_to_flip = RL*RLflipcrit;

    case {'A','P'}
        APflipcrit = sign(PR.PositionerPrimaryAngle);
        if strcmpi(row_direction,'P')
            AP = 1;
        else
            AP = -1;
        end
        to_flip_or_not_to_flip = AP*APflipcrit;
    otherwise
        ME = MException('unflip2D:not implemented',['unflip2D:processing of Patient orientation value "',...
            row_direction,'" is not implemented']);
        throw (ME);
end

if to_flip_or_not_to_flip<0
    PR.im = fliplr(PR.im);
    PR = toggle_dicom_row_orientation(PR);
    PR.has_been_flipped_lr = 1;
else
    PR.has_been_flipped_lr = 0;
end

end

function PR =toggle_dicom_row_orientation(PR)
%see issue33 in repo https://bitbucket.org/madanh/harness
tmp = strsplit(PR.PatientOrientation,'\\');
[row_direction,col_direction] = tmp{:};
switch row_direction
    case 'R'
        o = 'L';
    case 'L'
        o = 'R';
    case 'A'
        o = 'P';
    case 'P'
        o = 'A';
    otherwise
        ME = MException('toggle_dicom_row_orientation:not implemented',['toggle_dicom_row_orientation:processing of Patient orientation value "',...
            row_direction,'" is not implemented']);
        throw (ME);
end
PR.PatientOrientation = [o,'\',col_direction];


end
