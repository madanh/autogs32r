function [vol] = readCTFolder(inPatientFolder,varargin)

% do we want to permute x and y when reading, to match indexing in images
% and matlab?
xy_permute = 1;
if nargin>1
    if ~varargin{1}
        xy_permute = 0;
    end
end

% load list of slice filenames
names = dir(inPatientFolder);
names([names.isdir]) = [];
names = {names.name};
N = length(names);

% if ~strcmp(im_type,'RAW')
    info_1 = dicominfo(fullfile(inPatientFolder,names{1}));    
    vol.data = zeros(info_1.Width,info_1.Height,N);
    for i=1:N

        info = dicominfo(fullfile(inPatientFolder,names{i}));
%% DIMENSIONS MANIPULATION HERE
        if xy_permute % permute x and y to match matlab indexing (default)
            X = permute(dicomread(info),[2 1]); 
        else
            X = dicomread(info); 
        end
        vol.data(:,:,info.InstanceNumber) = X;

    end
    
    

    vol.dx = info.PixelSpacing(1); %dx
    vol.dy = info.PixelSpacing(2); %dy
    vol.dz = info.SliceThickness; %dz
    vol.ImageOrientationPatient = info_1.ImageOrientationPatient;
        % get explicit direction cosines from ImageOrientationPatient
        % cosine number refers to dimension number in matlab indexing
        % matlab indexing is [r,c,s]
        vol.cos1 = vol.ImageOrientationPatient(1:3);
        vol.cos2 = vol.ImageOrientationPatient(4:6);
        vol.cos3 = cross(vol.cos1,vol.cos2);
        
    vol.ImagePositionPatient = info_1.ImagePositionPatient;
    vol.LastSlicePostion = info.ImagePositionPatient;
    vol.toPatientMatrix= [vol.dx.*vol.cos1,vol.dy.*vol.cos2,vol.dz.*vol.cos3,vol.ImagePositionPatient;[0 0 0 1]]; 
    vol.inPatientFolder = inPatientFolder;
    
    
    
