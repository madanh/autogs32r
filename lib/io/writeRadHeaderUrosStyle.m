function [ st] = writeRadHeaderUrosStyle( inFullFileName, h )
%WRITEHEADERUROSSTYLE Write the header in the .rad format de-facto used by
%Uros
%   There's a .rad format that is used for historical reasons and this
%   function implements write for it. It was actually copied and pasted 
%   here from one of Uros's scripts and some changes made in variable 
%   names.
%
%   second argumet h is a header structure
%
%   v.0.0.0 2014-10-03 Hennadii Madan

  
    fidHdr = fopen ( inFullFileName, 'wt' );    
   try
    fprintf(fidHdr, 'Size[pixels]: ');
    fprintf(fidHdr, '%4g ', h.M );
    fprintf(fidHdr, '%4g\n',h.N );
    fprintf(fidHdr, 'Step[mm]: ');
    fprintf(fidHdr, '%4g ', h.dx );
    fprintf(fidHdr, '%4g\n',h.dy );
    fprintf(fidHdr, 'DataType: ');
    fprintf(fidHdr, '%s\n', h.DataType );
    fprintf(fidHdr, '%s\n', 'TPosition:' );
    fprintf(fidHdr, '%9f %9f %9f %9f\n',h.Txf(1,1),h.Txf(1,2),h.Txf(1,3),h.Txf(1,4) );
    fprintf(fidHdr, '%9f %9f %9f %9f\n',h.Txf(2,1),h.Txf(2,2),h.Txf(2,3),h.Txf(2,4) );
    fprintf(fidHdr, '%9f %9f %9f %9f\n',h.Txf(3,1),h.Txf(3,2),h.Txf(3,3),h.Txf(3,4) );
    fprintf(fidHdr, '%1g %1g %1g %1g\n\n',0,0,0,1 );
    fprintf(fidHdr, '%s %9f %9f %9f', 'SourcePosition[mm]:',h.xsf,h.ysf,h.zsf );
    st = fclose(fidHdr);
   catch
       st = fclose(fidHdr);
   end
    


end
