function [ config ] = readConfig( configFileName )
%READCONFIG Summary of this function goes here
%   Detailed explanation goes here
%% READ THE CONFIG FILE FIELD-WISE
try
    [readsett,result] = inifile(configFileName,'readall');
    for k = 1:size(readsett,1)
        if ~isnan(str2double(readsett{k,4}))
            config.(readsett{k,3})=str2double(readsett{k,4});
        else
            config.(readsett{k,3})=readsett{k,4};
        end
    end
catch
    warning(['unable to read',configFileName, 'using default configuration values']);
end

end

