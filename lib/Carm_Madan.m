function [P,R,t] = Carm_Madan(SOD,SID,PA,SA)
%% 0 ROTATIONS IN PCS TO ALIGN Oy- with the cameras third axis
PA = PA/180*pi; %primary and secondary angles to radians
SA = SA/180*pi;

Rpa = [cos(-PA) -sin(-PA) 0;sin(-PA) cos(-PA) 0; 0 0 1]; %primary rotation matrix
%get secondary rotation matrix vvv
% if PA == 0    
%     Rsa = [1 0 0;0 cos(SA) -sin(SA);0 sin(SA) cos(SA)]; % WHY WE NEED THIS SPECIAL CASE, TO SAVE 5microseconds?? HM
% else
%     v = Rpa'*[1;0;0];
%     [u(1,1),u(2,1),u(3,1)] = unit_vector(v(1),v(2),v(3));    % WHY?? It should already be unit! HM
%     Rsa = u*u' + cos(SA)*(eye(3) - u*u') + sin(SA)*[0 -u(3) u(2);u(3) 0 -u(1);-u(2) u(1) 0];    
% end
Rsa = [1 0 0;
       0 cos(SA) -sin(SA);
       0 sin(SA) cos(SA)];


R = Rsa*Rpa; %combined rotation matrix
t = [0;0;0]; %<========Different from SCHECTER!!

positionerRotationMatrix = [[R t];[0 0 0 1]]; %transform matrix in affine coordinates



%% 1 go to the following CS: origin-X-ray source, 3rd axis - proj direction
% 1st and 2nd aes colinear with X-ray image axes (u,v)
toSKsiIpsilonZetaMatrix =[1 0 0 0;
                        0 0 -1 0;
                        0 -1 0 +SOD;
                        0 0 0 1];
                    
%% 2 Projection matrix along Szeta axis (third) in mm!
toUVprimeMatrix = [1 0 0 0;
             0 1 0 0;
             0 0 1/SID 0];

                
%% COMPOSE 0,1,2,3
P = toUVprimeMatrix*toSKsiIpsilonZetaMatrix*positionerRotationMatrix;
                
