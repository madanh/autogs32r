%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% CALCULATION OF 4x4 TRANSFORM MATRIX BASED ON CORRESPONDING POINTS
% see Eggert et al. 1997
%
% X1 - points that needs to be transformed
% X2 - target points
% T - transform matrix that maps points X1 to X2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function T = svdT(X1,X2)

% number of corresponding points
N = size(X1,1);

MXm = X1' - repmat(mean(X1',2),1,N);
MXRm = X2' - repmat(mean(X2',2),1,N);

[U,~,V] = svd(MXm*MXRm');

% rotation
R = V*U';
if det(R)<0
    p = eye(3);
    p(3,3) = det(U*V');
    R = U*p*V';
end

T = eye(4);
T(1:3,1:3) = R;
T(1:3,4) = mean(X2',2) - R*mean(X1',2);