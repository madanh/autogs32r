% Brent�s method to find a minimum.
function [xmin, fmin] = brent(ax,bx,cx,func,tol,itermax)

ITMAX=100;
CGOLD=0.3819660;
ZEPS=1e-4;

if nargin<6, itermax = ITMAX; end

% Here ITMAX is the maximum allowed number of iterations; CGOLD is the golden ratio;
% and ZEPS is a small number that protects against trying to achieve fractional accuracy
% for a minimum that happens to be exactly zero.
d=0.0;
e=0.0; 
if (ax < cx), a = ax; else a = cx; end
if (ax > cx), b = ax; else b = cx; end
x=bx;
w=bx;
v=bx;

fw=func(x);
fv=fw;
fx=fw;

% Main program loop.
for iter = 0 : itermax
    xm=0.5*(a+b);
    tol1=tol*abs(x)+ZEPS;
    tol2=2.0*tol1;
    if (abs(x-xm) <= (tol2-0.5*(b-a)))
        fmin=fx;
        xmin=x;
        return;
    end
    
    % Construct a trial parabolic fit.
    if (abs(e) > tol1)
        r=(x-w)*(fx-fv);
        q=(x-v)*(fx-fw);
        p=(x-v)*q-(x-w)*r;
        q=2.0*(q-r);
        if (q > 0.0), 
            p = -p; 
        end
        q=abs(q);
        etemp=e;
        e=d;
    
        if (abs(p) >= abs(0.5*q*etemp)) || (p <= (q*(a-x))) || (p >= q*(b-x))
            if x >= xm, 
                e=a-x; 
            else
                e=b-x; 
            end
            d=CGOLD*e;
            % The above conditions determine the acceptability of the parabolic fit. Here
            % we take the golden section step into the larger of the two segments.
        else
            d=p/q; %Take the parabolic step.
            u=x+d;
            if (u-a < tol2 || b-u < tol2) 
                d=mysign(tol1,xm-x); 
            end
        end
    else
        if x >= xm, 
            e=a-x; 
        else
            e=b-x; 
        end        
        d=CGOLD*e;
    end

    if abs(d) >= tol1, u = x + d; else u = x + mysign(tol1,d); end
    fu=func(u);

    % This is the one function evaluation per iteration.
    if (fu <= fx)
        if (u >= x) a=x; else b=x; end
        [v,w,x,u] = shft3(v,w,x,u);
        [fv,fw,fx,fu] = shft3(fv,fw,fx,fu);
    else 
        if (u < x) 
            a=u; 
        else
            b=u; 
        end
        
        if (fu <= fw || w == x)
            v=w;
            w=u;
            fv=fw;
            fw=fu;
        elseif (fu <= fv || v == x || v == w) 
            v=u;
            fv=fu;
        end
    end

end

warning('Too many iterations in Brent line search!');

function [a b c d] = shft3(a, b, c, d)

a=b; 
b=c;
c=d;

function ret = mysign(a,b)

if b>=0
    if a>=0, ret = a;
    else ret = -a; end
else
    if a>=0, ret = -a;
    else ret = a; end
end

