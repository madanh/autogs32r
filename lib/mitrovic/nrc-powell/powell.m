% Multidimensional minimization by Powell's method.
% p = powell(pp, func, 1e-4, 200)
function [p,fret,iter] = powell(pp, func, ftol, itermax, outfcn, bplot)

ITMAX=200; % Maximum allowed iterations.
TINY=1.0e-25; % A small number.

if nargin<6, bplot = false; end
if nargin<5, outfcn = []; end
if nargin<4, itermax = ITMAX; end

p=pp;
n = numel(pp);
ximat = eye(n);
xi = ones(n,1);
pt = zeros(n,1);
ptt = zeros(n,1);
fret=func(p);
pt=p; % Save the initial point.

iter = 0;
while true
    
    % Output optimization steps
    if ~isempty(outfcn) 
        if ~iter,         
            outfcn([],[],'init');
        end        
        optimValues.fval = fret;
        outfcn(p,optimValues,'iter');       
    end
    
    % Display optimization output    
    if bplot
        if ~iter, 
            fprintf(1,'\tIter\t\tCriterion\n');
            fprintf(1,'------------------------------\n');            
        end
        fprintf(1,'\t%2d\t\t\t%3.4f\n',iter,fret);        
    end
        
    fp=fret;
    ibig=0;
    del=0.0;  % Will be the biggest function decrease.

    % In each iteration, loop over all directions in the set.    
    for i = 1 : n 
        for j = 1 : n
            xi(j) = ximat(j,i); 
        end
        fptt=fret;
        [p, fret, xi] = linmin(p, xi, func);                    
        if (fptt-fret > del)
            del=fptt-fret;
            ibig=i+1;
        end
    end
     
    % Check stopping conditions.
    if (2.0*(fp-fret) <= ftol*(abs(fp)+abs(fret))+TINY) break; end
%     if (abs(fp-fret) <= ftol) break; end    
    if (iter == itermax) break; end
    iter = iter + 1;
    
    for j = 1 : n
        ptt(j)=2.0*p(j)-pt(j);
        xi(j)=p(j)-pt(j);
        pt(j)=p(j);
    end

    fptt=func(ptt);
    
    if fptt < fp
        t=2.0*(fp-2.0*fret+fptt)*((fp-fret-del)^2)-del*((fp-fptt)^2);
        if t < 0.0             
            [p, fret, xi] = linmin(p, xi, func);            
            for j = 1 : n
                ximat(j,ibig) = ximat(j,n);
                ximat(j,n) = xi(j);
            end
        end
    end    
    
end

if (iter == itermax) 
    warning('Powell exceeding maximum iterations.'); 
end

% Output optimization steps
if ~isempty(outfcn) 
    if ~iter,         
        outfcn([],[],'init');
    end        
    optimValues.fval = fret;
    outfcn(p,optimValues,'iter');
end

% Display optimization output    
if bplot
    fprintf(1,'\t%2d\t\t\t%3.4f\n',iter+1,fret);        
    fprintf(1,'------------------------------\n');                
end

% return p

