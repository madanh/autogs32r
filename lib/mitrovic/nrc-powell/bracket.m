% Bracket method
% [ax bx cx] = bracket(0.0, 1.0, func);
function [ax bx cx varargout] = bracket(a, b, func)

GOLD=1.618034;
GLIMIT=100.0;
TINY=1.0e-20;

ax=a; 
bx=b;		
fa=func(ax);
fb=func(bx);		
if (fb > fa) 
    [ax,bx] = SWAP(ax,bx);
    [fb,fa] = SWAP(fb,fa);		
end	
cx=bx+GOLD*(bx-ax);
fc=func(cx);

while fb > fc
    r=(bx-ax)*(fb-fc);
    q=(bx-cx)*(fb-fa);
    u=bx-((bx-cx)*q-(bx-ax)*r)/(2.0*mysign(max(abs(q-r),TINY),q-r));
    ulim=bx+GLIMIT*(cx-bx);
    if ((bx-u)*(u-cx) > 0.0)
        fu=func(u);
        if (fu < fc)
            ax=bx;
            bx=u;
            fa=fb;
            fb=fu;
            return;
        elseif (fu > fb)
            cx=u;
            fc=fu;
            return;
        end
        u=cx+GOLD*(cx-bx);
        fu=func(u);
    elseif ((cx-u)*(u-ulim) > 0.0)
        fu=func(u);
        if (fu < fc)
            [bx,cx,u] = shft3(bx,cx,u,u+GOLD*(u-cx));
            [fb,fc,fu] = shft3(fb,fc,fu,func(u));
        end
    elseif ((u-ulim)*(ulim-cx) >= 0.0)
        u=ulim;
        fu=func(u);
    else
        u=cx+GOLD*(cx-bx);
        fu=func(u);
    end
    [ax,bx,cx] = shft3(ax,bx,cx,u);
    [fa,fb,fc] = shft3(fa,fb,fc,fu);
end

if nargout>3, varargout{1} = fa; end
if nargout>4, varargout{2} = fb; end
if nargout>5, varargout{3} = fc; end
		
function [ax bx] = SWAP(ax, bx)
        tmp = ax;
        ax = bx;
        bx = tmp;

function [a b] = shft2(a, b, c)
    a=b;
    b=c;

function [a b c] = shft3(a, b, c, d)
    a=b;
    b=c;
    c=d;
function [a b c] = mov3(d, e, f)
    a=d; 
    b=e; 
    c=f;
		
function ret = mysign(a,b)

if b>=0
    if a>=0, ret = a;
    else ret = -a; end
else
    if a>=0, ret = -a;
    else ret = a; end
end		
