% Linemethod wrapper
% [p, fmin, xi] = linmin(p, xi, func)
function [p, fmin, xi] = linmin(p, xi, func)

f1dim = @(x) func(p + x .* xi);
ax=0.0;
xx=3.0;

[ax bx cx] = bracket(ax, xx, f1dim);
[xmin, fmin] = brent(ax, bx, cx, f1dim, 1e-4);

xi = xi .* xmin;
p = p + xi;
