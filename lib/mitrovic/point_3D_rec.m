%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% RECONSTRUCTION OF 3D POINT USING EPIPOLAR GEOMETRY
% see Dumay et al. 1994
%
% X0R,Y0R,Z0R - coordinates of reconstructied 3D point in world coordinate system
% xs,ys,zs - coordinates of focal points in world coordinate system
% x0,y0,z0 - coordinates of projected 3D point in world coordinate system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%#codegen


function [x0R,y0R,z0R,errors] = point_3D_rec(xs,ys,zs,x0,y0,z0)
ex = zeros(1,2);
ey = ex;
ez = ex;
for i = 1:2
    [ex(i),ey(i),ez(i)] = unit_vector(x0(i)-xs(i),y0(i)-ys(i),z0(i)-zs(i));
end

% calculation of 3D reconstructed point
t = (-sum([ex(1) ey(1) ez(1)].*[xs(2) ys(2) zs(2)]) + ...
    sum([ex(1) ey(1) ez(1)].*[ex(2) ey(2) ez(2)])*(sum([ex(2) ey(2) ez(2)].*[xs(2) ys(2) zs(2)])...
    - sum([ex(2) ey(2) ez(2)].*[xs(1) ys(1) zs(1)]))...
    + sum([ex(1) ey(1) ez(1)].*[xs(1) ys(1) zs(1)]))./(sum([ex(1) ey(1) ez(1)].*[ex(2) ey(2) ez(2)])^2 - 1);
s = -sum([ex(2) ey(2) ez(2)].*[xs(2) ys(2) zs(2)]) +...
    sum([ex(2) ey(2) ez(2)].*[xs(1) ys(1) zs(1)]) + ...
    t*sum([ex(1) ey(1) ez(1)].*[ex(2) ey(2) ez(2)]);

% vector S1S2
S1S2x = xs(2) + s*ex(2) - xs(1) - t*ex(1);
S1S2y = ys(2) + s*ey(2) - ys(1) - t*ey(1);
S1S2z = zs(2) + s*ez(2) - zs(1) - t*ez(1);

% reconstructed 3D point
x0R = xs(1) + t*ex(1) + 0.5*S1S2x;
y0R = ys(1) + t*ey(1) + 0.5*S1S2y;
z0R = zs(1) + t*ez(1) + 0.5*S1S2z;

errors=sqrt(S1S2x.^2+S1S2y.^2+S1S2z.^2)/2;
