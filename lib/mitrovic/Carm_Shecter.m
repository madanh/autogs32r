function [P,R,t] = Carm_Shecter(SOD,SID,PA,SA,u0,v0)
%%
% comments by Hennadii Madan 2014.06.06
PA = PA/180*pi; %primary and secondary angles to radians
SA = SA/180*pi;

Rpa = [cos(PA) 0 sin(PA);0 1 0;-sin(PA) 0 cos(PA)]; %primary rotation matrix
%get secondary rotation matrix vvv
if PA == 0    
    Rsa = [1 0 0;0 cos(SA) -sin(SA);0 sin(SA) cos(SA)];
else
    v = Rpa'*[1;0;0];
    [u(1,1),u(2,1),u(3,1)] = unit_vector(v(1),v(2),v(3));    
    Rsa = u*u' + cos(SA)*(eye(3) - u*u') + sin(SA)*[0 -u(3) u(2);u(3) 0 -u(1);-u(2) u(1) 0];    
end

R = Rpa*Rsa; %combined rotation matrix
t = [0;0;-SOD]; %translation

P = [-SID 0 -u0;0 -SID -v0;0 0 1]*[R t]; %transform matrix in affine coordinates

