function [ix,iy] = proj_skel(XCTOp,YCTOp,ZCTOp,Tx,dx,dy,xs,ys,zs,Tct)

% equations of detector plane in world coordinate system
P(1,:) = Tx*[0;0;0;1];
P(2,:) = Tx*[1;0;0;1];
P(3,:) = Tx*[0;1;0;1];    

N = length(XCTOp);
ix = zeros(N,1);
iy = zeros(N,1);
for z = 1:N
        
    % transforming the position of centerline point from CT to
    % world coordinate system
    pCT = Tct*[XCTOp(z);YCTOp(z);ZCTOp(z);1];

    % finding corresponding gradient on X-ray images   
    [x0,y0,z0] = plane_line_intersection([P(:,1);xs;pCT(1)],[P(:,2);ys;pCT(2)],[P(:,3);zs;pCT(3)]);    

    % transforming back to 2D X-ray image        
    pX = (inv(Tx)*[x0;y0;z0;1])';
    ix(z) = pX(1)/dx+1;    
    iy(z) = pX(2)/dy+1;  

end