%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CALCULATION OF TRANSFORM MATRIX 
% IF RIGID-BODY TRANSFORM PARAMETERS ARE KNOWN (tx,ty,tz,wx,wy,wz)
% 
% tx,ty,tz,wx,wy,wz - rigid-body transform parameters
% T - 4x4 transform matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function T = rigid2T(tx,ty,tz,wx,wy,wz)

wx = pi/180*wx;
wy = pi/180*wy;
wz = pi/180*wz;

T(1,1) = cos(wy)*cos(wz);
T(1,2) = sin(wx)*sin(wy)*cos(wz)-cos(wx)*sin(wz);
T(1,3) = cos(wx)*sin(wy)*cos(wz)+sin(wx)*sin(wz);
T(1,4) = tx;
T(2,1) = cos(wy)*sin(wz);
T(2,2) = sin(wx)*sin(wy)*sin(wz)+cos(wx)*cos(wz);
T(2,3) = cos(wx)*sin(wy)*sin(wz)-sin(wx)*cos(wz);
T(2,4) = ty;
T(3,1) = -sin(wy);
T(3,2) = sin(wx)*cos(wy);
T(3,3) = cos(wx)*cos(wy);
T(3,4) = tz;
T(4,1:3)=0;
T(4,4)=1;
