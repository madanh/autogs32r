%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ALGORITHM FOR FINDING POINT-LINE DISTANCE IN 3D
% http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html
%
% X,Y,Z - x,y,z coordinates of 3 points, 2 points are defining line,
%         and 1 observing point
% d - point-line distance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function d = point_line_distance3D(X,Y,Z)

t = -dot([X(1);Y(1);Z(1)]-[X(3);Y(3);Z(3)],[X(2);Y(2);Z(2)]-[X(1);Y(1);Z(1)])/moduo_vector(X(2)-X(1),Y(2)-Y(1),Z(2)-Z(1))^2;

d = sqrt((X(1)-X(3)+(X(2)-X(1))*t)^2 + (Y(1)-Y(3)+(Y(2)-Y(1))*t)^2 + (Z(1)-Z(3)+(Z(2)-Z(1))*t)^2);