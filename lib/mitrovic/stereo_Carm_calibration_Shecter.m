function [d,xs,ys,zs,Tx,errors] = stereo_Carm_calibration_Shecter(X3D,Y3D,Z3D,X2D,Y2D,dx,dy,IN)

% fprintf(1,'%.f ',IN);
% fprintf(1,'\n');
SOD(1) = IN(1);
SID(1) = IN(2);
PA(1) = IN(3);
SA(1) = IN(4);
u0(1) = IN(5);
v0(1) = IN(6);
SOD(2) = IN(7);
SID(2) = IN(8);
PA(2) = IN(9);
SA(2) = IN(10);
u0(2) = IN(11);
v0(2) = IN(12);

Tx = zeros(4,4,2);
xs = zeros(2,1);
ys = zeros(2,1);
zs = zeros(2,1);
for j = 1:2
   
    [P,R,t] = Carm_Shecter(SOD(j),SID(j),PA(j),SA(j),u0(j),v0(j)); % Get transform from world to Carm coords using C-arm angles data
    
    Twc = eye(4);
    Twc(1:3,1:3) = R;
    Twc(1:3,4) = t; %go to affine coords

    Tcw = inv(Twc); %inverse transform

    % X-ray source`coordinates in world coordinate system
    xs(j) = Tcw(1,4);
    ys(j) = Tcw(2,4);
    zs(j) = Tcw(3,4);   

    % X-ray imaging plane to camera coordinate system
    T2Dc = rigid2T(-u0(j),-v0(j),-SID(j),0,0,0);

    % X-ray imaging plane to woorld coordinate system
    Tx(:,:,j) = Tcw*T2Dc;    
    
end

% reconstruction of 3D points
NM = length(X3D);
x0 = zeros(2,1);
y0 = zeros(2,1);
z0 = zeros(2,1);
XW = zeros(NM,1);
YW = zeros(NM,1);
ZW = zeros(NM,1);
for i = 1:NM
    
    for j = 1:2

        % 2D points in woorld coordinate system
        p = Tx(:,:,j)*[X2D(i,j);Y2D(i,j);0;1];

        x0(j) = p(1);
        y0(j) = p(2);
        z0(j) = p(3);

    end       

    %  3D reconstruction of i-th point on 3D skeleton based on
    %  epipolar geometry in world coordinate system           
    [XW(i,1),YW(i,1),ZW(i,1),errors(i)] = point_3D_rec(xs,ys,zs,x0,y0,z0);
        
end

Tct = svdT([X3D Y3D Z3D],[XW YW ZW]);

X2Dp = X2D;
Y2Dp = Y2D;
for j = 1:2    
    [X2Dp(:,j),Y2Dp(:,j)] = proj_skel(X3D,Y3D,Z3D,Tx(:,:,j),dx(j),dy(j),xs(j),ys(j),zs(j),Tct);    
    X2Dp(:,j) = (X2Dp(:,j)-1)*dx(j);
    Y2Dp(:,j) = (Y2Dp(:,j)-1)*dy(j);
end
dp = (X2D - X2Dp).^2 + (Y2D - Y2Dp).^2;
d = sqrt(1/3/(NM+1)*sum(dp(:)));
