%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODIFYING VECTOR TO UNIT LENGTH
%
% x,x,x - x,y,z components of observed vector
% ux,uy,uz - x,y,z components of unifiied observed vector
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%#codegen

function [ux,uy,uz] = unit_vector(x,y,z)
    n = sqrt(x^2+y^2+z^2);
    ux = x/n;
    uy = y/n;
    uz = z/n;
end
