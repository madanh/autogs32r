%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODUO OF A VECTOR
%
% [Vm] = moduo_vector(X,Y,Z)
%
% X, Y, Z - vector components
% Vm - moduo of a vector
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Vm] = moduo_vector(X,Y,Z)

N=size(X,1);

for i=1:N
    
    Vm(i,1)=sqrt(X(i,1)^2+Y(i,1)^2+Z(i,1)^2);
    
end
