%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREDICTING THE ERROR IN RIGID-BODY POINT-BASED REGISTRATION
% see Fitzpatrick et al. 2001
%
% X3D,Y3D,Z3D - 3D points in 3D image coordinate system
% X2D,Y2D - 2D points in 2D image coordinate system
% Tx - transformation matrix from 2D image to world coordinate system
% xs,ys,zs - position of X-ray sources in world coordinate system
% XCTOp,YCTOp,ZCTOp - target points in 3D image cooridnate system
% mTRE,TRE - estimated target registration erros
% FLE - fiducial localization error
% FRE - fiducial registration error
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [mTRE,TRE,FLE,FRE] = point_reg_error(X3D,Y3D,Z3D,X2D,Y2D,Tx,xs,ys,zs,XCTOp,YCTOp,ZCTOp)

NM = size(X3D,1);
N = size(XCTOp,1);

% reconstruction of markers
for i = 1:NM
   
    for j = 1:2
        
        
        % 2D points in woorld coordinate system
        p = Tx(:,:,j)*[X2D(i,j);Y2D(i,j);0;1];

        x0(j) = p(1);
        y0(j) = p(2);
        z0(j) = p(3);

    end               
        
    % 3D reconstruction of i-th marker based on
    % epipolar geometry in world coordinate system           
    [XWM(i,1),YWM(i,1),ZWM(i,1)] = point_3D_rec(xs,ys,zs,x0,y0,z0);    
    
end
Tct_reg = svdT([X3D Y3D Z3D],[XWM YWM ZWM]);

% 3D markers in world coordinate system
pp = zeros(NM,4);
for i = 1:NM
    pp(i,:) = Tct_reg*[X3D(i);Y3D(i);Z3D(i);1];                
end  

% center of mass of marker configuration in world coordinate system
Xcm = mean(pp(:,1));
Ycm = mean(pp(:,2));
Zcm = mean(pp(:,3));

% 3D skeleton (target points) in world coordinate system
ppS = zeros(N,4);
for i = 1:N
    ppS(i,:) = Tct_reg*[XCTOp(i);YCTOp(i);ZCTOp(i);1];                
end  

% estimation of mTRE based on FRE (see Tomazevic MICCAI 2002)
% RMS distance between real and reconstructed 3D markers (fiducial
% registration error FRE), and estimated fiducial localization error)
FRE = sqrt(sum((pp(:,1) - XWM).^2 + (pp(:,2) - YWM).^2 + (pp(:,3) - ZWM).^2)/NM);
FLE = sqrt(NM/(NM-2))*FRE;

% finding principle axes using PCA
paxe = princomp([pp(:,1) pp(:,2) pp(:,3)]);
TRE = zeros(N,1);
dk2_fk2 = zeros(3,1);
for i = 1:N

    % three principle axes
    for j = 1:3        

        % distance of target point from j-th principle axis
        dk = point_line_distance3D([Xcm;Xcm + paxe(1,j);ppS(i,1)],[Ycm;Ycm + paxe(2,j);ppS(i,2)],[Zcm;Zcm + paxe(3,j);ppS(i,3)]);

        % RMS distance of the fiducial markers from the j-th principle axis
        fk = 0;
        for kk = 1:NM            
            pom = point_line_distance3D([Xcm;Xcm + paxe(1,j);pp(kk,1)],[Ycm;Ycm + paxe(2,j);pp(kk,2)],[Zcm;Zcm + paxe(3,j);pp(kk,3)]);
            fk = fk + pom^2;         
        end
        fk = sqrt(fk/NM);

        dk2_fk2(j) = dk^2/fk^2;

    end

    TRE(i,1) = FLE*sqrt( 1/NM + mean(dk2_fk2)/NM );
    
end
mTRE = mean(TRE);