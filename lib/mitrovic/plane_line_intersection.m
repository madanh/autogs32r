%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ALGORITHM FOR FINDING PLANE-LINE INTERSECTION POINT
% http://mathworld.wolfram.com/Line-PlaneIntersection.html
%
% X,Y,Z - x,y,z coordinates of 5 points, 3 points are defining plane,
%         2 points are defining line
% x0,y0,z0 - intersection point
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [x0,y0,z0] = plane_line_intersection(X,Y,Z)

t=-det([1 1 1 1;X(1:4)';Y(1:4)';Z(1:4)'])/det([1 1 1 0;X(1:3)' X(5)-X(4);Y(1:3)' Y(5)-Y(4);Z(1:3)' Z(5)-Z(4)]);

g = det([1 1 1 1;X(1:4)';Y(1:4)';Z(1:4)']);
d = det([1 1 1 0;X(1:3)' X(5)-X(4);Y(1:3)' Y(5)-Y(4);Z(1:3)' Z(5)-Z(4)]);

x0 = X(4)+(X(5)-X(4))*t;
y0 = Y(4)+(Y(5)-Y(4))*t;
z0 = Z(4)+(Z(5)-Z(4))*t;



