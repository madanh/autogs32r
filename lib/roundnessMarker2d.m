function [ markerCenters, varargout ] = roundnessMarker2d( X )
%AUTOMARKER2D detect fiducials in 2D image
%   return an array with x and y column and 12 markers
%   1.Blobness filter
%   2.Threshold
%   3.Median filter
%   4.Hough (imfindcircles)
%   5.Sort (take topmost 12 markers)

diagnostic=0;
if diagnostic
    figure;
    imagesc(X);
end
if nargin <2
    p.r=5;
end
skip = 0
if ~skip
    p.threshold = 1e7;
    p.median_kernel = [5 5];
    p.radius_range = [2 9];
    p.n_markers =12;
    p.fiducialAreaInPixels = [4 p.r^2*pi*2];
    markerCenters = [];
    
    
    %% 4. Roundness
    
    
    %     tic;imagesc(roundness(imgradient(b)));toc;colormap('jet');colorbar;hold on;
    
%     try;saveas(gcf,strcat('./res/',num2str(gcf),'.png'));end
    %     figure;
    %     tic;A=roundnessv2(X);toc
    %    imagesc(A);colormap('jet');colorbar;hold on;
    
    b=roundness(X).*(max(X(:))-X);
    if diagnostic
        figure;
        
        imagesc(b);colormap('jet');colorbar;hold on;
    end
    try;imwrite(255.*b./max(B(:)),jet(256),strcat('./res/',num2str(gcf),'.png'));end
    
else
    %%skipping
    markerCenters = [];
end

% % %   4.Hough (imfindcircles)
% %     [c ,radii,metr]= imfindcircles(b,p.radius_range,'ObjectPolarity','dark');
% %
% % if ~isempty(c)
% %    for i =1:size(c,1)
% %     text(c(i,1)+5,c(i,2)+5,num2str(metr(i)*10,0),'Color',[1 1 1]);
% %     text(c(i,1)-5,c(i,2)-5,num2str(radii(i),0),'Color',[1 1 1]);
% %    end
% %
% %
% % end
% %
% % markerCenters = c;
%%  4.find connected components
m=b>p.threshold;
CC = bwconncomp(m,4);

%% REMOVE TOO SMALL MARKERS

%% REMOVE TOO BIG MARKERS





% %%   5.take the candidates with highest blobness
% % IN case of this patient and sampling the fiducials have sizes around 45 pixels.
% fiducialVolumeInPixels = p.fiducialAreaInPixels; %TODO: calculate this from sampling and physical ball size
%
% % Let's get only those
% % figure
% % hold on
% CC.removeindex = zeros(1,numel(CC.PixelIdxList));%prepare to remove all bad blobs
% for i = 1:numel(CC.PixelIdxList)
%     blob = CC.PixelIdxList{i};
%     [bx,by] = ind2sub(size(b),blob);
%     if (numel(blob) > max(fiducialVolumeInPixels)) || (numel(blob) < min(fiducialVolumeInPixels))% || any(paxes(bx,by,))>8 || any(paxes(bx,by,))<4
%         %if the blob is too big or small (in span/voxel count terms) to
%         %be a marker
%         CC.removeindex(i) = 1;
%
%         if diagnostic; scatter(by,bx,1,[0 0 1],'o'); end;% then plot it black
%         %     elseif min(span(bx,by,))<3 || max(span(bx,by,))>6%
%         %         CC.removeindex(i) = 1;
%         %
%         %         if diagnostic; scatter(by,bx,8,[0 0.8 0],'o'); end;% then plot it green
%     else  %if the blob has about the right voxel count
%         %         tttt = paxes(bx,by);
%         %         r12 = tttt(1)/tttt(2);
%         %         r23 =1;
%         %         if any([r12,r23]>1.43) || any([r12,r23]<0.7)
%         %             CC.removeindex(i) = 1;
%         %
%         if diagnostic; scatter(by,bx,1,[1 0 0],'o');end; % then plot it blue
%         %         else
%         %
%         %
%         %         end
%     end
%
% end
%
% CC.PixelIdxList(logical(CC.removeindex))=[];
% CC.NumObjects = CC.NumObjects -sum(CC.removeindex);
% CC.removeindex = [];
%
% weightedMean = @(coords,weights) sum(coords.*weights)./sum(weights);
% for i = 1:numel(CC.PixelIdxList)
%     blob = CC.PixelIdxList{i};
%     [bx,by] = ind2sub(size(m),blob); %NOTE THAT WE USE BLOBNESS AS WEIGHT BELOW
%     CC.cx(i,1)= weightedMean(bx,b(blob)-p.threshold); % Here I subtract the threshold so the it matches
%     CC.cy(i,1)= weightedMean(by,b(blob)-p.threshold); % the formula (2.1) from Dejan Tomazevic's thesis
%     %     CC.cz(i,1) = weightedMean(,m(blob)-thr); % ref:(Bose and Amir 1990; Chiorboli and Vecchi 1993)
%
% end
% markerCenters = sortrows([CC.cy,CC.cx],2); %%
% % markerCenters = markerCenters(1:12,:);
% varargout{1} = CC.PixelIdxList;

%% Get the blobness maxima in the blobs
%get centers
weightedMean = @(coords,weights) sum(coords.*weights)./sum(weights);
for i = 1:numel(CC.PixelIdxList)
    blob = CC.PixelIdxList{i};
    [bx,by] = ind2sub(size(m),blob); %NOTE THAT WE USE BLOBNESS AS WEIGHT BELOW
    CC.cx(i,1)= weightedMean(bx,b(blob)-p.threshold); % Here I subtract the threshold so the it matches
    CC.cy(i,1)= weightedMean(by,b(blob)-p.threshold); % the formula (2.1) from Dejan Tomazevic's thesis
end


a=[];
for i = CC.PixelIdxList
    a=[a max(b(i{1}))];
end

% Split  the blobness maxima in two groups with minimal variance using Otsu
blobnessMaximumThreshold = graythresh(a);

% take at least 12 candidates!
if sum(a>=blobnessMaximumThreshold)<12
    blobnessMaximumThreshold = graythresh(a(a<blobnessMaximumThreshold));
end

%remove all blobs below the threshold
CC.removeindex = a<blobnessMaximumThreshold;

CC.cx(CC.removeindex) = [];
CC.cy(CC.removeindex) = [];
CC.PixelIdxList(CC.removeindex) = [];
CC.NumObjects = CC.NumObjects - numel(CC.removeindex);
CC.removeindex = [];


markerCenters = sortrows([CC.cy,CC.cx],2); %%
% markerCenters = markerCenters(1:12,:);
varargout{1} = CC.PixelIdxList;

if diagnostic
    fprintf('%1.f markers detected\n',numel(CC.cx));
    scatter(CC.cy,CC.cx,'g+');
end

end


function out = paxes(x,y)
%return principal axes as singular values
try
    [u,s,v] = svd([x-mean(x),y-mean(y)]);
    out = [s(1,1),s(2,2)];
    %         [s(1,1),s(2,2),s(3,3)]
catch
    out = [];
end
end

