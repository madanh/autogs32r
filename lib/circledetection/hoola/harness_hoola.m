function [ c_i,r_i,w_i ] = harness_hoola( roi,in_opt )
%HARNESS_HOOLA Summary of this function goes here
%   Detailed explanation goes here
%% set close to optimal options
opt = struct(                        'radius', [4 12]);
                  

%% unite opts
opt = unite_options(opt,in_opt);

if isfield(opt,'maximum_radius_in_projection') && isfield(opt,'minimum_radius_in_projection')
    opt.radius = [opt.minimum_radius_in_projection,opt.maximum_radius_in_projection];

end

%% call hoola
[c_i,r_i]= hoola( roi, opt.radius,opt );

%% straighten the output
w_i = ones(size(r_i));

end

