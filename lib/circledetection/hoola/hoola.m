function [out_center,out_radius]= hoola( roi, in_r ,varargin)
%Hoola find a _single_ fiducial in a roi with subpixel accuracy
%   We get a small 2D region as an input, and expect to have only one
%   marker in there. Again we use the fact that gradient direction forms a
%   "carousel" inside the marker.
%   in_r should is a two element vector defining a range of vales that
%   marker radius can take

%% options (hardcoded)
opt.diagnostic = false;
opt.nangles = 90;
% opt.nangles_fine = 1000;
opt.nsamples = 1000;
opt.tikz_suffix = '';

%% parsing
if nargin>2
    opt = unite_options(opt,varargin{1});
end

%% robust quantile for 'winsorizing'
robust_quantile_cutoff = 1-2*pi*max(in_r)./numel(roi);
%% grad images
g = imgradient(roi);

if max(g(:))-min(g(:))<1 %if there is nothing to detect (overexposed)
    out_center = [];
    out_radius = [];
    return
end
%% find max p(u,v)
p = carou(roi,round(mean(in_r)));
[maxv,ind]=max(p(:));
[x1mp,x2mp] = find(p==maxv,1,'first');


%% init the search
angles = (linspace(0,360-360/opt.nangles,opt.nangles)).';
robust_quantile = quantile(g(:),robust_quantile_cutoff);
robust_prob_map = g;
robust_prob_map(g> robust_quantile)=robust_quantile;
robust_interpolant = griddedInterpolant(robust_prob_map,'cubic','none');
% cost function , input is [x1,x2,r] circle params;
% extra care  with dmensions   is needed here, since robust_interpolant uses ndgrid format
robust_cost = @(x) -sum(robust_interpolant(x(1)+x(3).*cosd(angles),x(2)+x(3).*sind(angles))); 


%% search v2: pixelwise /integer exhaustive
[x1,x2,r] = ndgrid(x1mp-min(in_r):x1mp+min(in_r),x2mp-min(in_r):x2mp+min(in_r),min(in_r):max(in_r));

the_x = num2cell([x1(:),x2(:),r(:)],2);
the_cube = cellfun(robust_cost,the_x);
rx1 = numel(x1mp-min(in_r):x1mp+min(in_r));
rx2 = numel(x2mp-min(in_r):x2mp+min(in_r));
the_cube = reshape(the_cube,rx1,rx2,[]);

%find hte maximum voxel
[v,i] = min(the_cube(:));
[x1,x2,r] = ind2sub(size(the_cube),i);
x1 = x1+x1mp-min(in_r)-1;
x2 = x2+x2mp-min(in_r)-1;
r = r+min(in_r)-1;
if opt.diagnostic
    figure(302)
    cla;
    imagesc(robust_prob_map);
    hold on;
    x = [x1,x2,r];
    scatter(x(2)+x(3).*cosd(angles),...
    x(1)+x(3).*sind(angles));
    scatter(x(2),x(1));
    drawnow;
end
%% format output (0th order approx)

out_center = [x1,x2];
out_radius = r;


%% search v2 : refine with fmincon with 1 px bounds
% angles = (linspace(0,360-360/opt.nangles_fine,opt.nangles_fine)).';
% 
% % cost function , input is [x1,x2,r] circle params;
% % extra care  with dmensions   is needed here, since F uses ndgrid format
% cost = @(x) -sum(F(x(1)+x(3).*cosd(angles),x(2)+x(3).*sind(angles))); 

x0 = [x1,x2,r];

lb = [x1-0.5,x2-0.5,r-0.5];
ub = [x1+0.5,x2+0.5,r+0.5];

lb = [x1-1,x2-1,r-1];
ub = [x1+1,x2+1,r+1];
% cost function , input is [x1,x2,r] circle params;
% extra care  with dmensions   is needed here, since F uses ndgrid format
F = griddedInterpolant(g,'cubic','none');
cost = @(x) -sum(F(x(1)+x(3).*cosd(angles),x(2)+x(3).*sind(angles))); 


opts = optimoptions(@fmincon,'Display','none');



    x_tilde = fmincon(cost,x0,[],[],[],[],lb,ub,[],opts);

%% diagnostic visual
if opt.diagnostic   
    figure(302);
    hold on;
    x = x_tilde;
    scatter(x(2)+x(3).*cosd(angles),...
        x(1)+x(3).*sind(angles),'r.');
    scatter(x(2),x(1),'r*');
    colorbar;
    drawnow;
    title(['cost=',num2str(cost(x_tilde))])
    figure(304)
    cla
    imagesc(roi)
    colormap gray
   axis image
   hold on;
    scatter(x(2)+x(3).*cosd(angles),...
        x(1)+x(3).*sind(angles),'r.');
    scatter(x(2),x(1),'r*');
    title(['cost=',num2str(cost(x_tilde))])
end
%% format output (1st order approx)

out_center = x_tilde(1:2);
out_radius = x_tilde(3);


%% TRADITIONAL SUBPIXEL EDGE

% generate directions around the circle
angles = (linspace(0,360-360/opt.nangles,opt.nangles)).';
u = cosd(angles);
v = sind(angles);
n = [u,v];

%points on the circumference:
xh = bsxfun(@plus,[x_tilde(1),x_tilde(2)],x_tilde(3).*n);
% generate lines (numerically for now, if it works then maybe(never)  will
% switch to analytic
sample_points =linspaceNDim(xh-n,xh+n,opt.nsamples);

lineprof = reshape(F(reshape(permute(sample_points,[2,1,3]),2,[],1).'),size(sample_points,1),[]);

[~,mind] = max(lineprof,[],2);
edge_points = cell2mat(arrayfun(@(x) sample_points(x,:,mind(x)),(1:size(sample_points,1)).','UniformOutput',false));

bad_points = sum((edge_points-xh).^2,2)>0.5;%more than pixel's diagonal away

edge_points(bad_points,:)=[];
if isempty(edge_points)
    return
end
sample_points(bad_points,:,:) = [];



if opt.diagnostic
    figure(304);
    scatter(edge_points(:,2),edge_points(:,1),'m+');
    drawnow;
end
% FIT THE CIRCLE (COPY PASTE FROM REFINE_FROM_ARCS)
%% vvvvvvvvvvvvvvvvvvvvvvvvvvvvv:
b = HyperSVD(edge_points);
meanX = 0;
meanY = 0;
if opt.diagnostic
      scatter(b(:,2),b(:,1),'g*');
      viscircles(b(1,[2,1]),b(3),'EdgeColor',[0 0.85 0],'LineWidth',1);
      drawnow;
end


%% format output 2nd order approx
out_center = [b(2) b(1)];
out_radius = b(3);


%% PUB_QUALITY FIGURES WITH TIKZ
if isfield(opt,'make_tikz_figures')&& opt.make_tikz_figures
    path_prefix = 'images/secondary/'
    % A
    figure(351);
    cla;
    imagesc(roi);
    % title('A');
    colormap gray;
    set(gca,'XTick',[]);
    set(gca,'YTick',[]);
matlab2tikz(strcat('A',opt.tikz_suffix,'.tikz'), 'width', '\figurewidth',...'height', '\figureheight',... 'extraAxisOptions',axoptions,...
    'showInfo', false,'imagesAsPng',true,'relativeDataPath',path_prefix);  
    
    % B
    figure(352);
    cla;
    imagesc(robust_prob_map);
    colormap gray;
    set(gca,'XTick',[]);
    set(gca,'YTick',[]);
    hold on;
    x = x_tilde;
    scatter(x(2)+x(3).*cosd(angles),...
        x(1)+x(3).*sind(angles),'r.');
%     scatter(x(2),x(1),'r+');
    % title('B');
matlab2tikz(strcat('B',opt.tikz_suffix,'.tikz'), 'width', '\figurewidth',...'height', '\figureheight',... 'extraAxisOptions',axoptions,...
    'showInfo', false,'imagesAsPng',true,'relativeDataPath',path_prefix);     
    
    % C
    figure(353);
    cla;
    imagesc(roi);
    % title('C');
    colormap gray;
    set(gca,'XTick',[]);
    set(gca,'YTick',[]);
    hold on;
    scatter(edge_points(:,2),edge_points(:,1),100,'b.');
    scatter(edge_points(:,2),edge_points(:,1),35,'g.');
    viscircles(b(1,[2,1]),b(3),'EdgeColor',[0 1 0],'LineWidth',0.5,...
        'EnhanceVisibility',false);
%    scatter(x(2),x(1),'g+'); %Old center (from fitting)
    scatter(b(:,2),b(:,1),'g+'); 
% axoptions={'scaled y ticks = false',...
%            'y tick label style={/pgf/number format/.cd, fixed, fixed zerofill}'};    
matlab2tikz(strcat('C',opt.tikz_suffix,'.tikz'), 'width', '\figurewidth',...'height', '\figureheight',... 'extraAxisOptions',axoptions,...
    'showInfo', false,'imagesAsPng',true,'relativeDataPath',path_prefix);  
    
    
end
end

