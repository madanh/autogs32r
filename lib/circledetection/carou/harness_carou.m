function [ c_i,r_i,w_i, P] = harness_carou( in_im,varargin )
%harness_carou a wrapper around carou to provide standard output format
%and good defaults to harness
%% set close to optimal options
opt = struct(   'minimum_radius_in_projection', 6 ,...     in px
    'maximum_radius_in_projection',10,...
    'carou_tolerance', 5); % in degrees

opt.nmarkers_per_group = 4;
opt.ngroups = 3;
%% parsing
if nargin>1
    opt = unite_options(opt,varargin{1});
end

in_r = [opt.minimum_radius_in_projection opt.maximum_radius_in_projection];
%% call carou
P = carou(in_im,in_r);


%% autothresh-dilate-first peak
Q = imdilate(P,strel('disk',max(in_r)));

u = unique(Q(:));
d = diff(histc(Q(:),u));
ind = find(d<0,1,'last');
% thr = u(ind);

% CC = bwconncomp(P>thr);
%% autothresh-clusters
n_markers =opt.nmarkers_per_group *opt.ngroups;
flag_good_to_go = 0;

while ~flag_good_to_go
    thr = u(ind);
    CC = bwconncomp(P>mean(thr));

    if CC.NumObjects>=n_markers
        S = regionprops(CC,P,'Centroid');
        coords= vertcat(S.Centroid);
        markerLinkage = linkage(coords);
        markerClusters = cluster(markerLinkage,'cutoff',opt.max_interball_distance_in_projection,'criterion','distance');
        counts = histc(markerClusters,unique(markerClusters));
        %the 7+4 rule:
        flag_good_to_go =...
            sum((counts>=opt.nmarkers_per_group)+...
            (counts>=2.*(opt.nmarkers_per_group)-1))>=opt.ngroups;
    end
% if opt.diagnostic
%     figure;imagesc(P>thr);
% end
if opt.diagnostic && ~flag_good_to_go; 
    fprintf(2,'Thresh guess was wrong\n');
end;

    ind = ind-1;
end


%==OUT
S = regionprops(CC,P,'Centroid','MaxIntensity');
c_i = vertcat(S.Centroid);
r_i = mean(in_r).*ones(size(S,1),1);
w_i = vertcat(S.MaxIntensity);


end


