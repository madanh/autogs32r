function [ simulatedD,circMask,tolerance] = carousel_filled_template( R )
%CAROUSEL_TEMPLATE Summary of this function goes here
%   Detailed explanation goes here
[x1,x2] = ndgrid(-R:R,-R:R);
r = sqrt((x1.*x1+x2.*x2));
% circular mask
circMask = r<R;

simulatedD = -atan2d(x1,x2).*double(circMask);%+360*(rand(size(x1))-0.5).*~circMask;

tolerance = (180/pi)*4./r.*double(circMask); %see yellow notebook for proof
end

