function [ out ] = carou_filled( im,templ,mask,tol )
%CAROU_FILLED Summary of this function goes here
%   Detailed explanation goes here
%#codegen

[w,h]=size(im);
[wt,ht]=size(templ);
wh=(wt-1)/2; %halfwidth of the template
hh=(ht-1)/2;


out = ones([w,h]);

for i = 1+wh:w-wh
    for j = 1+hh:h-hh
        out(i,j) = sum(sum((abs(im(i-wh:i+wh,j-hh:j+hh)-templ)<tol) & mask));
    end
end
out = out-1;

end

