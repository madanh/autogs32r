function c2 = carou( in_im,in_r)


%thr w.r.t. counts of pixels that mathc the template normalized to max possible number of counts
% so thr = 0.25 can be interpreted as a quarter-circle match


% r = min(in_r);
r = max(in_r);
[~,gd] = imgradient(in_im);

[templ,circMask,tolerance] = carousel_filled_template(r);
c2 = carou_filled(gd,templ,circMask,tolerance);%last argument is tolerance
c2=c2./sum(sum(circMask));


%     figure;imagesc(c2);



end

