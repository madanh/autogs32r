function [c_i,r_i,diag] = cht(in_im,varargin)
    %a wrapper around imfindcircles, enables training
    warning('off','images:imfindcircles:warnForSmallRadius');
    %% default options
    opt.AccumulatorThreshold = 0.15*100;
    opt.EdgeThreshold = 150;
    opt.RadiusLower = 5;
    opt.RadiusHigher = 15;
    
    %% if defaults are requested
    if strcmpi(in_im,'get default options')
        c_i = opt;
        return
    end
    %% option merger
    % override if external options are supplied
    if nargin>1
        in_options = varargin{1};
        opt = unite_options(opt,in_options);
    end

    %% option translation into smth imfindcircles understands
    sens = 1-opt.AccumulatorThreshold/100;
    
    %% required for compatibility (i.e. working around matlabs ridiculous im2single) internal conversion
    if ~ isa(in_im,'double')
        in_im = double(in_im); % for compatibility
    end
    
    %% FUNCTION CALL
        [c_i,r_i] = imfindcircles(in_im,[opt.RadiusLower opt.RadiusHigher],'EdgeThreshold',opt.EdgeThreshold,'Sensitivity',sens,'ObjectPolarity','dark');
    %% output packaging
        diag =[];
end