function [out_center,out_radius] = refine_from_arcs(im, arc, varargin)
%CONSTANCTS
diagnostic = false;
opt.n_interpoints = 1000; %number of poinbts in the line profile along normal
% [r,c] = ind2sub(size(im),arc);
% h = scatter(c,r,'r.');
% PREPARATIONS
if nargin >3
    g = varargin{1};
    d = varargin{2};
else
    [g,d] = imgradient(im);
end
if diagnostic
    figure(13374);
    clf;
    imagesc(g);
    hold on;
end


F = griddedInterpolant( g, 'cubic');


for i = 1:numel(arc)
    ind =arc{i}.';
    [y,x] = ind2sub(size(im),ind);
    p = [y,x];
    nn = [-sind(d(ind)),cosd(d(ind))];
    sample_points =linspaceNDim((p-nn),(p+nn),opt.n_interpoints);
    if diagnostic
        scatter(sample_points(:,2,1),sample_points(:,1,1),'c.');
        scatter(sample_points(:,2,end),sample_points(:,1,end),'r.');
    end
    lineprof = reshape(F(reshape(permute(sample_points,[2,1,3]),2,[],1).'),size(sample_points,1),[]);
%     if diagnostic
%         figure(13375);
%         plot(lineprof.');
%     end
    
    
    [~,mind] = max(lineprof,[],2);
    edge_points{i} = cell2mat(arrayfun(@(x) sample_points(x,:,mind(x)),(1:size(sample_points,1)).','UniformOutput',false));
    
    if diagnostic
        figure(13374);
        scatter(edge_points{i}(:,2),edge_points{i}(:,1),'b*');
    end
    
    a{i} = HyperSVD(edge_points{i});
    if diagnostic
        figure(13374);
        scatter(a{i}(:,2),a{i}(:,1),'r*');
%         scatter(a{i}(:,1),a{i}(:,2),'m*');
        viscircles(a{i}(1,[2,1]),a{i}(3));
    end
    residuals{i} = sum(bsxfun(@minus,edge_points{i},a{i}(1,[1,2])).^2,2)-a{i}(3).^2;
end
% %% DEMEAN (hopefully will fix one annoying bug)
% meanX = mean(subX);
% meanY = mean(subY);
% subX = subX-meanX;
% subY = subY-meanY;

%% FIT THE ELLIPSE
% a = CircleByTaubin([subX,subY]);
%% THIS IS FOR DEBUGGING ONLY!
%% vvvvvvvvvvvvvvvvvvvvvvvvvvvvv:
b = HyperSVD(vertcat(edge_points{:}));
meanX = 0;
meanY = 0;
if diagnostic
      scatter(b(:,2),b(:,1),'g*');
      viscircles(b(1,[2,1]),b(3),'EdgeColor',[0 0.85 0],'LineWidth',3);
end
%% ^^^^^^^^^^^^^^^^^^^^^^^^^^
%% THIS IS FOR DEBUGGING ONLY!
the_center = [b(2) b(1)];
out_radius = b(3);

%% OLOLO


out_center = the_center+[meanX meanY];
%  delete(h);
end
