function [rc_i,r_i,varargout] = spread(in_im,varargin)
% SPREAD :DETECT CIRCLES
% inspired by chen and wu
% how is this different:
% 1.two bug fixes
% 2.
%
% DONE:
% 1.remove quasilines
% 2.limit the intersection point search to one connected component (may
% fail on some images, but I don't think it would ever fail on out db. At
% worst it would generate duplicate circles.
%
% 3.Actual pairing: for valid intersection points difference f radii should
% be smaller then treshold
%
% 4. Incorporate direction info to discriminate between false pairs.
%
% 5. Arc pair chaining => Centers and radii. (didnt do radii, no need).
%
% 6.Replace bisector from points with bisector from average gradient
% direction

% TODO:
%
%
% 1. After quasiline removal the CCs are no longer connected. This should
% be taken into account (probably in spread_arc_segmentation)
%
% 2. arcs overlap (in spread_arc_segmentaion)
%
%
%% INPUT
% in_im; input image



%% OPTIONS
%default options
opt.diagnostic=1;

% self-options of this function
opt.balls_are_dark = true;
opt.madans_criterion_threshold = 1.5;
opt.radius_tolerance = 4; %(px)
opt.pairs_per_circle_threshold = 1; %integer>=1

%spread_arc_segmentation options follow:
opt.max_arc_angle=45;
opt.minlenght=5;
opt.radius=9; %px
% opt.min_arc_len=5;
opt.remove_quasilines=true;
opt.presmooth=true;
opt.sobel_threshold = 0; % 0 will use matlab;s internally autoselected thresholds

%fine grain diagnostic control
opt.show_bisectors = 0;
opt.show_intersections = 0;
opt.show_good_intersections =1;
opt.show_pairing_matrix = false;
opt.show_arc_numbers = 1;


% derived options (can be overriden)
opt.threshold_on_movement_after_refinement = opt.radius;

if strcmpi(in_im,'get default options')
    c_i = opt;
    return
end

if ~ isa(in_im,'double')
    in_im = double(in_im); % for compatibility
end

% override if external options are supplied
if nargin>1
    in_options = varargin{1};
    opt = unite_options(opt,in_options);
end





%% SEGMENT THE ARCS
[arc,original_w,g,grad_dir] = spread_arc_segmentation(in_im,opt);
% cast to single to make sane thresholds in edge function that is used
% internally by spread_arc_segmentation.
% original_w is gradient directions in degrees , for edge pixels
if isempty(arc)
    rc_i = [];
    r_i = [];
    varargout{1} = struct('n_i',[]);
    return
end

%% "CONSTANTS"

sz = size(in_im);
n_arcs = numel(arc);

if opt.diagnostic
%     figure(1001); 
    figure; 
    title('diagnostic visual for SPREAD circle detector')
    imagesc(in_im); colormap(gray); axis image; hold on
end
%% Show the image & arc numbers
if opt.diagnostic && opt.show_arc_numbers
%     figure(1001);
    
    % Show the arc numbers
    for j =1:n_arcs
        [x1,x2] = ind2sub(sz,arc{j});
        text(x2,x1,num2str(j),'Color',[0 0 1])
    end
    
end




%% GET ORTHOGONAL BISECTORS *My METHOD*
%These arrays hold coefficients of bisectors for each arc

bs=-ones(size(arc));


%         grad_values_fun = @(x) mean(original_w(x),2); <-- this is wrong
%         way to do it beacuase average angle of 0 and 360 is NOT 180

% correct calculation of average angle as per https://stackoverflow.com/questions/5343629/averaging-angles
grad_values_fun = @(x) atan2d(sum(sind(original_w(x)),2),sum(cosd(original_w(x)),2));

average_arc_gradient = cellfun(grad_values_fun,arc);
as = -tand(average_arc_gradient);
mean_xs = cellfun(@(arc) mean(ceil(arc./sz(1)),2),arc);
mean_ys = cellfun(@(arc) mean(rem(arc,sz(1)),2),arc);
cs = -as.*mean_xs+mean_ys;

%             DIAGNOSTIC DRAWING OF BISECTORS

if opt.diagnostic && opt.show_bisectors
%     figure(1001);
    scatter(mean_xs,mean_ys,'yd');
    xx=[1,sz(2)];
    yy = bsxfun(@minus,(-cs./bs).',(as./bs).'*xx);
    hold on;
    plot(xx.',yy.','Color',rand(1,3));
end










%% ARCS PAIRING

pd = squareform(pdist([mean_xs; mean_ys].'));
good =sparse(pd<2*max(opt.radius+opt.radius_tolerance));






% INTERSECTIONS
cb=kron(cs',bs);
ba=kron(bs',as);
ac=kron(as',cs);
% xint,yint are bisector intersection points
xint=(cb-cb')./(ba-ba');
yint=(ac-ac')./(ba-ba');
xint(isnan(xint))=0;
yint(isnan(yint))=0;

%valid intersection points should lie inside the image
valid=(round(xint)>0)&(round(xint)<sz(2))&(round(yint)>0)&(round(yint)<sz(1))&good;
valid=logical(valid);

% SHOW THE INTERSECTIONS
if opt.diagnostic && opt.show_intersections
    figure(1001);
    colormap(gray);
    hold on;
    scatter(xint(valid),yint(valid),'rx');
end


% GET DISTANCES FROM EACH ARC MIDPOINT TO THE INTERSECTION POINT
dx = bsxfun(@minus,xint,mean_xs);
dy = bsxfun(@minus,yint,mean_ys);
d = sqrt(dx.^2+dy.^2);

% they all should be close to radius
radius_good = d<(opt.radius+opt.radius_tolerance) & d>(opt.radius-opt.radius_tolerance);

% GET WU's CRITERION FOR ARC PAIRING
%wus_criterion = abs(d-d')./(d+d');
madans_criterion = abs(d-d');

good = valid & radius_good & madans_criterion<opt.madans_criterion_threshold & good;
good = good & good.';

%reduce x_int and y_int
xint(~good)=0;
yint(~good)=0;
xint= sparse(xint);
yint= sparse(yint);


% check polarity
if opt.balls_are_dark
    indices = (sz(1).*spfun(@(x) round(x)-1, xint))+(spfun(@(x) round(x), yint));
    intersection_intensity=xint; %duplicate sparse matrix to get the right indexing
    intersection_intensity(good) = in_im(indices(good));
    arc_intensity = double(cellfun(@(x) mean(in_im(x)),arc)); %double cuz m'lab can't bsxfun 'nithin' else y'know
    good(bsxfun(@gt,intersection_intensity,arc_intensity))=0;
    good = good & good.';
end




if opt.diagnostic && opt.show_good_intersections
%     figure(1001);
    scatter(xint(good),yint(good),'bo');
end
%% CHAIN THE ARC PAIRS BASED ON "CONNECITVITY"
[S, C] = graphconncomp(good); % this guy returns graph ccs even when there're no edges :(
n_i = hist(C,1:S);%number of elements in each graph cc
%this gives graph connected components
% with (strictly) more than opt.pairs_per_circle_threshold pairs
good_ccs = find(n_i>opt.pairs_per_circle_threshold );

if isempty(good_ccs)
    rc_i = double.empty([2,0]);
    r_i = rc_i;
    diag.n_i = 0;
    varargout{1} = diag;
    return
end
%

%% GET the CENTERS

c_i = [arrayfun(@(x) [mean(nonzeros(xint(C==x,C==x)))],good_ccs); arrayfun(@(x) [mean(nonzeros(yint(C==x,C==x)))],good_ccs)].';
% VIS
if opt.diagnostic && ~isempty(c_i)
%     figure(1001);
    scatter(c_i(:,1),c_i(:,2),100,'gs')
    scatter(c_i(:,1),c_i(:,2),90,'rs')
    scatter(c_i(:,1),c_i(:,2),80,'bs')
    text(c_i(:,1),c_i(:,2),num2str(n_i(good_ccs).'),'Color',[1 0 1],'FontSize',15);
    
end
n_i = n_i(good_ccs);
%% REFINE THE CENTERS
% pixels = arrayfun(@(x) [arc{C==x}],good_ccs,'UniformOutput',false);
arcs_of_a_circle = arrayfun(@(x) {arc{C==x}},good_ccs,'UniformOutput',false);
% [rc_i,r_i] = cellfun(@(px) refine_from_arcs(in_im, px,g,grad_dir),pixels.','UniformOutput',false);
[rc_i,r_i] = cellfun(@(x) refine_from_arcs(in_im, x,g,grad_dir),arcs_of_a_circle,'UniformOutput',false);
rc_i = vertcat(rc_i{:});
r_i=vertcat(r_i{:});
% vis refined centers
if opt.diagnostic && ~isempty(c_i)
%     figure(1001);
    scatter(rc_i(:,1),rc_i(:,2),100,'g+');
end

%% REMOVE CIRCLES THAT HAVE ORIGINAL AND REFINED CENTER TOO FAR APART
good_ind = max(abs(c_i-rc_i),[],2)<opt.threshold_on_movement_after_refinement;
rc_i = rc_i(good_ind,:);
n_i = n_i(good_ind);
r_i = r_i(good_ind);



%% REMOVE CIRCLES WITH WRONG RADIUS
good_ind = r_i>=opt.radius-opt.radius_tolerance & r_i<=opt.radius+opt.radius_tolerance;
rc_i = rc_i(good_ind,:);
n_i = n_i(good_ind);
r_i = r_i(good_ind);

% vis refined centers
if opt.diagnostic && ~isempty(c_i)
%     figure(1001);
    scatter(rc_i(:,1),rc_i(:,2),100,'r+');
end

%% OUTPUT
if opt.diagnostic && opt.show_pairing_matrix
    figure(1002); title('pairing matrix');
    spy(good);
end


%% Prepare diagnostic output
if nargout>2
    varargout{1}=cell2struct({n_i,arc,[xint(valid) yint(valid)],good_ccs,good},{'n_i','arc','intersection_points','good_ccs','good'},2);
    
end

end
