function [arc,original_w,varargout] = spread_arc_segmentation(in_im,varargin)
%SPREAD_ARC_SEGMENTATION segment edge image into tiny arcs
%   Varargin is for options structure. See:
%   https://bitbucket.org/madanh/harness/wiki/Options_API
%
%
%% INPUT
% in_im; input image

%% OPTIONS
opt.max_arc_angle=45;
opt.minlenght=5;
opt.diagnostic=0;
opt.radius=9; %px
% opt.min_arc_len=5;
opt.remove_quasilines=false;


if nargin>1
    in_options = varargin{1};
    opt = unite_options(opt,in_options);
end

%% "CONSTANTS"
opt.quasiline_angle = opt.max_arc_angle/2;
max_arc_angle=opt.max_arc_angle;
max_len_px = floor(2*pi*min(opt.radius)*max_arc_angle/360);
%the following formula is from "if bent by 2 pixels then it;s not good)"
% opt.quasiline_angle=2*opt.max_arc_angle/max_len_px;

%% INIT
debug_quasiline_count=0;

%% PREPROCESSING

% optional presmooth:
if exist('opt','var')&&isfield(opt,'presmooth')&&opt.presmooth
    w = imfilter(in_im,fspecial('gaussian'));
end
% if isfield(opt,'canny_threshold') && max(opt.canny_threshold)>0
%     [e,threshold_that_was_used_in_edge_detector] = edge(in_im,'canny',opt.canny_threshold);
% else
%     [e,threshold_that_was_used_in_edge_detector] = edge(in_im,'canny');
% end

if isfield(opt,'sobel_threshold') && max(opt.sobel_threshold)>0
    e = edge(in_im,'sobel',opt.sobel_threshold);
else
    e = edge(in_im,'sobel');
end

[g,d] = imgradient(in_im,'sobel');

d = d+180;
d(d(e==1)==0)=360; %mark pixels that are on edge and have gradient in 0 direction
%as 360 degree, so that  they are not confused with non-edge pixels after
%this:

w = d.*e; %w is a gradient direction image for edge pixels only
% w = int16(round(w));
original_w=w;
if exist('opt','var')&&isfield(opt,'diagnostic')&&opt.diagnostic
    figure;
    imagesc(w);
    hold on; axis image;
    drawnow;
end


%% ARC SEGMENTATION
CC=bwconncomp(w);

% Remove small CC's

to_remove = false(1,CC.NumObjects);
for i = 1:CC.NumObjects
    if numel(CC.PixelIdxList{i})<=opt.minlenght;
        to_remove(i)=true;
    end
end
CC.PixelIdxList(to_remove)=[];
CC.NumObjects=numel(CC.PixelIdxList);


% Go through CC's:
sz = size(in_im);
arc={}; %this will contain pixels that belong to potential arcs
arc_counter=0;
cc_counter = 0;
connected_arcs_current_size = numel(CC.PixelIdxList)*2;
connected_arcs = cell(connected_arcs_current_size,1);
cc_parent=cell(numel(CC.PixelIdxList),1); %track from which cc an arc comes
for i = 1:numel(CC.PixelIdxList)
    px=CC.PixelIdxList{i}; %pixels of the current connected component
    [x, y] = ind2sub(sz,px);
    numpx = numel(px);
    cc_counter = cc_counter +1;
    for j=1:numpx; %pxi = pixel index
        if w(px(j))<361  &&  w(px(j))>0 % if valid unvisited pixel (second clause seems to be redundant)
            % start a new arc segment
            arc_counter=arc_counter+1;
            arc{arc_counter}=[];
            acw = w(px(j));
            accw=acw;
            most_cw =px(j);
            most_ccw = most_cw;
            visit(px(j));
            % this removes quasilines and %this removes too short arcs
            if (opt.remove_quasilines && distance_cw(accw,acw)<=opt.quasiline_angle)||numel(arc{arc_counter})<opt.minlenght
                w(arc{arc_counter})=-1;    %mark pixels as invalid
                arc{arc_counter}=[];  %remove the segment
                arc_counter=arc_counter-1;  %reset the counter
                % if on both sides of the arc there are valid pixels of cc
                % (visited or not) then break the cc
%                 if j<numpx && any(w(px(j+1:numpx))<361) && any(w(neighborhood_indices(most_cw,sz))>0) && any(w(neighborhood_indices(most_ccw,sz))>0)
%                     cc_counter = cc_counter +1;
%                 end
                
            %this removes too short arcs
            else
            %add current arc (if added to arc cell array) to cc_parent
            cc_parent{i}=unique([cc_parent{i} arc_counter]);
            %dynamically increase size of connected_arcs
            if cc_counter > connected_arcs_current_size
                connected_arcs = [connected_arcs; cell(connected_arcs_current_size,1)];
                connected_arcs_current_size = connected_arcs_current_size*2;
            end
            %add current arc to the cc
            connected_arcs{cc_counter}  = unique([connected_arcs{cc_counter} arc_counter]);
            end
        end
    end
    
end



% EDGE CASE: LAST ARC MIGHT BE EMPTY, CHECK THAT
if ~isempty(arc) && isempty(arc{end})
    arc(end)=[];
end

if nargout>2
%     varargout{1}=cc_parent;
    varargout{1}=g;
    varargout{2}=d;
    
end


%% SHOW THE ITSY-BITSY ARCS ^^
% 
% if nargout>3
%     buf = zeros(size(w));
%     for arc_index_for_diagnostic = 1:numel(arc)
%         px_index_for_diagnostic = arc{arc_index_for_diagnostic};
%         buf(px_index_for_diagnostic)=arc_index_for_diagnostic;
%         %        imagesc(buf);
%         %        drawnow
%     end
%     if opt.diagnostic
%         figure;
%         image(buf);
%         drawnow
%     end
%     varargout{2}=buf;
% end

%% A NESTED FUNCTION FOLLOWS!! OOOOh MATLAB

    function visit(in_px)
        %             disp(starting_angle)
        arc{arc_counter}=[arc{arc_counter} in_px];
        w(in_px)=361; %assign a VISITED(361) value
        if numel(arc{arc_counter})<=max_len_px;
            [x_current, y_current] = ind2sub(sz,in_px);
            %         if opt.diagnostic
            %             scatter(y_current,x_current,'r.');
            %             drawnow;
            %         end
            adjacent = px((max(abs(x-x_current),abs(y-y_current))<=1)); %edge pixels in 8 neighborhood
            adjacent(adjacent==in_px)=[]; %exclude the central pixel
            for next_pixel = adjacent' % need a transposition here for iteration to work
                if w(next_pixel)<361 && w(next_pixel)>0 %if not visited and valid (second clause redundant)
                    if iscw(acw,w(next_pixel))
                        acw=w(next_pixel);
                        most_cw = next_pixel;
                    end
                    if ~iscw(accw,w(next_pixel))
                        accw=w(next_pixel);
                        most_ccw = next_pixel;
                    end
                    % (inverse) termination criterion:
                    
                    if distance_cw(accw,acw)<=max_arc_angle
                        visit(next_pixel);
                    end
                end
            end
        end
        
    end

end

function [logical_out] = iscw(reference_angle,input_angle)
%outputs true if input_angle is clockwise from reference_angle
delta=reference_angle-input_angle;
if delta<-180 || delta>0
    logical_out=true;
else
    logical_out=false;
end

end

function out = distance_cw(accw,acw)
%utility for termination criterion
out = accw-acw;
if out<0
    out=out+360;
end
end

