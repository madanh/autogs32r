%read png
im=imread('test1.png');
imagesc(im);
colormap gray
axis image

%grab 12 roos
coords = cell(1,12);
for i = 1:numel(coords)
    [x,y] = ginput(2);
    x = sort(x);
    y = sort(y);
    coords{i} = [x,y]
    rois{i} = im(y(1):y(2),x(1):x(2));
end

% look at each
for i = 1:numel(rois)
    figure
    imagesc(rois{i})
end
%save
save('rois.mat','rois');