function [out_center,out_radius]= sunny_refinement( roi, r )
%SUNNY_REFINEMENT find a _single_ fiducial in a roi with subpixel accuracy
%   We get a small 2D region as an input, and expect to have only one
%   marker in there. Again we use the fact that gradient direction forms a
%   "carousel" inside the marker.
%   r should be the _minimal_ radius of the marker

% options (hardcoded)
opt.diagnostic = true;
opt.nsamples = 1000;
opt.nangles = 360;
opt.threshold_quantile= 0.9;
%grad images
[gr,gdr] = imgradient(roi);

if max(gr(:))-min(gr(:))<1 %if there is nothing to detect (overexposed)
    out_center = [];
    out_radius = [];
    return
end
% find max p(u,v)
p = carou(roi,r);
[maxv,ind]=max(p(:));
[x1,x2] = find(p==maxv,1,'first');
mp = [x1,x2];
% generate template and tolerance
[templ,circMask,tolerance] = carousel_filled_template(r);
[wh,hh] = size(templ);
% crop the p(u,v) image to match template
im=roi(x1-r:x1+r,x2-r:x2+r);
gd = gdr(x1-r:x1+r,x2-r:x2+r);
% g  = gr(x1-r:x1+r,x2-r:x2+r);
%get good pixels image
good_pixels = (abs(gd-templ)<tolerance).*circMask;

%show the image
if opt.diagnostic
    figure(83315)
    subplot(2,2,1)
    cla;
    imagesc(roi)
    subplot(2,2,2)
    cla;
    imagesc(gdr);
    subplot(2,2,3)
    cla;
    imagesc(p);
    subplot(2,2,4)
    cla;
    imagesc(good_pixels)
    drawnow;
end
% generate directions around the circle
angles = (linspace(0,360-360/opt.nangles,opt.nangles)).';
u = cosd(angles);
v = sind(angles);
n = [u,v];

% generate lines (numerically for now, if it works then maybe(never)  will
% switch to analytic
sample_points =linspaceNDim(bsxfun(@minus,[r+1,r+1],zeros(size(n))),bsxfun(@plus,[r+1,r+1],2*r.*n),opt.nsamples);

[x1g,x2g] = find(good_pixels);
% find line points that lie "inside" good pixels
stretches = ismember(reshape(permute(round(sample_points),[2,1,3]),2,[]).',[x1g,x2g],'rows'); %maybe some reshapology will be needed
% find _SUM_ lenght of continuous segments that lie within good pixels
stretches = reshape(stretches,numel(angles),[]);
if opt.diagnostic
    fig_histo = 10101;
    figure(fig_histo)
    subplot(2,1,1)
    cla;
    imagesc(stretches)
end

l = zeros(size(angles));
for i = 1:numel(angles)
    a = diff([0 ,find(diff(stretches(i,:))), opt.nsamples]);
    l(i) = sum(a((2-stretches(i,1):2:end)));
end
% find the threshold as the first minimum from the right of the length
% histogram

[lhc,bc] = hist(l,round(sqrt(opt.nangles)));
if opt.diagnostic
    subplot(2,1,2)
    cla;
    hist(l,round(sqrt(opt.nangles)))
end
%% THRESHOLD SELECTION
%% vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
% %"last peak" method
dhc = diff(lhc);
first_max_from_the_right= find(dhc>=0,1,'last'); %this might need minor adjustment, to improve recall
dhc = dhc(1:first_max_from_the_right);
l_thresh = bc(find(dhc<0,1,'last'));
% l_thresh = 0.5.*opt.threshold_quantile.*opt.nsamples;
% l_thresh = quantile(l,opt.threshold_quantile);
% l_thresh = 450;
%% ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
%%  THRESHOLD SELECTION

% select only rays that are above the threshold
sample_points=sample_points(l>=l_thresh,:,:);
if opt.diagnostic
    figure(83315)
    
    hold on
    scatter(sample_points(:,2,1),sample_points(:,1,1),'c.');
    scatter(sample_points(:,2,end),sample_points(:,1,end),'r.');
    drawnow;
end



%moce to coordnate system of roi
sample_points(:,1,:)=sample_points(:,1,:)-r-1+x1;
sample_points(:,2,:)=sample_points(:,2,:)-r-1+x2;
% go subpixel as usual
F = griddedInterpolant( gr, 'cubic','none');


lineprof = reshape(F(reshape(permute(sample_points,[2,1,3]),2,[],1).'),size(sample_points,1),[]);

[~,mind] = max(lineprof,[],2);
mind_bad=mind>opt.nsamples.*1;
mind(mind_bad) = [];
sample_points(mind_bad,:,:) = [];
edge_points = cell2mat(arrayfun(@(x) sample_points(x,:,mind(x)),(1:size(sample_points,1)).','UniformOutput',false));

if isempty(edge_points)
    out_center = [];
    out_radius = [];
    return
end


if opt.diagnostic
    figure(13376);
    cla;
    imagesc(gr)
    axis image
    hold on
    colormap gray
    scatter(edge_points(:,2),edge_points(:,1),'m+');
    drawnow;
end

%% FIT THE CIRCLE (COPY PASTE FROM REFINE_FROM_ARCS)
%% vvvvvvvvvvvvvvvvvvvvvvvvvvvvv:
b = HyperSVD(edge_points);
meanX = 0;
meanY = 0;
if opt.diagnostic
      scatter(b(:,2),b(:,1),'g*');
      viscircles(b(1,[2,1]),b(3),'EdgeColor',[0 0.85 0],'LineWidth',1);
      drawnow;
end

the_center = [b(2) b(1)];
out_radius = b(3);

%% OLOLO


out_center = the_center+[meanX meanY];
%  delete(h);

%% JUST IN CASE: RESIDUALS
residuals = sqrt(sum(bsxfun(@minus,edge_points,b(1,[1,2])).^2,2))-b(3);
R2 = rms(residuals);
if R2>=1.4142 %sqrt 2
        out_center = [];
    out_radius = [];
    return
end
end

