function [varargout] = calibrate(vol,WPR,OPR,in_opt)


%CALIBRATE Uros's retrospective carm calibration adapted to my automation harness
%   Detailed explanation goes here
%% Notes
%    Note that input XMc,YMc etc that are used by Uros's original script
%    are multiplied by dx and dy and start at zero upon their creation in 
%    corresponding *Center_Calculation.m scripts. Like this:
%         XMc = (XMc-1)*dx;
%         YMc = (YMc-1)*dy;
%   I introduce coresponding changes.

opt.diagnostic = 1;

if nargin>3
    opt = unite_options(opt,in_opt);
end


%% Front matter
diagnostic = opt.diagnostic;

errors=[];


%% Correspondences
% populate XMc etc
        WPR.XMc = [];
        WPR.YMc = [];
        OPR.XMc = [];
        OPR.YMc = [];
        X3D = [];
        Y3D = [];
        Z3D = [];

for i = 1:size(vol.markerIntristicCoords3D,1)
    inOPR = find(OPR.To3DMarkerCorrespondence==i);
    inWPR = find(WPR.To3DMarkerCorrespondence==i);
    if ~isempty(inOPR) && ~isempty(inWPR)
        WPR.XMc = [WPR.XMc ;(WPR.markerIntristicCoords2D(inWPR,1)-1)*WPR.dx];
        WPR.YMc = [WPR.YMc;(WPR.markerIntristicCoords2D(inWPR,2)-1)*WPR.dy];
        OPR.XMc = [OPR.XMc ;(OPR.markerIntristicCoords2D(inOPR,1)-1)*OPR.dx];
        OPR.YMc = [OPR.YMc;(OPR.markerIntristicCoords2D(inOPR,2)-1)*OPR.dy];
        X3D = [X3D;(vol.markerIntristicCoords3D(i,1)-1)*vol.dx];
        Y3D = [Y3D;(vol.markerIntristicCoords3D(i,2)-1)*vol.dy];
        Z3D = [Z3D;(vol.markerIntristicCoords3D(i,3)-1)*vol.dz];
    end
end

% Return prematurely if not enough  markers are present
errors.nmarkers = size(WPR.XMc,1);
if errors.nmarkers<3
	OPR_r = OPR;
	WPR_r = WPR;
		errors.mTRE = Inf;
	errors.FLE = Inf;
	errors.FRE = Inf;
    varargout{1} = [];
    varargout{2} = [];
    varargout{3} =errors;
    varargout{4} = WPR_r;
    varargout{5} = OPR_r;
	warning('calibrate.m:: not enough markeres detected for calibration');
	return
end
% FINAL PARAMETERS
Tct = eye(4);

X2D = [WPR.XMc OPR.XMc];
Y2D = [WPR.YMc OPR.YMc];

% matched fiducials in 2D and 3D images
NM = size(X3D,1);% number of fiducial markers used for registration

% initial parameters
IN = [];
dx = [];
dy = [];
if diagnostic
    figure
end
for j = {WPR OPR}
        [Nv,Nu] = size(j{1}.im);   
        u0i = (Nu-1)*j{1}.dx/2;
        v0i = (Nv-1)*j{1}.dy/2;     
        IN = [IN;j{1}.DistanceSourceToPatient;j{1}.DistanceSourceToDetector;...
            j{1}.PrimaryAngle;j{1}.SecondaryAngle;u0i;v0i];        
        %add dx and dy in order to feed them to stereo_Carm_calibration_Shecter
        dx = [dx j{1}.dx];
        dy = [dy j{1}.dy];
end


% refinement of calibration parameters
SM_PD = @(Tr) (stereo_Carm_calibration_Shecter(X3D,Y3D,Z3D,X2D,Y2D,dx,dy,Tr));     
IN_r = powell(IN, SM_PD, 1/100000000000, 3000);
[d,xs,ys,zs,Tx] = SM_PD(IN_r);
disp('Calibration is over')


% estimation of 3D error
for i = 1:NM
    
    for j = 1:2

        % 2D points in woorld coordinate system
        p = Tx(:,:,j)*[X2D(i,j);Y2D(i,j);0;1];

        x0(j) = p(1);
        y0(j) = p(2);
        z0(j) = p(3);

    end       

    % 3D reconstruction of i-th point on 3D skeleton based on
    % epipolar geometry in world coordinate system           
    [XW(i,1),YW(i,1),ZW(i,1)] = point_3D_rec(xs,ys,zs,x0,y0,z0);
        
end
Tct_reg = svdT([X3D Y3D Z3D],[XW,YW,ZW]);

% 3D markers in world coordinate system
pp = zeros(NM,4);
for i = 1:NM
    pp(i,:) = Tct_reg*[X3D(i);Y3D(i);Z3D(i);1];                
end   


%%
%% visualization
DataType = 'float';

im = {WPR.im OPR.im};
for j = 1:2
   
    % load and squeeze to 3D matrix

    XDSA = im{j};
    [N,M] = size(XDSA);    
    
    % MIP in initial gold standard position
    Txc = Tct_reg\Tx(:,:,j);
    p = Tct_reg\[xs(j);ys(j);zs(j);1];
%% VISUALIZATION
if diagnostic
    
    xsc = p(1);
    ysc = p(2);
    zsc = p(3);    


    [X2Dp(:,j),Y2Dp(:,j)] = proj_skel(X3D,Y3D,Z3D,Txc,dx(j),dy(j),xsc,ysc,zsc,Tct);   
% %    [X2Sp(:,j),Y2Sp(:,j)] = proj_skel(XCTOp,YCTOp,ZCTOp,Txc,dx(j),dy(j),xsc,ysc,zsc,Tct);   



    subplot(1,2,j)
    imagesc(XDSA)
    colormap gray
    
    hold on
    scatter(X2D(:,j)/dx(j) + 1, Y2D(:,j)/dy(j) + 1,'gx');%,2.5,[0 1 0],'filled')
    scatter(X2Dp(:,j), Y2Dp(:,j),25,[1 0 0],'+')    
%    scatter(X2Sp(:,j), Y2Sp(:,j),1,[0 1 0],'filled')    
    hold off  
    
    legend({'detected','projected'},'Location','Best');
    
end

%%
    Txf(:,:,j) = Txc;
    xsf(j) = p(1);
    ysf(j) = p(2);
    zsf(j) = p(3);    
    %% return values
    % 
    
    outHeader.M = M;
    outHeader.N= N;
    outHeader.dx = dx(j);
    outHeader.dy = dy(j);
    outHeader.DataType = DataType;
    outHeader.Txf  = Txf(:,:,j);
    outHeader.xsf  = xsf(j);
    outHeader.ysf  = ysf(j);
    outHeader.zsf  = zsf(j);
    
    varargout{j} = outHeader;
 
    

end

[mTRE,TRE,FLE,FRE] = point_reg_error(X3D,Y3D,Z3D,X2D,Y2D,Txf,xsf,ysf,zsf,X3D,Y3D,Z3D);
disp(['FRE is ',num2str(FRE),' and mTRE is ',num2str(mTRE)]);

%% return errors
errors.mTRE = mTRE;
errors.TRE= TRE;
errors.FLE = FLE;
errors.FRE = FRE;
varargout{3} = errors;

%% return augmented WPR and OPR structures
WPR_r = WPR;
WPR_r.DistanceSourceToPatient=IN_r(1);
WPR_r.DistanceSourceToDetector=IN_r(2);
WPR_r.PrimaryAngle=IN_r(3);
WPR_r.SecondaryAngle=IN_r(4);
WPR_r.PrincipalPointUX=IN_r(5);
WPR_r.PrincipalPointVY=IN_r(6);
varargout{4} = WPR_r;

OPR_r = OPR;
OPR_r.DistanceSourceToPatient=IN_r(7);
OPR_r.DistanceSourceToDetector=IN_r(8);
OPR_r.PrimaryAngle=IN_r(9);
OPR_r.SecondaryAngle=IN_r(10);
OPR_r.PrincipalPointUX=IN_r(11);
OPR_r.PrincipalPointVY=IN_r(12);
varargout{5} = OPR_r;

vol_r = vol;
vol_r.Tct_reg = Tct_reg;
varargout{6} = vol_r;

end






