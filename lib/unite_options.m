function [ out_opt] = unite_options( out_opt, in_opt )
%UNITE_OPTIONS unite two structures, overwrite fields if necessary
%   This is an utility for option parsing, it merges out_opt and in_opt,
%   andoverwrites fields in out_opt if necessary
% assert(isstruct(out_opt));
% assert(isstruct(in_opt));


% out_fields = fields(out_opt);
% for f = out_fields
%     if ~isfield(in_opt,f)
%         in_opt.(f)=out_opt(f);
%     end
% end


in_fields = fields(in_opt);
for g = in_fields.'
    out_opt = setfield(out_opt,g{:},in_opt.(g{:}));
end

end

