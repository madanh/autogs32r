function h = gena_show( a , varargin)
%GENA_SHOW Summary of this function goes here
%   Detailed explanation goes here


if size(a,2)==3
 h = scatter3(a(:,1),a(:,2),a(:,3),varargin{:});

else
    if size(a,2)~=2 && size(a,1)==2
        a = transpose(a);
    else
%          fprintf(2,'you are the KEYBOARD, LOL \n');
%         keyboard;
    end
    h = scatter(a(:,1),a(:,2),varargin{:});
end
end