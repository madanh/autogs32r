function [out,centre,radius] = ball_processor(X,Y,Z,roidata,in_opt)

cm = jet;
opt.diagnostic = 1;
opt.n_interpoints = 1000;
opt.ball_radius_in_px = 2.5;
centre = [];
radius = [];
if ~isempty(in_opt)
    opt = unite_options(opt,in_opt);
end

%find the good isovalue (max gradient point)
[gy,gx,gz] =gradient(roidata);

g = sqrt(gx.^2+gy.^2+gz.^2);


[g_max,g_max_ind] =max(g(:));
isoval = roidata(g_max_ind);
if isoval==0
    out = false;
    return
end
%get isosurface
iso_struct = isosurface(X,Y,Z,roidata,isoval);
if any([isempty(iso_struct.faces) isempty(iso_struct.vertices)])
    out = false;
    return
end
n_boundary_edges = size(boundaryedges(iso_struct.faces),1);
%% OUTPUT
out = n_boundary_edges==0;

%% DIAG
if opt.diagnostic && out
    figure
    %         clear the refitting figure if not clean yet
    h = 313373; %elite, you know
    if ishandle(h) && strcmp(get(h, 'type'), 'figure'); clf(h); end
    %show it
    %     figure;
    %     title(['Number of boundary edges = ',num2str(n_boundary_edges)]);
    % subplot(2,2,1)
    %     pcolor3(X,Y,Z,roidata,'alphalim',[0 10000]);
    %     caxis([0 10000])
    %     axis vis3d
    
    p=patch(iso_struct);
    set(p,'FaceColor','green');
    set(p,'FaceAlpha',0.1);
    set(p,'EdgeAlpha',0.3);
    set(p,'LineWidth',2);
    axis tight
    camlight left;
    lighting gouraud
    axis vis3d
    daspect([1 1 1])
            view(3)
    hold on
     set(gca,'visible','off');
end







if out
    %% SPHERE FITTING, YAAAAY!
    %get normals at isovertices
    n(:,1)=interpn(X, Y, Z, gx, iso_struct.vertices(:,1), iso_struct.vertices(:,2), iso_struct.vertices(:,3),'cubic');
    n(:,2)=interpn(X, Y, Z, gy, iso_struct.vertices(:,1), iso_struct.vertices(:,2), iso_struct.vertices(:,3),'cubic');
    n(:,3)=interpn(X, Y, Z, gz, iso_struct.vertices(:,1), iso_struct.vertices(:,2), iso_struct.vertices(:,3),'cubic');
    nn = bsxfun(@rdivide,n.',sqrt(dot(n.',n.'))).'; %normalize the normals,lol
    % prepare for "ultrares" sampling
    
    F = griddedInterpolant(X, Y, Z, g, 'cubic');
    %     F1  = griddedInterpolant(X, Y, Z, g, 'spline');
    sample_points = linspaceNDim((iso_struct.vertices-nn),(iso_struct.vertices+nn),opt.n_interpoints);
    
    if opt.diagnostic
        for i = 1:size(sample_points,1)
            gena_line([sample_points(i,:,1);iso_struct.vertices(i,:,1)],'Color','blue');
            gena_line([iso_struct.vertices(i,:,1);sample_points(i,:,end)],'Color','red');
        end
    end
    % reshapology follows, but this is just line profiles
    lineprof = reshape(F(reshape(permute(sample_points,[2,1,3]),3,[],1).'),size(sample_points,1),[]);
    [~,mind] = max(lineprof,[],2);
    edge_points = cell2mat(arrayfun(@(x) sample_points(x,:,mind(x)),(1:size(sample_points,1)).','UniformOutput',false));
    
%     if opt.diagnostic
%         %         figure
%         gena_show(edge_points,'.','MarkerEdgeColor',cm(1,:))
%         axis vis3d
%         daspect([1 1 1])
% 
%     end
    
    % Ahh, ecstasy!
    [Center_LSE,Radius_LSE] = sphereFit(edge_points);
    
%     if opt.diagnostic
%         [Base_X,Base_Y,Base_Z] = sphere(20);
%         surf(Radius_LSE*Base_X+Center_LSE(1),...
%             Radius_LSE*Base_Y+Center_LSE(2),...
%             Radius_LSE*Base_Z+Center_LSE(3),'faceAlpha',0.05,'Facecolor','m','EdgeAlpha',0.05);
%         axis vis3d
%         daspect([1 1 1])
%     end
    
    
    outliers = 1;
    counter = 1;
    while counter<2 % any(outliers)
        %% FIND OUTLIERS
        d = bsxfun(@minus,edge_points,Center_LSE);
        d = sqrt(sum(d.^2,2));
        res = (d-Radius_LSE).^2;
        sigma = sqrt(var(res));
        outliers = res>3*sigma;
        %         disp(edge_points(outliers,:))
        %% REFIT WITHOUT OUTLIERS
        edge_points(outliers,:)  = [];
        
        [Center_LSE,Radius_LSE] = sphereFit(edge_points);
        
        %% SHOW A SPHERE
        if opt.diagnostic
            
            figure(313373);
            if exist('sphere_patch','var')
                delete(sphere_patch)
            end
            
            current_color = cm(counter,:);
            gena_show(edge_points,'.','MarkerEdgeColor',current_color)
            hold on
            gena_show(Center_LSE,'+','MarkerEdgeColor',current_color)
            [Base_X,Base_Y,Base_Z] = sphere(20);
            sphere_patch = surf(Radius_LSE*Base_X+Center_LSE(1),...
                Radius_LSE*Base_Y+Center_LSE(2),...
                Radius_LSE*Base_Z+Center_LSE(3),'faceAlpha',0.05,'Facecolor','c','EdgeAlpha',0.05);
            axis vis3d
            daspect([1 1 1])
            title(['iter:',num2str(counter),' inliers:',num2str(size(edge_points,1))]);
             set(gca,'visible','off');
            drawnow
        end
        counter = counter+1;
    end
        centre = Center_LSE;
    radius = Radius_LSE;
    %% KONSTANTIN REEVES IMPERSONATION:
    if out
        %build a ball around the centroid
        %     centroid = mean(iso_struct.vertices);
%         centroid = mean([X(1),X(end);Y(1) Y(end);Z(1) Z(end)],2);
        centroid = centre;
        insides = sqrt((X-centroid(1)).^2+(Y-centroid(2)).^2+(Z-centroid(3)).^2)...
            <=radius; %opt.ball_radius_in_px+1;
        weights =  roidata(insides);
        weights = weights-min(weights);
        weightedMean = @(coords) sum(coords.*weights(:))./sum(weights(:));
        centre = cellfun(weightedMean,{X(insides),Y(insides),Z(insides)});
%         radius = opt.ball_radius_in_px;
    end
    

end



if opt.diagnostic && out
    fprintf('isoval:%d  centre:%d  radius %d\n\n',isoval,centre,radius);
    
    set(p,'FaceAlpha',0.1);
    set(p,'FaceColor','yellow');
    figure(102);
    clf;cla;
    imagesc((g(:,:,round(size(g,3)/2))));
    colormap(gray);
    set(gca,'visible','off');
    the_slice = get(gca,'Children')
    the_slice = the_slice.CData
    [~,gmi] = max(the_slice)
    [~,gmi] = max(the_slice(:))
    [gmx,gmy] = ind2sub(size(the_slice),gmi)
    hold on
    scatter(gmy,gmx,'bx')
    
end


%% get weighted centroids from thresholding (emulate human set)
% if out
%     bw = roidata>=0.8*isoval;
%     CC = bwconncomp(bw,6);
%     vol = cellfun(@numel,CC.PixelIdxList);
%     [~,largestccind] = max(vol);
%     inp = CC.PixelIdxList{largestccind};
%     roidata = roidata-isoval;
%     centre = cellfun(@(x) (sum(x(inp).*roidata(inp))./sum(roidata(inp))),{X,Y,Z});
%
% end

%% get weighted centroids inside the isosurf
% if out
%     inp = inpolyhedron(iso_struct.faces,iso_struct.vertices,[X(:),Y(:),Z(:)],'FlipNormals',true);
%     centre = cellfun(@(x) (sum(x(inp).*roidata(inp))./sum(roidata(inp))),{X,Y,Z});
%     if opt.diagnostic
%         gena_show(centre,40,'g','MarkerFaceColor','g')
%         gena_show(centre,100,'r+','MarkerFaceColor','g')
%     end
% %     centre = mean(iso_struct.vertices);
% end

%% GRADIENT RELATED STUFF
%try gradient

% subplot(2,2,3)
% pcolor3(g);
% axis vis3d
%
% g_iso_struct = isosurface(g,0.5*g_max);
%
% subplot(2,2,4)
% p=patch(g_iso_struct);
% set(p,'FaceColor','green');
% set(p,'FaceAlpha',0.5);
% axis tight
% camlight left;
% lighting gouraud
% axis vis3d
% drawnow

end

