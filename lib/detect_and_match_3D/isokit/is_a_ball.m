function [out,centre] = is_a_ball(X,Y,Z,roidata,in_opt)
opt.diagnostic = 1;
opt.ball_radius_in_px = 2.5;
centre = [];
    
if ~isempty(in_opt)
    opt = unite_options(opt,in_opt);
end

%find the good isovalue (max gradient point)
[gx,gy,gz] =gradient(roidata);
g = sqrt(gx.^2+gy.^2+gz.^2);


[g_max,g_max_ind] =max(g(:));
isoval = roidata(g_max_ind);
if isoval==0
    out = false;
    return
end
%get isosurface
iso_struct = isosurface(X,Y,Z,roidata,isoval);
if any([isempty(iso_struct.faces) isempty(iso_struct.vertices)])
    out = false;
    return
end
n_boundary_edges = size(boundaryedges(iso_struct.faces),1);

if opt.diagnostic
    %show it
%     figure; 
%     title(['Number of boundary edges = ',num2str(n_boundary_edges)]);
    % subplot(2,2,1)
%     pcolor3(X,Y,Z,roidata,'alphalim',[0 10000]);
%     caxis([0 10000])
%     axis vis3d
    hold on
    p=patch(iso_struct);
    % set(p,'FaceColor','black');
    set(p,'FaceAlpha',0.02);
    set(p,'EdgeAlpha',0.02);
    axis tight
    camlight left; 
    lighting gouraud
    axis vis3d
end

out = n_boundary_edges==0;
if opt.diagnostic && out 
    fprintf('isoval:%d\n',isoval); 
    set(p,'FaceAlpha',0.04);
    set(p,'FaceColor','red');
    
end


%% get weighted centroids in attempt to emulate Konstantin (hi Keano)
if out
    inp = inpolyhedron(iso_struct.faces,iso_struct.vertices,[X(:),Y(:),Z(:)],'FlipNormals',true);
    centre = cellfun(@(x) (sum(x(inp).*roidata(inp))./sum(roidata(inp))),{X,Y,Z});
    if opt.diagnostic
        gena_show(centre,40,'g','MarkerFaceColor','g')
        gena_show(centre,100,'r+','MarkerFaceColor','g')
    end
     centroid = mean(iso_struct.vertices);
     


end

%% GRADIENT RELATED STUFF
%try gradient

% subplot(2,2,3)
% pcolor3(g);
% axis vis3d
% 
% g_iso_struct = isosurface(g,0.5*g_max);
% 
% subplot(2,2,4)
% p=patch(g_iso_struct);
% set(p,'FaceColor','green');
% set(p,'FaceAlpha',0.5);
% axis tight
% camlight left; 
% lighting gouraud
% axis vis3d
% drawnow

end