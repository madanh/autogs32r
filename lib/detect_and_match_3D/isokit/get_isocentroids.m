fnames = dir('rois*.mat');
fnames = {fnames.name};
for i = 1:numel(fnames)
    fprintf(2,'i=%02d\n',i);
    fname = fnames(i);
    rois = load(fname{:});
    rois = rois.rois;
    [~,centroids] = cellfun(@is_a_ball,rois,'UniformOutput',false);
    fprintf(2,'nmarkers:%d\n',nmarkers);
    vertcat(centroids{:})
    close all
    
end