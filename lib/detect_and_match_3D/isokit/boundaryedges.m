function edges=boundaryedges(tri)
% generates the list of boundary edges of a triangulation

% sort each triangle on vertex. This will
% also sort the edges.
tri = sort(tri,2);

% all edges
edges = [tri(:,[1 2]);tri(:,[1 3]);tri(:,[2, 3])];

% sortrows to bring replicated edges together
edges = sortrows(edges);

% find replicated edges
k = find(all(diff(edges)==0,2));

% delete the replicated edges
edges([k;k+1],:) = [];
end