function gena_line(a,varargin)
    x = [a(1,1); a(end,1)];
    y = [a(1,2); a(end,2)];
    z = [a(1,3); a(end,3)];
    line(x,y,z,varargin{:});
    
end