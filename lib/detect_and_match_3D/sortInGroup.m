function order = sortInGroup(inMarkerCoords,sortdim)
%% Detect edge markers
distMatrix = squareform(pdist(inMarkerCoords));
[maxDist] = max(distMatrix(:));
[edge1,edge2] = find(distMatrix == maxDist,1);
%% Pick the topmost edge marker
if inMarkerCoords(edge1,3)>inMarkerCoords(edge2,sortdim)
    top = edge1;
else
    top = edge2;
end
%% Sort by distance to the topmost edge marker
[~, order] =  sortrows(distMatrix,top);


end