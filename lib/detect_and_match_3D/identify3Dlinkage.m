function [ vol] = identify3Dlinkage( vol ,in_options)
%IDENTIFY3D split markers into 3 groups of 4 -top, bottom, middle and sort
%markers in each group by distance from the seed marker. Seed marker is the
%topmost in top and middle (first and third) groups, and the bottom one in
%the bottom (second) group
%   Detailed explanation goes here

%% IMPORTANT: NOW THIS FUNCTION WORKS ONLY WITH 3 GROUPS




%% DEFAULTS
opt.single_link_cutoff_threshold = 26;
opt.diagnostic = 1;
opt.group_size = 4; % 4 markers in one group
opt.reverse_order = false;
opt.sortdim = 3;


%% OPTIONS

opt = unite_options(opt,in_options);

%% INIT
markerCoords3d = vol.markerIntristicCoords3D.*repmat([vol.dx, vol.dy, vol.dz],size(vol.markerIntristicCoords3D,1),1);
%% GET CLUSTERS
nMarkers = size(markerCoords3d,1);

markerLinkage = linkage(markerCoords3d);
markerClusters = cluster(markerLinkage,'cutoff',opt.single_link_cutoff_threshold,'criterion','distance');

if opt.diagnostic
    figure
    dendrogram(markerLinkage);
    hold on
    line([0;14],[opt.single_link_cutoff_threshold;opt.single_link_cutoff_threshold],'LineStyle',':','Color','r')
    xlabel('marker number');
    ylabel('distance, mm');

end

%% GET NUMBER OF ELEMENTS IN CLUSTERS
nClusters = max(markerClusters);
for i = 1:nClusters
    numberOfMarkersInACluster(i) = sum((markerClusters==i));
end


%% remove too small clusters
smallClusters = find(numberOfMarkersInACluster<(opt.group_size-1))';
if ~isempty(smallClusters)
    [~,indecesOfEqualElements] = intersect(markerClusters,smallClusters);
%     counter(indecesOfEqualElements)=0;
    markerClusters(indecesOfEqualElements) = 0;
   
end

%% aggregate clusters
[markerClusters,order] = sort(markerClusters,'descend');
 vol.markerIntristicCoords3D =  vol.markerIntristicCoords3D(order,:);
 vol.markerIntristicCoords3D(markerClusters==0,:) = [];
%     markerCoords3d(counter==0,:) = [];
    markerClusters(markerClusters==0)=[];

if unique(markerClusters)<3
    ME= MException('identify3dlinkage:reduce_threshold','Not enough valid markers detected to make thre groups');
    throw(ME);
end






end


