function [ vol, varargout ] = detect3dGradOtsuIso( vol, in_options)
%AUTOMARKER3D Summary of this function goes here
%   Detailed explanation goes here

varargout{1} = [];
%% DEFAULTS
opt.ct_threshold = 10000;
opt.maximum_number_of_3d_markers = 120;
opt.diagnostic = 1;
opt.ball_radius_in_mm = 1;
opt.max_paxes_ratio = 1.45;

%% OPTIONS
opt = unite_options(opt,in_options);



% thr = opt.ct_threshold;%10000;
nmarkers = opt.maximum_number_of_3d_markers;%120;
diagnostic =opt.diagnostic;
% fiducialVolumeRangeInPixels = [0 4].*(1.33*pi*opt.ball_radius_in_mm.^3/(vol.dx*vol.dy*vol.dz));

%% VARS
opt.ball_radius_in_px = opt.ball_radius_in_mm./vol.dx; %isotropic sampling assumed


%% Enchance markers - gauss
% sigma = 2*opt.ball_radius_in_mm./vol.dx; %isotropic sampling assumed
% kern_size = round(3*sigma);
% filter_force = 10;
% filtered_data = smooth3(vol.data,'gaussian',kern_size,sigma);
% filtered_data = smooth3(vol.data,'box',ceil(sigma));
% filtered_data = vol.data - filtered_data;
% filtered_data(filtered_data<0)=0;

%% Enchance markers - blobness
% filtered_data = BlobnessFilter3D(vol.data,struct('ScaleRange',[1 1].*opt.ball_radius_in_mm./vol.dx));
% varargout{1} = filtered_data;

%% Enchance markers - nothing
filtered_data = vol.data;

%% Threshold the enchanced volume with quntile (volume based)
% fid_volume = round(12*1.33*pi*opt.ball_radius_in_mm.^3/(vol.dx*vol.dy*vol.dz));
% sorted_filtered_data = sort(filtered_data(:),'descend');
% thr = sorted_filtered_data(fid_volume)
% bw = sorted_filtered_data;
% clear sorted_filtered_data;
% bw = filtered_data>=thr;

%% Threshold gradient using otsu
[gy,gx,gz] =gradient(vol.data); %to see why gy,gx look at gradient.m code,when you see it you will cry, I promise.
g = sqrt(gx.^2+gy.^2+gz.^2);
thr = graythresh(g(:)).*max(g(:));
bw = g>thr;

%% show selected
% h = figure('Visible','off');
% show_3dbw(bw);
% set(h,'CreateFcn','set(gcf,''visible'',''on'')'); 
% drawnow;
% saveas(h,fullfile(vol.inPatientFolder,'selected_markers.fig'),'fig');

%% return prematurely
% markerCenters = [];
% return;

%% Prepare for diagnostic visualization
if diagnostic
    figure; hold on;
end

% THESE dx's are used in diagnostic visualization, 
% but for now they are just ones
    dxc = 1;
    dyc = 1;
    dzc = 1;
%% Get connected components

CC = bwconncomp(bw, 6);


%% filter out some bad blobs
S = regionprops(CC,'BoundingBox');
% bounding box should not exceed 4 radii
bbox_side_limit = 4*opt.ball_radius_in_px;
bbox = vertcat(S.BoundingBox);

bad_bbox_large = any(bbox(:,4:6)>bbox_side_limit,2);

% OPTIONAL: remove small speckle-like candidates
bad_bbox_small = any(bbox(:,4:6)<=2,2);

% OPTIONAL: remove elongated
% Let's get only those

CC.removeindex = bad_bbox_small|bad_bbox_large;
% CC.removeindex = zeros(1,numel(CC.PixelIdxList));%prepare to remove all bad blobs
% for i = 1:numel(CC.PixelIdxList)
%     blob = CC.PixelIdxList{i};
%     [bx,by,bz] = ind2sub(size(vol.data),blob);
%     if (numel(blob) > max(fiducialVolumeRangeInPixels)) || (numel(blob) < min(fiducialVolumeRangeInPixels))% || any(paxes(bx,by,bz))>8 || any(paxes(bx,by,bz))<4
%         %if the blob is too big or small (in span/voxel count terms) to
%         %be a marker
%         CC.removeindex(i) = 1;
%         
%         if diagnostic; scatter3((bx-1).*dxc,(by-1).*dyc,(bz-1).*dzc,8,[0 0 0],'o'); end;% then plot it black
% 
%     else %if the blob has about the right voxel count
%         tttt = paxes(bx,by,bz);
%         if ~isempty(tttt)
%             r12 = tttt(1)/tttt(2);
%             r23 = tttt(2)/tttt(3);
%             if any([r12,r23]>opt.max_paxes_ratio) || any([r12,r23]<1/opt.max_paxes_ratio)
%                 CC.removeindex(i) = 1;
% 
%                 if diagnostic; scatter3((bx-1).*dxc,(by-1).*dyc,(bz-1).*dzc,8,[0 0 1],'o');end; % then plot it blue
%             else
% 
%                 if diagnostic; scatter3((bx-1).*dxc,(by-1).*dyc,(bz-1).*dzc,8,[1 0 0],'o'); end; % then plot it red
%             end
%         end
%     end
%     
% end

CC = purge_CC(CC);

%% IF DIAGNOSTIC
if diagnostic
    show_3dbw(bw,8,'k.');
    hold on;
    axis vis3d;
end

if diagnostic
    for i = 1:numel(CC.PixelIdxList)
        blob = CC.PixelIdxList{i};
        [bx,by,bz] = ind2sub(CC.ImageSize,blob);
        scatter3((bx-1).*dxc,(by-1).*dyc,(bz-1).*dzc,9,[0 0 0],'r.');
    end
end
%% GET CENTROIDS TO BE USED AS SEEDS
S = regionprops(CC,'centroid');
    for i = 1:numel(CC.PixelIdxList)
        blob = CC.PixelIdxList{i};
        [bx,by,bz] = ind2sub(CC.ImageSize,blob);
        CC.centroid{i} = [mean(bx(:)),mean(by(:)),mean(bz(:))];
    end
    
    
%% DEVEL: GENERATE ROIS
if diagnostic
    figure;
    hold on;
end

for i = 1:numel(CC.centroid)
    coord_range = @(x) round(x-2*opt.ball_radius_in_px):round(x+2*opt.ball_radius_in_px);
    cr= cell(1,3);
    for j = 1:3
        cr{j} = coord_range(CC.centroid{i}(j));
        cr{j}(cr{j}<1)=[];
        cr{j}(cr{j}>CC.ImageSize(j))=[]; 
    end
    CC.data{i} = vol.data(cr{1},cr{2},cr{3}); % MOTLOB, PLEASE!
    [X,Y,Z] = ndgrid(cr{1},cr{2},cr{3});
    [CC.removeindex(i),CC.centre{i},CC.radius{i}] = ball_processor(X,Y,Z,CC.data{i},opt); % CLOSED ISOSURFACE HELP SIFT OUT NOT-BALLS
end

CC.removeindex = ~CC.removeindex;


CC = purge_CC(CC);

% remove boru with smoru radius
CC.removeindex = ([CC.radius{:}]>opt.ball_radius_in_px*2) | ([CC.radius{:}]<opt.ball_radius_in_px*0.5);
CC = purge_CC(CC);

markerCenters = vertcat(CC.centre{:});


%% GET THE INTENSITY WEIGHTED MEAN COORDINATES (TO BE USED AS CENTERS)
% a small anon function for the lazy me
% weightedMean = @(coords,weights) sum(coords.*weights)./sum(weights);
% for i = 1:numel(CC.PixelIdxList)
%     blob = CC.PixelIdxList{i};
%     [bx,by,bz] = ind2sub(size(vol.data),blob);
%     CC.cx(i,1)= weightedMean(bx,vol.data(blob)-thr); % Here I subtract the threshold so the it matches
%     CC.cy(i,1)= weightedMean(by,vol.data(blob)-thr); % the formula (2.1) from Dejan Tomazevic's thesis
%     CC.cz(i,1) = weightedMean(bz,vol.data(blob)-thr); % ref:(Bose and Amir 1990; Chiorboli and Vecchi 1993)
%     
% end
% markerCenters = [CC.cx,CC.cy,CC.cz];
%% ESTABLISH ORDER!

% [~, order] = sortrows(markerCenters,3);
% order = flipud(order);
% nmarkers = min(nmarkers,size(order,1));
% order = order(1:nmarkers);
% markerCenters = markerCenters(order(1:nmarkers),:);

%% OUTPUT

vol.markerIntristicCoords3D = markerCenters;

end

function [span] =  span(x,y,z)
span = [max(x)-min(x);max(y)-min(y);max(z)-min(z)];

end

function out = paxes(x,y,z)
%return principal axes as singular values
try
    [u,s,v] = svd([x-mean(x),y-mean(y),z-mean(z)]);
    out = [s(1,1),s(2,2),s(3,3)];
    %         [s(1,1),s(2,2),s(3,3)]
catch
    out = [];
end
end

function CC = purge_CC(CC)
    n = numel(CC.removeindex);   
    for fieldname = fieldnames(CC).'
        if numel(CC.(fieldname{1}))==n && ~strcmp(fieldname{1},'removeindex')
            CC.(fieldname{1})(logical(CC.removeindex))=[];
        end
    end
    CC.NumObjects = CC.NumObjects -sum(CC.removeindex);
    CC.removeindex = [];
end



