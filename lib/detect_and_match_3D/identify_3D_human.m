function [ vol ] = identify_3D_human( vol ,in_options)
%IDENTIFY_3D_HUMAN load human provided markers from raw DB
%   Has to be used only with the "old" database, not Hybrid (or
%   *3D_Markers.mat files have to be copied into corresponding folders of
%   Hybrid database)

opt.filename = '*3D_Markers.mat';
opt.sortdim = 3;

if nargin>1
    unite_options(opt,in_options)
end



%% LOAD THE DATA FOR vol.fname
the_full_file_name = dir(fullfile(vol.inPatientFolder,'..',opt.filename));
the_full_file_name = the_full_file_name(1).name;
the_full_file_name = fullfile(vol.inPatientFolder,'..',the_full_file_name);
uros = load(the_full_file_name);
uros.markerIntristicCoords3D  = [uros.XMc./vol.dx uros.YMc./vol.dy uros.ZMc./vol.dz]+1;

% G = {1:4,5:8,9:12};
% for g = 1:3
%     vol.markerIntristicCoords3D(G{g},:) = sortInGroup(uros.markerIntristicCoords3D(G{g},:);
% end

%% OVERWRITE vol

vol.markerIntristicCoords3D = uros.markerIntristicCoords3D;
end

