function [ markerCenters, varargout ] = detectMarker3d( vol, in_options)
%AUTOMARKER3D Summary of this function goes here
%   Detailed explanation goes here
%% DEFAULTS
opt.ct_threshold = 10000;
opt.maximum_number_of_3d_markers = 120;
opt.diagnostic = 1;
opt.ball_radius_in_mm = 1;
opt.max_paxes_ratio = 1.45;

%% OPTIONS
opt = unite_options(opt,in_options);



thr = opt.ct_threshold;%10000;
nmarkers = opt.maximum_number_of_3d_markers;%120;
diagnostic =opt.diagnostic;
fiducialVolumeRangeInPixels = [0.66 1.33].*(1.33*pi*opt.ball_radius_in_mm.^3/(vol.dx*vol.dy*vol.dz));
%% Prepare for diagnostic visualization
if diagnostic
    figure; hold on;
end

% THESE dx's are used in diagnostic visualization, 
% but for now they are just ones
    dxc = 1;
    dyc = 1;
    dzc = 1;



%% Threshold
thr = opt.ct_threshold;%10000;
nmarkers = opt.maximum_number_of_3d_markers;%120;


%% Get connected components
CC = bwconncomp(vol.data>thr, 6);


% Let's get only those

CC.removeindex = zeros(1,numel(CC.PixelIdxList));%prepare to remove all bad blobs
for i = 1:numel(CC.PixelIdxList)
    blob = CC.PixelIdxList{i};
    [bx,by,bz] = ind2sub(size(vol.data),blob);
    if (numel(blob) > max(fiducialVolumeRangeInPixels)) || (numel(blob) < min(fiducialVolumeRangeInPixels))% || any(paxes(bx,by,bz))>8 || any(paxes(bx,by,bz))<4
        %if the blob is too big or small (in span/voxel count terms) to
        %be a marker
        CC.removeindex(i) = 1;
        
        if diagnostic; scatter3((bx-1).*dxc,(by-1).*dyc,(bz-1).*dzc,8,[0 0 0],'o'); end;% then plot it black

    else %if the blob has about the right voxel count
        tttt = paxes(bx,by,bz);
        r12 = tttt(1)/tttt(2);
        r23 = tttt(2)/tttt(3);
        if any([r12,r23]>opt.max_paxes_ratio) || any([r12,r23]<1/opt.max_paxes_ratio)
            CC.removeindex(i) = 1;
            
            if diagnostic; scatter3((bx-1).*dxc,(by-1).*dyc,(bz-1).*dzc,8,[0 0 1],'o');end; % then plot it blue
        else
       
            if diagnostic; scatter3((bx-1).*dxc,(by-1).*dyc,(bz-1).*dzc,8,[1 0 0],'o'); end; % then plot it red
        end
    end
    
end

CC.PixelIdxList(logical(CC.removeindex))=[];
CC.NumObjects = CC.NumObjects -sum(CC.removeindex);
CC.removeindex = [];

%% GET THE INTENSITY WEIGHTED MEAN COORDINATES (TO BE USED AS CENTERS)
% a small anon function for the lazy me
weightedMean = @(coords,weights) sum(coords.*weights)./sum(weights);
for i = 1:numel(CC.PixelIdxList)
    blob = CC.PixelIdxList{i};
    [bx,by,bz] = ind2sub(size(vol.data),blob);
    CC.cx(i,1)= weightedMean(bx,vol.data(blob)-thr); % Here I subtract the threshold so the it matches
    CC.cy(i,1)= weightedMean(by,vol.data(blob)-thr); % the formula (2.1) from Dejan Tomazevic's thesis
    CC.cz(i,1) = weightedMean(bz,vol.data(blob)-thr); % ref:(Bose and Amir 1990; Chiorboli and Vecchi 1993)
    
end
markerCenters = [CC.cx,CC.cy,CC.cz];
[~, order] = sortrows(markerCenters,3);
order = flipud(order);
nmarkers = min(nmarkers,size(order,1));
order = order(1:nmarkers);
markerCenters = markerCenters(order(1:nmarkers),:);
varargout{1} = CC.PixelIdxList(order(1:nmarkers));


end

function [span] =  span(x,y,z)
span = [max(x)-min(x);max(y)-min(y);max(z)-min(z)];

end

function out = paxes(x,y,z)
%return principal axes as singular values
try
    [u,s,v] = svd([x-mean(x),y-mean(y),z-mean(z)]);
    out = [s(1,1),s(2,2),s(3,3)];
    %         [s(1,1),s(2,2),s(3,3)]
catch
    out = [];
end
end

