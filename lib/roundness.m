function [ outim ] = roundness(im, ksize)
%ROUNDNESS Summary of this function goes here
%   Detailed explanation goes here
test =1
if test 
    ksize = 5;
end
numelKernel = (2*ksize+1).*(2*ksize+1);
x1lim = size(im,1)-ksize-1;
x2lim = size(im,2)-ksize-1;
outim = zeros(size(im));
%% quick and dirty and stupid
parfor i = ksize+1:x1lim
    for j = ksize+1:x2lim
        a = im(i-ksize:i+ksize,j-ksize:j+ksize);
        
        
        
        a=a-sum(a(:),'double')/numelKernel;
        %An (error prone) check that the circle is dark:
        if a(ksize,ksize)>0; outim(i,j)=0;continue;end
        
        sigmaASigmaB=sum(sum(a.*a));
        if sigmaASigmaB==0; outim(i,j)=0;continue;end

        t1 =sum(sum(a.*a'));
        if t1<=0; continue;end
       
        t2 = sum(sum(a.*flip(a,1)));
        if t2<=0; continue;end

        t3 = sum(sum(a.*flip(a,2)));
        if t3<=0; continue;end
        
        t4 = sum(sum(a.*fliplr(a')));
        if t4<=0; continue;end
%         kernelValues=kernelValues/3*std(kernelValues(:));
%         t1 = kernelValues-kernelValues';
%         t2 = kernelValues-flipud(kernelValues);
%         t3 = kernelValues-fliplr(kernelValues);
%         t4 = kernelValues-fliplr(kernelValues');
        

%         outim(i,j) = abs(max(kernelValues(:))-min(kernelValues(:)))*t;
%         outim(i,j) = var(kernelValues(:))*var(t1(:))*var(t2(:))*var(t3(:))*var(t4(:));
        scalingFactor = sigmaASigmaB./numelKernel;
         outim(i,j) = scalingFactor*t1*t2*t3*t4/(sigmaASigmaB^4);%
    end
end

