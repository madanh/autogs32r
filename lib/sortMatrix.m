function out = sortMatrix(a,order)

t = a(:,order);
out = t(order,:);
end
