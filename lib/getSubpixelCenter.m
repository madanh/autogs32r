function out_center = getSubpixelCenter(in,index)
    %CONSTANCTS
    diagnostic = false;
    MARGINS = 16;
    % PREPARATIONS
    % Prepare a circular mask
    R = MARGINS;
    [x1,x2] = ndgrid(-R:R,-R:R);
    circMask = (sqrt((x1.*x1+x2.*x2))-R)<0;
    
    
    coords = round(in.markerIntristicCoords2D(index,:));
    ROI = in.im(coords(2)-MARGINS:coords(2)+MARGINS,coords(1)-MARGINS:coords(1)+MARGINS);
    BW = edge(ROI,'canny');
    
    [g,d] = imgradient(ROI);
    
    if diagnostic
        figure(2);
        subplot(1,3,1);
        imagesc(BW);
    end
    
%% MASK ALL
    g = g.*circMask;
    d = d.*circMask;
    BW = BW.*circMask;
    
    subX=[];
    subY=[];
    for i = find(BW)'
        [r,c] = ind2sub(size(BW),i);
        
        if abs(tand(d(i)))<=1
            deltax = 3;
            deltay = deltax*tand(d(i));
        else
            deltay = 3;
            deltax = deltay/tand(d(i));
        end

        
         [cx,cy,value] = improfile(g,[c+deltax;c-deltax],[r-deltay;r+deltay],1000,'bicubic');
         [~,maxind] = max(value);

         
         subX=[subX; cx(maxind)];
         subY=[subY; cy(maxind)];

    end
%% FIT THE ELLIPSE
    a = fit_ellipse(subX,subY);
    if diagnostic
        
       figure(2);
       subplot(1,3,3);
        imagesc(g);
        hold on;
        scatter(subX,subY,'r.');
        [X,Y] = meshgrid(1:size(g,2),1:size(g,1));
        axis image
        contour((a(1)*X.^2+a(2)*X.*Y+a(3)*Y.^2+a(4)*X+a(5)*Y+a(6)),[0,0],'r');
    end
    
%% FIND THE CENTER!
    % maths from here http://www.geom.uiuc.edu/docs/reference/CRC-formulas/node28.html
 
    
%      For the central conics (the ellipse, the point, and the hyperbola), the center (x,y) is the solution of the system of equations
% 
% 2Ax+Cy+D=0,
% Cx+2By+E=0,
% 
% and the axes have slope q and -1/q, where q is given by (2) . 

       A = a(1);
       B = a(3); % this is correct! (because different authors use different letters for coeffs)
       C = a(2); % this is correct!
       D = a(4);
       E = a(5);
       F = a(6);
       
       lhs = [2*A C; C 2*B];
       rhs = [-D; -E];
       
       the_center = lhs\rhs;
       the_center = the_center';
       if diagnostic
           figure(2);
            subplot(1,3,2);
           hold off;
           imagesc(ROI);
           axis image
           hold on;
        scatter(the_center(1),the_center(2),'+')
        drawnow
       end
       %% OLOLO
      
       if diagnostic
            subplot(1,3,1);
         imagesc(BW); axis image;
        hold on;
         scatter(the_center(1),the_center(2),'+')
         drawnow
       end
       
       out_center = coords + the_center - size(g')./2;
end
