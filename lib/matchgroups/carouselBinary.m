function [ outim ] = carouselBinary(in_im, r)
%CAROUSEL detect circles from gradient direction image

if r<3
    warning('r<3:, keyboarding');
    keyboard;
end

R = ceil(r);
%% get gradient directions image
[~,im] = imgradient(in_im);


%% Prepare a template for given radius
templ = carousel_template(R);
tsupport = templ~=0;
% maxchain = sum(tsupport(:))
tolerance = (180/pi)*4/R; %see yellow notebook for proof
%% iterate through image pixels and see the longest chain of pixels that matches the template
%ROUNDNESS Summary of this function goes here
%   Detailed explanation goes here
ksize = R;
% index_of_the_center = 5;
% numelKernel = (2*ksize+1).*(2*ksize+1);
x1lim = size(im,1)-ksize-1;
x2lim = size(im,2)-ksize-1;
outim = false(size(im));

%% quick and dirty and stupid
parfor i = ksize+1:x1lim
    for j = ksize+1:x2lim
        a = im(i-ksize:i+ksize,j-ksize:j+ksize);
        c = a(tsupport)-templ(tsupport);
        c = min(abs(c),abs(c-360));
        outim(i,j)=all(c<tolerance);
        
    end
end


end





