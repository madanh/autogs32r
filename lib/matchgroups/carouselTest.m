load('Utilities/test3.mat'); % creates a variable 'test' with the test image in it
[g,d]=imgradient(test);
figure;imagesc(d); axis image; colormap jet;

%% test the atan2 image
% simulatedD = carousel_template(8);

% figure;imagesc(simulatedD); axis image; colormap jet;

%% different radii
for r = 12:-1:1
c{r} = carousel(test,r);
figure;
imagesc(c{r}==1);
title(num2str(r));
end