function [ simulatedD] = carousel_template( R )
%CAROUSEL_TEMPLATE Summary of this function goes here
%   Detailed explanation goes here
[x1,x2] = ndgrid(-R:R,-R:R);
% circular mask
circMask = abs(sqrt((x1.*x1+x2.*x2))-R)<0.5;

simulatedD = -atan2d(x1,x2).*circMask;%+360*(rand(size(x1))-0.5).*~circMask;

end

