function [ s ] = cleanup_the_scene( s ,t)
%CLEANUP_THE_SCENE dedupe the scene s with distance threshold t

while 1
dm = squareform(pdist(s));
[r,c] = find(dm<t & dm>0);
if isempty(r);break;end;
% s(r,:)  = [mean([s(r,1),s(c,1)],2)  mean([s(r,2),s(c,2)],2)];
s(c,:) = [];

end

end

