function [out2d ] = identify_exhaust_sac( in2d, vol, in_options )
%IDENTIFY_RANSAC find correspondences between markers in 2d and 3d

%% DEFAULT OPTIONS
opt.diagnostic =1 ;
opt.full = 0;

opt.ball_radius_for_cleanup = 8;
opt.error_correction_factor = 0.5;

% exhaust_sac options
opt.inlier_distance_threshold =4; %[px]


%% UNITE OPTIONS
opt = unite_options(opt,in_options);

%% GET MODEL POINTS
%% = INIT
SOD = in2d.DistanceSourceToPatient;
SID = in2d.DistanceSourceToDetector;
PA = in2d.PrimaryAngle;
SA = in2d.SecondaryAngle;

u0 = in2d.dx*(size(in2d.im,2))/2;
v0 = in2d.dy*(size(in2d.im,1))/2;
% overwrite u0 v0 if in2d is an already registered projection
if isfield(in2d,'PrincipalPointUX')
    u0 = in2d.PrincipalPointUX;
    v0 = in2d.PrincipalPointVY;
end




%% = PROJECT THE MARKERS FROM 3D TO 2D
%
%   There is a problem that we don't know the location of the isocenter.
%   We use the center of the 3D volume as the isocenter. Then we define a
%   coordinate system Ciso oriented in the same way as patient system but
%   with origin in this estimated isocenter. This is achieved by simply
%   replacing the translation column (fourth) of the vol.toPatientMatrix
%   with minus half width/height/depth of the image.
%     [nx,ny,nz] = size(vol.data);
centerCoordsWorld= ((size(vol.data)-1).*[vol.dx vol.dy vol.dz])/2;
tx = -centerCoordsWorld(1);
ty = -centerCoordsWorld(2);
tz = -centerCoordsWorld(3);

toCisoMatrix = vol.toPatientMatrix;
toCisoMatrix(:,4) = [tx ty tz 1]';
homogeneousMarkerCoordsCt = [vol.markerIntristicCoords3D';ones(1,size(vol.markerIntristicCoords3D,1))];

homogeneousMarkerCoordsCiso = toCisoMatrix*homogeneousMarkerCoordsCt;
[P,R,t] = Carm_Madan(SOD,SID,PA,SA); % This projection matrix returns
% values in mm and with origin in center of the image!
UVWprime = P*homogeneousMarkerCoordsCiso;

% next two lines get normal coords from homogenious
u1=UVWprime(1,:)./UVWprime(3,:);
u2=UVWprime(2,:)./UVWprime(3,:);
%next two lines move the origin to pixel 1,1 and go from mm to px
im1 = u1./in2d.dx+size(in2d.im,2)/2+0.5;
im2 = u2./in2d.dy+size(in2d.im,1)/2+0.5;
projectedMarkerCoords=[im1',im2'];




%% = FINALLY WRITE OUT THE MODEL POINTS
modelPointIndex{1} = 1:4;
modelPointIndex{2} = 5:8;
modelPointIndex{3} = 9:12;
for i = 1:numel(modelPointIndex)
    modelPoints{i} = projectedMarkerCoords(modelPointIndex{i},:);
    
end
%% = CALCULATE BALL RADIUS AND INTERBALL DISTANCE IN PROJECTION FORM PHYSICAL PARAMETERS
assert(abs(in2d.dy-in2d.dx)<0.001); % assert isotropic sampling in X-ray
% 0-order  approximation
if isfield(opt,'ball_radius_in_mm')
    opt.maximum_radius_in_projection = floor(SID/(SOD-max(size(vol.data))/2)/(in2d.dx)*opt.ball_radius_in_mm);
    opt.minimum_radius_in_projection = floor(SID/(SOD+max(size(vol.data))/2)/(in2d.dx)*opt.ball_radius_in_mm);
end

opt.max_interball_distance_in_projection = SID/SOD/in2d.dx*opt.interball_distance_in_mm*1.1+2*opt.maximum_radius_in_projection;
%% get scene points


if ~isfield(in2d,'markerIntristicCoords2D')
    [scenePoints,r_i,w_i,P] = feval(opt.detect_2d_function_name,in2d.im,opt);
    % scenePoints = cleanup_the_scene(scenePoints,opt.ball_radius_for_cleanup);
    
    if opt.diagnostic

    figure;

    imagesc(P); axis image; hold on;
    gena_show(projectedMarkerCoords);
    drawnow

        viscircles(scenePoints,r_i,'EdgeColor',[0.5 0 0],'DrawBackground',false);
        drawnow
    end
    
    %     [w_sorted,w_index] = sort(w_i,'descend');
    %     num_markers_to_keep = 2*size(vol.markerIntristicCoords3D,1);
    %     if num_markers_to_keep<=size(scenePoints,1)
    %         w_thr = w_sorted(2*num_markers_to_keep);
    %         scenePoints = scenePoints(w_i>=w_thr,:);
    %         r_i = r_i(w_i>=w_thr,:);
    %     end
else
    scenePoints = in2d.markerIntristicCoords2D;
end

%% GROUPWISE exhaust_sac
nm = size(modelPoints{1},1);
ns = size(scenePoints,1);
clear best

for j = 1:3
    best(j) = exhaust_sac(scenePoints,modelPoints{j},P,opt);
    
end
for j = 1:3
    best(j).modelPointIndex = modelPointIndex{j};
end


%% ERROR CORRECTION
[b,ix] = sort([best.cost],'ascend');
other_indeces = 2:numel(ix);
corrected = [];
if b(1)<mean(b(other_indeces))*opt.error_correction_factor
    corrected.tform = mean(cat(3,best(ix(other_indeces)).tform),3);
    corrected.tmp = [modelPoints{ix(1)} ones(nm,1)]*corrected.tform;
    corrected.to_show =corrected.tmp(:,1:2);
end


%% DIAGNOSTIC VIS

if opt.diagnostic
    figure;
    hold off;
    imagesc(in2d.im);
    hold on;
    
    viscircles(scenePoints,r_i,'EdgeColor',[0.75 0.25 0.25],'DrawBackground',false);
    
    
    %initial
    gena_show(projectedMarkerCoords,'o','MarkerEdgeColor',[1 0.5 0]);
end

if opt.diagnostic
    for j = 1:3
        if best(j).cost~=realmax
            to_show = [modelPoints{j} ones(nm,1)]*best(j).tform;
            gena_show(to_show(:,1:2),'o','MarkerFaceColor',[0.5 0 1]);
            text(to_show(1,1)+16,to_show(1,2),num2str(log10(best(j).cost)),'Color',[0.9 0 0.9]);
            %             gena_show(best(j).model_transformed_and_refined,'.','MarkerFaceColor',[0 0.5 0]);
        else
            text(0.5,0.5,['Group number',num2str(j),'did not converge']);
        end
        
        if ~isempty(corrected)
            gena_show(corrected.to_show,'+','MarkerEdgeColor',[0.2 0.2 1]);
        end
    end
    drawnow;
    
    %                     legend({'scene';'initial';'tformed';'inliers'});
    try
        [~,fn] = fileparts(in2d.fname)
        saveas(gcf,['./diag/ransac/fig',fn,'.png'],'png');
    catch ME
        warning([mfilename,'could not save diagnostic figure',ME.message])
    end
    
end

%          keyboard;

%% OVEWRITE THE WORST GROUP WITH CORRECTED VALUES
if ~isempty(corrected)
    roi_margin = opt.maximum_radius_in_projection+opt.maximum_radius_in_projection;
    fprintf(2,'identify_exhaust_sac: error correction has kicked in\n');
%     [~, ~, TreeRoot] = kdtree( scenePoints, []);
    
    % return correspondences
    best(ix(1)).model_transformed_and_refined = [modelPoints{ix(1)} ones(nm,1)]*corrected.tform;
    best(ix(1)).model_transformed_and_refined(:,3)=[];
    %get closest points
%     [ Idx, ~, TreeRoot ] = kdtreeidx([], modelTransformed(:,1:2), ...
%         TreeRoot);
    % rerun ransac on the closest points
% best_corrected = exhaust_sac(scenePoints(Idx,:),modelTransformed(:,1:2),P,opt);
    
%     %overwrite the worst group with corrected correspondences
%     correspondences_corrected(inliers_corrected>0) = Idx(correspondences_corrected(inliers_corrected>0));
%     correspondences{ix(1)} = correspondences_corrected;
%     correspondences{ix(1)}(~inliers_corrected) =0;

% format matching
% best_corrected.modelPointIndex = best(ix(1)).modelPointIndex;
   
    % reassign the corrected values
%     best(ix(1)) = best_corrected;
    % release the memory
%     kdtreeidx([], [], TreeRoot);

% get a ROI around the group

    points1 = [modelPoints{ix(1)} ones(nm,1)]*best(ix(end)).tform;
    points1(:,3)=[];
    
    points2 = [modelPoints{ix(1)} ones(nm,1)]*best(ix(end-1)).tform;
    points2(:,3)=[];
    
    points = [points1;points2];
    
    ul = bsxfun(@max,min(points)-roi_margin,[1 1]);
    br = bsxfun(@min,max(points)+roi_margin,fliplr(size(in2d.im)));
    
    groi = in2d.im(ul(2):br(2),ul(1):br(1));%group roi
    
    % process inside the roi
    % get group points
        optgroi = opt;
    optgroi.ngroups = 1;
    [scenePointsgroi,r_igroi,w_igroi,~] = feval(opt.detect_2d_function_name,groi,optgroi);
    Pgroi = P(ul(2):br(2),ul(1):br(1));

    best_correctedgroi = exhaust_sac(scenePointsgroi,modelPoints{ix(1)},Pgroi,optgroi);
    
    best_correctedgroi.model_transformed_and_refined=bsxfun(@plus,best_correctedgroi.model_transformed_and_refined,ul);
    best_correctedgroi.modelPointIndex = best(ix(1)).modelPointIndex;
    best(ix(1)) = best_correctedgroi;
    

end
%% Attempts to get subpixel circles and save some undetected at the same time

% GET ROIS
roi_margin = opt.maximum_radius_in_projection+opt.minimum_radius_in_projection;
for j = 1:numel(best)
%     subs_into_im= linspaceNDim(max(best(j).model_transformed_and_refined-roi_margin,ones(size(best(j).model_transformed_and_refined))),...
%         best(j).model_transformed_and_refined+roi_margin,...
%         2*roi_margin+1);
    upper_left_corner = max(best(j).model_transformed_and_refined-roi_margin,ones(size(best(j).model_transformed_and_refined)));
    lower_right_corner = bsxfun(@min,best(j).model_transformed_and_refined+roi_margin,fliplr(size(in2d.im)));
    k_to_remove = [];
    for k = 1:size(upper_left_corner,1)
%         roi =  in2d.im(squeeze(subs_into_im(k,2,:)),squeeze(subs_into_im(k,1,:)));
        roi =  in2d.im(upper_left_corner(k,2):lower_right_corner(k,2),...
            upper_left_corner(k,1):lower_right_corner(k,1));
        [refined_center,r_i,w_i] = harness_hoola(roi,opt);
        
        
        if ~isempty(refined_center)
            %% DELETEME!!! down
                %#debug
%                 saveas(302,['a',num2str(randi(10000)),'.png']);
%                  saveas(304,['b',num2str(randi(10000)),'.png']);
                
                %% DELETEME !! up
            % if multiple matches, pick the one closest to the initial guess
            refined_center = refined_center-roi_margin-1;
            if size(refined_center,1)>1
                
                [~,ind] = min(sum(refined_center.^2,2));
                refined_center = refined_center(ind,:);
            end
            best(j).model_transformed_and_refined(k,:) = refined_center+best(j).model_transformed_and_refined(k,:);
        else
            k_to_remove = [k_to_remove, k];
        end
    end
    best(j).model_transformed_and_refined(k_to_remove,:) = [];
    best(j).modelPointIndex(k_to_remove) = [];
    

    
end



%% CATER THE OUTPUT
in2d.markerIntristicCoords2D = vertcat(best.model_transformed_and_refined);
in2d.To3DMarkerCorrespondence = [best.modelPointIndex];
out2d = in2d;
end

