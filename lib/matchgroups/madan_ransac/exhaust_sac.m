function [best] = exhaust_sac( scenePoints,modelPoints, P, in_opt )
%VERY_RANSAC very random sample and consensus, lol
%   It's like RANSAC, but you have no idea which points match

%% DEFAULT OPTIONS
opt.points_to_match = 2; %"minimal" sample
opt.diagnostic = 0;

%% ADD EXTRA OPTIONS
opt = unite_options(opt,in_opt);

%% INIT
points_to_match = opt.points_to_match;

nm = size(modelPoints,1);
ns = size(scenePoints,1);

best.cost = -realmax;
best.tform  = zeros(3);
best.model_transformed_and_refined = zeros(size(modelPoints));
[h, w] = size(P);

%% PREPARE THE COMBINATIONS
Mc = nchoosek([1:size(modelPoints,1)],points_to_match);
% now we need ta add combinations that have flipped order
distinguishable_perms = perms([1:points_to_match]);
%Aaaah, the "beauty" of matlab, oui!
Mc = reshape(Mc(:,distinguishable_perms),[],points_to_match);

Sc = nchoosek([1:size(scenePoints,1)],points_to_match);

[Mind,Sind] = ndgrid([1:size(Mc,1)],[1:size(Sc,1)]);

% vis
if opt.diagnostic
    figure(13372);
    hold off
    imagesc(P);
    hold on;
    gena_show(scenePoints,'go');
    gena_show(modelPoints(:,1:2),'gs'); %starting points
    cur_points_handle = gena_show(modelPoints(:,1:2),'c+');
    
    points_handle = gena_show(modelPoints(:,1:2),'r.');
    
    
    drawnow;
end
%% ITER
for i = 1:numel(Mind)
    %match at raqndom
    m = modelPoints(Mc(Mind(i),:),:);
    s = scenePoints(Sc(Sind(i),:),:);
    
    %get tform
    tform = computeSimilarity(m,s); %apply to m to get into s's frame
    
    
    modelTransformed = [modelPoints ones(nm,1)]*tform;
    modelTransformed = round(modelTransformed(:,1:2));
    
    % are all points valid?
    if any(modelTransformed(:,1)>w | modelTransformed(:,1)<1 | modelTransformed(:,2)>h | modelTransformed(:,2)<1)
        continue
    end
    
    %% COST FUNCTION IS HERE
    %% vvvvvvvvvvvvvvvvvvvv
    P_i = sub2ind([h, w],modelTransformed(:,2),modelTransformed(:,1));
    cost = prod(P(P_i))/prod(P(P_i(Mc(Mind(i),:)))).*...
            1/(1+atan2(tform(2),tform(1)).^2).*... %penalize rotation
            1/exp(abs(log(det(tform(1:2,1:2))))); %penalize scaling
    
    %% ^^^^^^^^^^^^^^^^^^^^^
    %% COST FUNCTION IS HERE
    
    %             figure(1003);
    %             scatter(iter,cost);
    %             hold on;
    %             %rinse, repeat
    %             drawnow;
%     if opt.diagnostic
%         set(cur_points_handle,'XData',modelTransformed(:,1),'YData',modelTransformed(:,2));
% %         drawnow;
%     end
    if cost>best.cost
        best.i = i;
        best.tform = tform;
        best.cost = cost;
        best.model_transformed_and_refined  = modelTransformed;
        
        % vis
        if opt.diagnostic
            set(points_handle,'XData',modelTransformed(:,1),'YData',modelTransformed(:,2));
            drawnow;
        end
    end
    
    
    
end
if opt.diagnostic
    gena_show(best.model_transformed_and_refined,'y^');
    drawnow;
end

end

