function [out2d ] = identify_ransac( in2d, vol, in_options )
%IDENTIFY_RANSAC find correspondences between markers in 2d and 3d

%% DEFAULT OPTIONS
opt.diagnostic =0 ;
opt.full = 0;

opt.ball_radius_for_cleanup = 8;
opt.error_correction_factor = 10;

% very_ransac options
opt.inlier_distance_threshold =3; %[px]
opt.itermax = 50000;

%% UNITE OPTIONS
opt = unite_options(opt,in_options);

%% GET MODEL POINTS
%% = INIT
SOD = in2d.DistanceSourceToPatient;
SID = in2d.DistanceSourceToDetector;
PA = in2d.PrimaryAngle;
SA = in2d.SecondaryAngle;

u0 = in2d.dx*(size(in2d.im,2))/2;
v0 = in2d.dy*(size(in2d.im,1))/2;
% overwrite u0 v0 if in2d is an already registered projection
if isfield(in2d,'PrincipalPointUX')
    u0 = in2d.PrincipalPointUX;
    v0 = in2d.PrincipalPointVY;
end

%% = CALCULATE BALL RADIUS IN PROJECTION FORM PHYSICAL PARAMETERS
assert(abs(in2d.dy-in2d.dx)<0.001); % assert isotropic sampling in X-ray
% 0-order  approximation
if isfield(opt,'ball_radius_in_mm')
    opt.maximum_radius_in_projection = floor(SID/SOD/in2d.dx*opt.ball_radius_in_mm);
end


%% = PROJECT THE MARKERS FROM 3D TO 2D
%
%   There is a problem that we don't know the location of the isocenter.
%   We use the center of the 3D volume as the isocenter. Then we define a
%   coordinate system Ciso oriented in the same way as patient system but
%   with origin in this estimated isocenter. This is achieved by simply
%   replacing the translation column (fourth) of the vol.toPatientMatrix
%   with minus half width/height/depth of the image.
%     [nx,ny,nz] = size(vol.data);
centerCoordsWorld= ((size(vol.data)-1).*[vol.dx vol.dy vol.dz])/2;
tx = -centerCoordsWorld(1);
ty = -centerCoordsWorld(2);
tz = -centerCoordsWorld(3);

toCisoMatrix = vol.toPatientMatrix;
toCisoMatrix(:,4) = [tx ty tz 1]';
homogeneousMarkerCoordsCt = [vol.markerIntristicCoords3D';ones(1,size(vol.markerIntristicCoords3D,1))];

homogeneousMarkerCoordsCiso = toCisoMatrix*homogeneousMarkerCoordsCt;
[P,R,t] = Carm_Madan(SOD,SID,PA,SA); % This projection matrix returns
% values in mm and with origin in center of the image!
UVWprime = P*homogeneousMarkerCoordsCiso;

% next two lines get normal coords from homogenious
u1=UVWprime(1,:)./UVWprime(3,:);
u2=UVWprime(2,:)./UVWprime(3,:);
%next two lines move the origin to pixel 1,1 and go from mm to px
im1 = u1./in2d.dx+size(in2d.im,2)/2+0.5;
im2 = u2./in2d.dy+size(in2d.im,1)/2+0.5;
projectedMarkerCoords=[im1',im2'];


if opt.diagnostic
    figure;
    imagesc(in2d.im); colormap gray; axis image; hold on;
    gena_show(projectedMarkerCoords);
    drawnow
end

%% = FINALLY WRITE OUT THE MODEL POINTS

modelPoints{1} = projectedMarkerCoords(1:4,:);
modelPoints{2} = projectedMarkerCoords(5:8,:);
modelPoints{3} = projectedMarkerCoords(9:12,:);

%% get scene points
[scenePoints,~,w_i] = feval(opt.detect_2d_function_name,in2d.im);
% scenePoints = cleanup_the_scene(scenePoints,opt.ball_radius_for_cleanup);

[w_sorted,w_index] = sort(w_i,'descend');
num_markers = size(vol.markerIntristicCoords3D,1);
w_thr = w_sorted(num_markers);
scenePoints = scenePoints(w_i>=w_thr,:);
%% GROUPWISE (very)RANSAC
nm = size(modelPoints{1},1);
ns = size(scenePoints,1);
clear best
for j = 1:3
    [correspondences{j}, inliers{j}, best(j) ] = very_ransac (scenePoints,modelPoints{j},opt);
end



%% ERROR CORRECTION
[b,ix] = sort([best.ssd],'descend');
other_indeces = 2:numel(ix);
corrected = [];
if b(1)>sum(b(other_indeces))*opt.error_correction_factor
    corrected.tform = mean(cat(3,best(ix(other_indeces)).tform),3);
    corrected.tmp = [modelPoints{ix(1)} ones(nm,1)]*corrected.tform;
    corrected.to_show =corrected.tmp(:,1:2);
end


%% DIAGNOSTIC VIS

if opt.diagnostic
    figure;
    hold off;
    imagesc(in2d.im);
    hold on;
    
    gena_show(scenePoints,55,'ko');
    
    %initial
    gena_show(projectedMarkerCoords,'o','MarkerEdgeColor',[1 0.5 0]);
end

if opt.diagnostic
    for j = 1:3
        if best(j).ssd~=realmax
            to_show = [modelPoints{j} ones(nm,1)]*best(j).tform;
            gena_show(to_show(:,1:2),'o','MarkerFaceColor',[1 0 0]);
            text(to_show(1,1)+16,to_show(1,2),num2str(log10(best(j).ssd)),'Color',[0.9 0 0.9]);
            gena_show(best(j).model_transformed_and_refined,'.','MarkerFaceColor',[0 0.5 0]);
        else
            text(0.5,0.5,['Group number',num2str(j),'did not converge']);
        end
        
        if ~isempty(corrected)
            gena_show(corrected.to_show,'+','MarkerEdgeColor',[0.2 0.2 1]);
        end
    end
    drawnow;
    
    %                     legend({'scene';'initial';'tformed';'inliers'});
    try
        [~,fn] = fileparts(in2d.fname)
        saveas(gcf,['./diag/ransac/fig',fn,'.png'],'png');
    catch ME
        warning([mfilename,'could not save diagnostic figure',ME.message])
    end
    
end

%          keyboard;

%% OVEWRITE THE WORST GROUP WITH CORRECTED VALUES
if ~isempty(corrected)
    [~, ~, TreeRoot] = kdtree( scenePoints, []);
    
% return correspondences
    modelTransformed = [modelPoints{ix(1)} ones(nm,1)]*corrected.tform;
    %get closest points
    [ Idx, DistA, TreeRoot ] = kdtreeidx([], modelTransformed(:,1:2), ...
        TreeRoot);
    % rerun ransac on the closest points
    [correspondences_corrected, inliers_corrected, best_corrected ] =...
        very_ransac(scenePoints(Idx,:),modelTransformed(:,1:2),opt);
    
    %overwrite the worst group with corrected correspondences
    correspondences_corrected(inliers_corrected>0) = Idx(correspondences_corrected(inliers_corrected>0));
    correspondences{ix(1)} = correspondences_corrected;
    correspondences{ix(1)}(~inliers_corrected) =0;
    
    best(ix(1)) = best_corrected;
    % release the memory
    kdtreeidx([], [], TreeRoot);
end

%% CATER THE OUTPUT
in2d.markerIntristicCoords2D = scenePoints;
in2d.To3DMarkerCorrespondence = zeros(1,size(scenePoints,1));
c = vertcat(correspondences{:}).';
% below is f*&@^ing magic line, but what it does assign to each detected
% marker the number of corresponding projected marker, or zero
in2d.To3DMarkerCorrespondence(c(c>0)) = find(c>0);
out2d = in2d;


end

