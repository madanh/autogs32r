function [correspondences, inliers, best ] = very_ransac( scenePoints,modelPoints,in_opt )
%VERY_RANSAC very random sample and consensus, lol
%   It's like RANSAC, but you have no idea which points match

%% DEFAULT OPTIONS
opt.itermax = 20000;
opt.inlier_distance_threshold = 3; 

opt.points_to_match = 2;
opt.diagnostic = 0;

%% ADD EXTAR OPTIONS

opt = unite_options(opt,in_opt);


%% INIT

points_to_match = opt.points_to_match;

nm = size(modelPoints,1);
ns = size(scenePoints,1);
% KDTREE DUNNO IF WORTH IT
%%% Build the k-d Tree once from the reference datapoints.
[~, ~, TreeRoot] = kdtree( scenePoints, []);


iter = 0;
best.ssd = realmax;
best.tform  = zeros(3);
best.model_transformed_and_refined = zeros(size(modelPoints));

%% ITER
while iter<opt.itermax
    %match at raqndom
    m = modelPoints(randperm(nm,points_to_match),:);
    s = scenePoints(randperm(ns,points_to_match),:);
    
    %get tform
    tform = computeSimilarity(m,s); %apply to m to get into s's frame
    
    
    modelTransformed = [modelPoints ones(nm,1)]*tform;
    modelTransformed = modelTransformed(:,1:2);
    %calculate inliers (relative to ALL model points)
    [ ClosestPts, DistA, TreeRoot ] = kdtree([], modelTransformed, ...
        TreeRoot);
    inliers = DistA<opt.inlier_distance_threshold;
    ni = sum(inliers);
    if ni >= 2
        % refine the tform (use all inliers)
        tform_refined = computeSimilarity(modelTransformed(inliers,:),...
            ClosestPts(inliers,:)); %apply to m to get into s's frame
        
        model_transformed_and_refined = [modelTransformed(inliers,:) ones(ni,1)]*tform_refined;
        model_transformed_and_refined =model_transformed_and_refined(:,1:2);
        %get an sm
        d = ClosestPts(inliers,:)-model_transformed_and_refined;
        
        %% COST FUNCTION IS HERE
        %% vvvvvvvvvvvvvvvvvvvv
        ssd = max(10000000*(ni==2),sum(sqrt(dot(d,d))))/... %penalize distance, but only if it's not zero( when there are only 2 points)
            (ni*ni)*...%penalize too few inliers
            exp(abs(log(det(tform(1:2,1:2)))))*... %penalize scaling
            abs(atan2(tform(2),tform(1))); %penalize rotation
        %% ^^^^^^^^^^^^^^^^^^^^^
        %% COST FUNCTION IS HERE
        
        %             figure(1003);
        %             scatter(iter,ssd);
        %             hold on;
        %             %rinse, repeat
        %             drawnow;
        
        if ssd<best.ssd
            best.tform = tform*tform_refined;
            best.ssd = ssd;
            best.model_transformed_and_refined = model_transformed_and_refined;
            
            % vis
                            if opt.diagnostic
                                figure(13372);
                                hold off;
                                gena_show(scenePoints,'ko');
                                hold on;
                                gena_show(model_transformed_and_refined(:,1:2),'r.');
                                drawnow;
                            end
        end
    end
    iter = iter+1;
    
end

%% return correspondences
    modelTransformed = [modelPoints ones(nm,1)]*best.tform;
    %calculate inliers (relative to ALL model points)
    [ Idx, DistA, TreeRoot ] = kdtreeidx([], modelTransformed(:,1:2), ...
        TreeRoot);
    inliers = double(DistA<opt.inlier_distance_threshold);
    correspondences = Idx;
    correspondences(~inliers) =0;
    

%% cleanup the memory
    kdtreeidx([], [], TreeRoot);
end

