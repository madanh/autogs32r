function [ out ,b] = get3groups( im, varargin)
%GET3GROUPS lower the threshold until we get 3 groups of 4 markers or
%something similar

%% "CONSTANTS"
TOTAL_MARKERS=12;
MAXIMUM_DISTANCE_MARKERS_OF_THE_SAME_GROUP_IN_A_PROJECTION = 100;
MAX_ITER=100;

if nargin>=2
    MAXIMUM_DISTANCE_MARKERS_OF_THE_SAME_GROUP_IN_A_PROJECTION = varargin{1};
else
    warning('get3groups: second argument - maximum distance between groups is not provided, using 100 px');
end

%% INIT
wereGood=false;
thresh = max(im(:));
assert(all(im(:)>=0));
iter = 0;
while ~wereGood
    iter = iter+1;
    assert(iter<MAX_ITER);
    b = im>=thresh;
    CC = regionprops(b,'Centroid');
    if numel(CC)<TOTAL_MARKERS;
        thresh = lowerTheThreshold(thresh);
        continue;
    end
    markerCenters=vertcat(CC.Centroid);
    
    markerLinkage = linkage(markerCenters);
    markerClusters = cluster(markerLinkage,'cutoff',MAXIMUM_DISTANCE_MARKERS_OF_THE_SAME_GROUP_IN_A_PROJECTION ,'criterion','distance');
    
    %% Do we have 3x4?
    for i = unique(markerClusters)'
        n(i) = sum(markerClusters==i);
        
    end
    if sum(n>=4)>=3
        wereGood=true;
        break;
    end
    %% Do we have 1x4 +2x8?
    if sum(n>=4)>=2 && sum(n>=8)>=1
        wereGood=true;
        break;
    end
%     %% Do we have 1x12? <--highly implausible, so keyboard
%     if sum(n>=12)>=1
%         keyboard
%        
%     end
    
    thresh = lowerTheThreshold(thresh);
    
end
out = markerCenters;
end

function out = lowerTheThreshold(thr,varargin)
    amount = 0.1;
    if nargin>1
        amount=varargin{1};
    end
    out = thr-amount;
    assert(out>0);
end