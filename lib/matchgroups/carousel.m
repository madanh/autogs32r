function [ outim ] = carousel(in_im, r)
%CAROUSEL detect circles from gradient direction image

if r<3
    warning('r<3:?, keyboarding');
    keyboard;
end

R = ceil(r);
%% get gradient directions image
[~,im] = imgradient(in_im);


%% Prepare a template for given radius
templ = carousel_template(R);
tsupport = templ~=0;
maxchain = sum(tsupport(:));
tolerance = (180/pi)*4/R; %see yellow notebook for proof
%% iterate through image pixels and see the longest chain of pixels that matches the template
%ROUNDNESS Summary of this function goes here
%   Detailed explanation goes here
ksize = R;
% index_of_the_center = 5;
numelKernel = (2*ksize+1).*(2*ksize+1);
x1lim = size(im,1)-ksize-1;
x2lim = size(im,2)-ksize-1;
outim = zeros(size(im));

%% quick and dirty and stupid
parfor i = ksize+1:x1lim
    for j = ksize+1:x2lim
        a = im(i-ksize:i+ksize,j-ksize:j+ksize);
        
        %% debugging code
        %             b = zeros(size(a));
        %             b(templ~=0) = a(templ~=0)-templ(templ~=0);
        
        c = a(tsupport)-templ(tsupport);
        c = min(abs(c),abs(c-360));
        
        bw = c<tolerance;
        
        %% find the longest chain of values within tolerance
        %         % to account for chains that cross the zero angle just concatenate
        %         % the profile to itself:
        %
        %           chains= regionprops([bw;bw],'Area');
        % try
        %           outim(i,j) = max(cell2mat(struct2cell(chains))); %<- this is the maximum lenght
        % catch
        %
        % end
        %% find the longest chain of values within tolerance another method
        if ~all(bw)
            b = [bw;bw];
            ac = 0;
            oldac = 0;
            for k = 1:numel(b)
                if b(k)
                    ac=ac+1;
                elseif ac>oldac
                    
                    oldac = ac;
                    ac=0;

                    
                end
                
            end
            outim(i,j) = max(oldac,ac);
            
        else
            outim(i,j)=maxchain;
        end
    end
end


%% normalize output to 1
outim = outim/sum(tsupport(:));

end

