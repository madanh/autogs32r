function [out ] = identify_groups( in2d, vol, in_options )
%IDENTIFY Summary of this function goes here
%   Detailed explanation goes here

%% DEFAULTS
opt.reuse = 0;
opt.marker_projection_jitter_threshold = 3;
opt.maximum_radius_in_projection = 8;
opt.diagnostic = 0;


%% OPTIONS
opt = unite_options(opt,in_options);



%% INIT
SOD = in2d.DistanceSourceToPatient;
SID = in2d.DistanceSourceToDetector;
PA = in2d.PrimaryAngle;
SA = in2d.SecondaryAngle;

u0 = in2d.dx*(size(in2d.im,2))/2;
v0 = in2d.dy*(size(in2d.im,1))/2;
% overwrite u0 v0 if in2d is an already registered projection
if isfield(in2d,'PrincipalPointUX')
    u0 = in2d.PrincipalPointUX;
    v0 = in2d.PrincipalPointVY;
end

%% CALCULATE BALL RADIUS IN PROJECTION FORM PHYSICAL PARAMETERS
assert(abs(in2d.dy-in2d.dx)<0.001); % assert isotropic sampling in X-ray
% 0-order  approximation
if isfield(opt,'ball_radius_in_mm')
    opt.maximum_radius_in_projection = floor(SID/SOD/in2d.dx*opt.ball_radius_in_mm);
end





%% PROJECT THE MARKERS FROM 3D TO 2D
%
%   There is a problem that we don't know the location of the isocenter.
%   We use the center of the 3D volume as the isocenter. Then we define a
%   coordinate system Ciso oriented in the same way as patient system but
%   with origin in this estimated isocenter. This is achieved by simply
%   replacing the translation column (fourth) of the vol.toPatientMatrix
%   with minus half width/height/depth of the image.
%     [nx,ny,nz] = size(vol.data);
centerCoordsWorld= ((size(vol.data)-1).*[vol.dx vol.dy vol.dz])/2;
tx = -centerCoordsWorld(1);
ty = -centerCoordsWorld(2);
tz = -centerCoordsWorld(3);

toCisoMatrix = vol.toPatientMatrix;
toCisoMatrix(:,4) = [tx ty tz 1]';
%     ctToWorld = [vol.dx 0 0 -vol.dx;
%                     0 vol.dy 0 -vol.dy;
%                     0 0 vol.dz -vol.dz;
%                     0 0 0 1];
%     worldToCarm =   [ 1  0 0 0;
%                         0 0 -1 0;
%                         0 1 0 0;
%                         0 0 0 1] ; %change the translations to real ones if needed
homogeneousMarkerCoordsCt = [vol.markerIntristicCoords3D';ones(1,size(vol.markerIntristicCoords3D,1))];
% if ~isfield(vol,'Tct_reg')
% transl = worldToCarm*[-centerCoordsWorld';1];
% vol.Tct_reg = worldToCarm;
% vol.Tct_reg(:,4) = transl;
% end

homogeneousMarkerCoordsCiso = toCisoMatrix*homogeneousMarkerCoordsCt;
[P,R,t] = Carm_Madan(SOD,SID,PA,SA); % This projection matrix returns
% values in mm and with origin in center of the image!
UVWprime = P*homogeneousMarkerCoordsCiso;

% next two lines get normal coords from homogenious
u1=UVWprime(1,:)./UVWprime(3,:);
u2=UVWprime(2,:)./UVWprime(3,:);
%next two lines move the origin to pixel 1,1 and go from mm to px
im1 = u1./in2d.dx+size(in2d.im,2)/2+0.5;
im2 = u2./in2d.dy+size(in2d.im,1)/2+0.5;
projectedMarkerCoords=[im1',im2'];


if opt.diagnostic
    figure;
    imagesc(in2d.im); colormap gray; axis image; hold on;
    gena_show(projectedMarkerCoords);
    drawnow
end

%% GROUPS IN 3D:
temp3d = in2d;
temp3d.markerIntristicCoords2D = projectedMarkerCoords;
markersFrom3d = Projection(temp3d);


%% Projected groups (Group2D is a class that handles group matching in 2D)
g1 = Group2D(markersFrom3d,1:4);
g2 = Group2D(markersFrom3d,5:8);
g3 = Group2D(markersFrom3d,9:12);


%% DETECT MARKERS IN 2D:
MAXIMUM_DISTANCE_MARKERS_OF_THE_SAME_GROUP_IN_A_PROJECTION = ...
    max([g1.maximumIntermarkerDistance(),g2.maximumIntermarkerDistance(),g3.maximumIntermarkerDistance()])+...
    opt.maximum_radius_in_projection; % SHOULD I ADD JITTER HERE??
if ~opt.reuse || ~isfield(in2d,'markerIntristicCoords2D')
    %         in2d.markerIntristicCoords2D = carouselMarker2d(in2d.im,opt.maximum_radius_in_projection,MAXIMUM_DISTANCE_MARKERS_OF_THE_SAME_GROUP_IN_A_PROJECTION);
    
    C = carousel(in2d.im,opt.maximum_radius_in_projection);
    [in2d.markerIntristicCoords2D,b] = get3groups(C,MAXIMUM_DISTANCE_MARKERS_OF_THE_SAME_GROUP_IN_A_PROJECTION);
    in2d.markerIntristicCoords2D = cleanup_the_scene(in2d.markerIntristicCoords2D,opt.maximum_radius_in_projection);
    
    
end
%instantiate Projection class
markersFrom2d = Projection(in2d);


%% MATCHING
%what markers in the 2d image have the same orientation?
m1 = g1.matchingGroup(markersFrom2d,opt.marker_projection_jitter_threshold);
m2 = g2.matchingGroup(markersFrom2d,opt.marker_projection_jitter_threshold);
m3 = g3.matchingGroup(markersFrom2d,opt.marker_projection_jitter_threshold);

[m1,m2] = m1.crossvalidate(m2);
[m1,m3] = m1.crossvalidate(m3);
[m2,m3] = m2.crossvalidate(m3);

% assert that we have at lest some unambiguous matches
assert(any(m1.nummatches==1));
assert(any(m2.nummatches==1));
assert(any(m3.nummatches==1));




in2d.groups = [m1;m2;m3];

%% FINE IDENTIFICATION

groups3d{1} = 1:4;
groups3d{2} = 5:8;
groups3d{3} = 9:12;

for g_index = 1:numel(groups3d)
    in2d.To3DMarkerCorrespondence(in2d.groups(g_index).indices(in2d.groups(g_index).nummatches==1))=groups3d{g_index}(in2d.groups(g_index).nummatches==1);
    
    
end

%% REFINEMENT OF MARKER POSITIONS
% I know this is a bad place, but this code is probably going to fall out
% of usage anyway, and proper refactoring would just be a waste of time
for ind  = 1:size(in2d.markerIntristicCoords2D,1)
    in2d.markerIntristicCoords2D(ind,:)=getSubpixelCenter(in2d,ind);
    % if opt.diagnostic
    %     saveas(gca,strcat('./diag/OPR',num2str(inOPR),'.png'));
    % end
end
out = in2d;


%% vis
if opt.diagnostic
    figure;
    markersFrom2d.peek;
    hold on ;
    m1.show;
    m2.show;
    m3.show;
    
end


end