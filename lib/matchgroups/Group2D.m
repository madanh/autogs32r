classdef Group2D
    %GROUP2D Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        N
        %         Nmax
        markerCoords
        indices
        MAX_DISTANCE=80; %maximal distance between marker to be considered in
        %belonging to one group in 2D
        
        MAX_DISTANCE_SLACK_RATIO = 1.1;
        SMALL_CLUSTER_SIZE=1;
        VERTICAL_DIMENSION = 2;
        
        % Ambigouisness of marker matching, used in matching
        nummatches;
        
        
        %% HARDCODED CONSTANTS TO BE MOVED INTO CONFIGS!
        
    end
    
    methods
        function self = Group2D(inProjection, indices, varargin)
            % either instance of Projection class and  indeces of
            % elements, Nmax - maximum number of elements in a grouip
            %             self.Nmax = Nmax; %TODO: change to read from configs
            self.markerCoords = inProjection.markerIntristicCoords2D(indices(indices~=0),:);
            self.N = sum(indices~=0);
            self.indices = indices;
            
            if nargin>2
                self.nummatches = varargin{1};
            end
            

            
            
            %% set the max distance:
            shortest = sort(pdist(self.markerCoords));
            if numel(shortest)>1
                shortest = shortest(1:self.N);
            end
            self.MAX_DISTANCE=max(shortest)*self.MAX_DISTANCE_SLACK_RATIO;
            
        end
        
        
        function out = orientation(self)
            % Find orientation of the group
            
            %TODO: check that mutual orientations are consistent
            % Screw all, just use the simplest possible method:
            
            rel = self.markerCoords(2:end,:)- repmat(self.markerCoords(1,:),self.N-1,1);
            ang = rel(:,1)./rel(:,2);
            out = mean(ang);
            
            
        end
        
        
        function [markers,matchtable] = findInProjection(self,inProjection,thr)
            %             c = {'lol' 'cat' 'idk'};
            diagnostic = 0;
            while 1
                % inProjection of class Projection
                %             thr =  20; %should be equal to some multiple of marker projection diameter
                % y = ax+b
                a = self.orientation;
                %find what value b will take
                bextr = [0; inProjection.Nu-a*inProjection.Nv;a*inProjection.Nv;inProjection.Nu];
                bmin = min(bextr);
                bmax = max(bextr);
                
                %exhaustive search
                step = 1;
                %             NinProjection = size(inProjection.markerIntristicCoords2D,1);
                matchtable = [];% zeros(NinProjection,numel(bmin:step:bmax));
                for b = bmin:step:bmax
                    
                    matchtable = [matchtable,...
                        abs(a*inProjection.markerIntristicCoords2D(:,2)+b-inProjection.markerIntristicCoords2D(:,1))<thr];
                    
                    
                    
                    
                end
                [m,ind] = max(sum(matchtable));
                if diagnostic
                    figure;imagesc(matchtable);
                end
                s =sum(matchtable);
                c = bwconncomp(s==m);
                if c.NumObjects>2
                    thr = thr*1.5;
                    continue
                end
                for i =1:c.NumObjects
                    [row,~] = find(matchtable(:,c.PixelIdxList{i}));
                    c(i).row = unique(row);
                    c(i).link = min(min(pdist2(inProjection.markerIntristicCoords2D(row,:),self.markerCoords)));
                end
                [~,groupind] = min([c.link]);
                markers = c(groupind).row;
                %             markers = find(matchtable(:,ind));
                break
            end
        end
        
        
        %% MATCHING HERE!
        function [outGroup2D] = matchingGroup(self,inProjection,thr)
            opt.inlier_distance_threshold = thr; %options for ransac
            [corresp,inliers,best] = very_ransac(inProjection.markerIntristicCoords2D,self.markerCoords,opt);
            outGroup2D = Group2D(inProjection,corresp.',inliers.');
        end
        
        
        %% Croos validation (not THAT cross validation) of groups
            
            function [self,another] = crossvalidate(self,another)
                [~,ia,ib] = intersect(self.indices(self.indices>0),another.indices(another.indices>0));
                self.nummatches(ia)=2;
                another.nummatches(ib)=2;
            end
        
        
        
        
        
        function [index_in_projection,coords,internal_index] = theTopMarker(self)
            %             self.VERTICAL_DIMENSION = 2;
            [temp,temp_index] = sortrows(self.markerCoords,self.VERTICAL_DIMENSION);
            coords = temp(1,:);
            internal_index = temp_index(1);
            index_in_projection = self.indices(internal_index);
        end
        
        
        %% SHOW
        function show(self,varargin)
            %             gena_show(self.markerCoords);
            gena_text(self.markerCoords,varargin{:});
        end
        
        function r = rel(self)
            r = [0 0;diff(self.markerCoords)];
        end
        
        
        %% ANGLES
        function angles = angles(self);
            rel = self.rel;
            for i = 2:size(rel,1)-1
                angles(i) = pi - acos(dot(rel(i,:),rel(i+1,:))/(norm(rel(i,:))*norm(rel(i+1,:))));
            end
        end
        
        
        %% ENERGY
        function energy = energy(self, another)
            delta_phi = self.angles-another.angles;
            energy = dot(delta_phi,delta_phi);
        end
        
        %% MAXIMUM INTER MARKER DISTANCE
        function distmax = maximumIntermarkerDistance(self)
            distmax=max(sqrt(dot(self.rel',self.rel')));
        end
    end
    
end

