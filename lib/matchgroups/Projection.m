classdef Projection < matlab.mixin.Copyable
    %PROJECTION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        im
        dx
        dy
        PrimaryAngle
        SecondaryAngle
        DistanceSourceToDetector
        DistanceSourceToPatient
        markerIntristicCoords2D
        order
        %image size
        Nv
        Nu
        
        %principal point
        u0
        v0
    end
    
    methods
        %% CONSTRUCTOR (FROM LEGACY STRUCTURE)
        function self = Projection(in_struct)
            p = {'im'
                'dx'
                'dy'
                'PrimaryAngle'
                'SecondaryAngle'
                'DistanceSourceToDetector'
                'DistanceSourceToPatient'
                'markerIntristicCoords2D'};
            for i = 1:numel(p)
                self.(p{i})=in_struct.(p{i});
            end
            
            % if order field exists (markers identified externally), copy
            % it
            if isfield(in_struct,'order')
                self.order = in_struct.order;
            else
                self.order = [];
            end
            
            [Nv,Nu] = size(self.im);
            self.Nv = Nv;
            self.Nu = Nu;
            self.u0 = (Nu-1)*self.dx/2;
            self.v0 = (Nv-1)*self.dy/2;
            
        end
        
        %% GET Projection matrices
        function [P,R,t] =  getP(self)
            [P,R,t] = Carm_Shecter(self.DistanceSourceToPatient,self.DistanceSourceToDetector,self.PrimaryAngle,self.SecondaryAngle,self.u0,self.v0);
        end
        
        %% GET A TO WORLD MATRIX
        function Tx=toWorld(self)
            [~,R,t] =  getP(self);
            Twc = eye(4);
            Twc(1:3,1:3) = R;
            Twc(1:3,4) = t; %go to affine coords
            
            %             Tcw = inv(Twc); %inverse transform
            
            %             X-ray source`coordinates in world coordinate system
            %             xs = Tcw(1,4);
            %             ys = Tcw(2,4);
            %             zs = Tcw(3,4);
            
            %             X-ray imaging plane to camera coordinate system
            T2Dc = rigid2T(-self.u0,-self.v0,-self.DistanceSourceToDetector,0,0,0);
            
            %             X-ray imaging plane to woorld coordinate system
            Tx = Twc\T2Dc; %This is same as left multiply by inverse, but more precise
        end
        
        
        
        
        %% GET COORDINATES OF POINTS IN IMAGE PLANE IN 3D
        function out = markerWorldInplaneCoordinates(self)
            %% TODO: THIS MIGHT BE INCORRECT:
            % it's the way it is found in Uros's scripts and it kinda
            % works, but more logically would be to invert y axis and count
            % form top.
            X2D = (self.markerIntristicCoords2D(:,1)-1).*self.dx;
            Y2D = (self.markerIntristicCoords2D(:,2)-1).*self.dy;
            homoMarkerInplaneCoordinates = self.toWorld()*[X2D';Y2D';zeros(size(X2D'));ones(size(X2D'))];
            out = homoMarkerInplaneCoordinates(1:3,:)';
        end
        
        %% GET WORLD COORDINATES OF SOURCE
        function [coords, varargout] = sourceWorldCoordinates(self)
            % this function returns all coorfinates as a column vector in
            % first output, but also each coordinate separately in optional
            % outputs. This is to overcome the trully cosmic MATLAB
            % cretinism in a less ugly way then it is usually done.
            [~,R,t] =  getP(self);
            Twc = eye(4);
            Twc(1:3,1:3) = R;
            Twc(1:3,4) = t; %go to affine coords
            Tcw = inv(Twc); %inverse transform
            
            % X-ray source`coordinates in world coordinate system
            xs = Tcw(1,4);
            ys = Tcw(2,4);
            zs = Tcw(3,4);
            coords = [xs;ys;zs];
            varargout{1} = xs;
            varargout{2} = ys;
            varargout{3} = zs;
        end
        
        
        %% RECONSTRUCT
        function [reconstructedWorldMarkerCoordinates, errors] = reconstruct(self,anotherProjection)
            %% NOTE: point_3D_rec may be vectorized, but can't waste time to do this - doing Uros way
            
            % prepare input for point_3D_rec
            % source position
            xs = zeros(2,1);
            ys = zeros(2,1);
            zs = zeros(2,1);
            
            [~,xs(1),ys(1),zs(1)] = self.sourceWorldCoordinates;
            [~,xs(2),ys(2),zs(2)] = anotherProjection.sourceWorldCoordinates;
            
            % prepare point coords for point_3D_rec
            p1 = self.markerWorldInplaneCoordinates;
            p2 = anotherProjection.markerWorldInplaneCoordinates;
            for i = 1:numel(self.order)
                
              
                
                x0(1) = p1(i,1);
                y0(1) = p1(i,2);
                z0(1) = p1(i,3);
                
                x0(2) = p2(i,1);
                y0(2) = p2(i,2);
                z0(2) = p2(i,3);
                
           
            
            
            
            [XW(i,1),YW(i,1),ZW(i,1),errors(i)] = point_3D_rec(xs,ys,zs,x0,y0,z0);
            reconstructedWorldMarkerCoordinates = [XW,YW,ZW];
             end
        end
        
        %% RECEIVE A MARKER FROM ANOTHER PROJECTION AND FIGURE OUT THE 
        % Associated reconstruction errors
        
        function [errors] = reconstructionErrorMatrix(self,anotherProjection)
            %% NOTE: point_3D_rec may be vectorized, but can't waste time to do this - doing Uros way
            diagnostic = 1;
            % prepare input for point_3D_rec
            % source position
            xs = zeros(2,1);
            ys = zeros(2,1);
            zs = zeros(2,1);
            
            [~,xs(1),ys(1),zs(1)] = self.sourceWorldCoordinates;
            [~,xs(2),ys(2),zs(2)] = anotherProjection.sourceWorldCoordinates;
            
            % prepare point coords for point_3D_rec
            p1 = self.markerWorldInplaneCoordinates;
            p2 = anotherProjection.markerWorldInplaneCoordinates;
            
%             preallocate output matrix
                errors = -ones(size(p1,1),size(p2,1));
            if diagnostic
                figure
                scatter3(p1(:,1),p1(:,2),p1(:,3));
                hold on
                scatter3(p2(:,1),p2(:,2),p2(:,3),'g')
                axis image
                
            end
                spec = 'r.';
            for i = 1:size(p1,1)
                for j = 1:size(p2,1)
                
              
                
                x0(1) = p1(j,1);
                y0(1) = p1(j,2);
                z0(1) = p1(j,3);
                
                x0(2) = p2(i,1);
                y0(2) = p2(i,2);
                z0(2) = p2(i,3);
                
           
            
            
            
            [XW(i,j),YW(i,j),ZW(i,j),errors(i,j)] = point_3D_rec(xs,ys,zs,x0,y0,z0);
            if diagnostic
                scatter3(XW(i,j),YW(i,j),ZW(i,j),errors(i,j).*50)
            end
%             reconstructedWorldMarkerCoordinates = [XW,YW,ZW];
                end
             end
        end
        
        %% DIRECTION CLUSTERING FOR GROUP IDENTIFICATION
        
        function dirs = directions(self,number)
            offsets = self.markerIntristicCoords2D - repmat(self.markerIntristicCoords2D(number,:),size(self.markerIntristicCoords2D,1),1);
            dirs = atan(offsets(:,2)./offsets(:,1));
        end
        
        function dists = distances(self,number)
            offsets = self.markerIntristicCoords2D - repmat(self.markerIntristicCoords2D(number,:),size(self.markerIntristicCoords2D,1),1);
            dists = sqrt(offsets(:,1).^2+offsets(:,2).^2);
        end
        
        
        
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%  VISUALIZATION FACILITIES
        %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Display marker positions and numbers in a separate figure
        function peek(self,varargin)
%             figure;

            if nargin>1
                range = varargin{1};
            else
                range =1:size(self.markerIntristicCoords2D,1);
            end
            imagesc(self.im); axis image;
            hold on;
            gena_show(self.markerIntristicCoords2D(range,:));
            h = gena_text(self.markerIntristicCoords2D(range,:));
            set(h,'Color',[1 0 0]);
        end
        
        function [dist,dirs] = showOrientations(self,number)
%             figure;scatter(zeros(size(self.directions(number))),self.directions(number))
                dist=self.distances(number);
                dirs=self.directions(number);
              scatter(dist,dirs);
              hold on;
              gena_text(dist,dirs);
        end
    end
    
end

