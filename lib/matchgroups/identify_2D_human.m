function [out2d ] = identify_2D_human( in2d, vol, ~ )
%IDENTIFY_RANSAC find correspondences between markers in 2d and 3d

%% DEFAULT OPTIONS





% %% UNITE OPTIONS
% if nargin>2
%     unite_options(opt,in_options)
% end

%% GET THE FILENAME
[~,fname,~] = fileparts(in2d.fname);
projection_id = regexpi(fname,'(AP|LAT|OPR|WPR)','match');
projection_id = projection_id{end};
patient_id = regexpi(fname,'Pat[0-9]{1,2}','match');
patient_id = patient_id{end};

fname_pattern = [patient_id,'_',projection_id,'_Markers.mat'];
the_full_file_name = fullfile(vol.inPatientFolder,'..',fname_pattern);

%% LOAD THE DATA FOR vol.fname
uros = load(the_full_file_name);
uros.markerIntristicCoords2D  = [uros.XMc./in2d.dx uros.YMc./in2d.dy];

% special case when the  projection image has been flipped (see issue#33 on
% bitbucket
if isfield(in2d,'has_been_flipped_lr') && in2d.has_been_flipped_lr
    uros.markerIntristicCoords2D(:,1) = size(in2d.im,2)-uros.markerIntristicCoords2D(:,1);
end
%% AUGMENT THE PROJECTION STRUCTURE

in2d.markerIntristicCoords2D = uros.markerIntristicCoords2D;
in2d.To3DMarkerCorrespondence = 1:12;
%% OWERWRITE
out2d = in2d;


end

