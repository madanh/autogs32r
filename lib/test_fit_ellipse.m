figure;
N=100;
imagesc(zeros(N));
axis image;
[x,y] = getpts();

a = fit_ellipse(x,y); %parameter vector of an ellipse

[X,Y] = meshgrid(1:N,1:N);

% the_ellipse = zeros(N);
% the_ellipse(abs((a(1)*X.^2+a(2)*X.*Y+a(3)*Y.^2+a(4)*X+a(5)*Y+a(6)))<0.005)=1;
% imagesc(the_ellipse);
axis image
hold on;
contour((a(1)*X.^2+a(2)*X.*Y+a(3)*Y.^2+a(4)*X+a(5)*Y+a(6)),[0,0],'r')

scatter(x,y);