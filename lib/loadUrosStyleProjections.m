function [OPR,WPR] = loadUrosStyleProjections(folderName,varargin)
% Will break if folder name is invalid, no exception handling here
%
% This function loads projection DICOM files from the Cerebral database by Uros
% Mitrovic with original filenames (two different conventions are taken
% into account). Some metadata is passed within output structures.
%
%
% AP images are considered equivalent to WPR, and LAT to OPR.

opt.verbose=1;
if nargin > 1
    opt = unite_options(opt,varargin{1});
end
verbose = opt.verbose;
%% Find files to read
% Do we have OPR/WPR pair?
currentFolder = folderName;
sublist = dir(currentFolder);
for j = 1:length(sublist)
    currentFileName = fullfile(currentFolder, sublist(j).name);
    if ~sublist(j).isdir %if not a dir
        [path,name,ext] = fileparts(sublist(j).name);
        if strcmp(ext,'') %if no extension
            %% type 1 naming :
            %LAT is considered equivalent to OPR
            if ~isempty(regexpi(name,'2D-DSA_OPR_Marker')) || ~isempty(regexpi(name,'2D-DSA_LAT$'))
                if verbose; display(sublist(j).name);end
                OPR = loadOneProjection(currentFileName);
            end
            %% type 2 naming :
            %AP is considered equivalent to WPR
            if ~isempty(regexpi(name,'2D-DSA_AP$')) || ~isempty(regexpi(name,'2D-DSA_WPR_Marker'))
                if verbose; display(sublist(j).name);end
                WPR = loadOneProjection(currentFileName);
            end
            
        end
    end
end


end

