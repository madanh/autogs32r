# README #

The code is released AS IS with no warranties of anything. We are still working on publishing it in useable form.

### How to use ###

To use this you will need to put the DICOM files with a predefined naming convention into a certain folder. Then edit call.m to use this folder and run.

### Getting help ###

This code works for us but may not work for you. If that is the case and you are interested in making it work, please  email to hennadii dot madan at fe dot uni dash lj dot si.